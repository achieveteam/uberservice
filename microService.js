import _ from 'lodash';
import Promise from 'bluebird';
import util from 'util';
import buildVersion from './buildVersion.json';
import { Logger } from './logger';
import { Domain } from './domain';
import { UberStateMachine } from './uberStateMachine';
import { ProcessArguments } from './processArguments';
import { ConfigPatternEngine } from './configPatternEngine';
import { ToleranceCounter } from './toleranceCounter';
import { UberBus } from './uberBus';
import { Environment } from './environment';
import { SystemLoad } from './systemLoad';
import { GenericError } from '@badgeteam/liberr';

export class MicroService {
    static ServiceMonitorDomainName = 'com.onbadge.service.monitor';
    static EventStoreDomainName = 'com.onbadge.eventstore';
    static MonitorStatusQuery = 'status';

    get name() {
        return this._name;
    }

    get debugName() {
        return this._debugName;
    }

    get bus() {
        return this._bus;
    }

    get stateMachine() {
        return this._stateMachine;
    }

    get isFailed() {
        return (this.stateMachine.states.current == 'failure');
    }

    get processArguments() {
        return this._processArguments;
    }

    /**
     *
     * @public
     * @returns {{name: string, time: Date, state: string, uberserviceVersion: (string|string|*)}}
     */
    get status() {
        const cpuLoad = {};
        const sysLoad = SystemLoad.get();

        for (let i = 0; i < sysLoad.cpuCount; i++)
            _.set(cpuLoad, `CPU core #${i}`, sysLoad.getCpuLoad(i));

        let boundedContext = {};

        if (_.size(this._boundedContexts) > 0)
            boundedContext = this._boundedContexts[0].monitorInfo;

        return _.merge({
            name: this._name,
            time: new Date(),
            state: this._stateMachine.states.current,
            uberserviceVersion: buildVersion.buildVersion,
            nodeVersion: process.version,
            boundedContext,
            log: Logger.getBuffer()
        }, cpuLoad);
    }

    get serviceSubDomain() {
        return `com.onbadge.${_.camelCase(this.name)}.service`;
    }

    /**
     * @param config {Object} service configuration payload
     */
    constructor(config) {
        this.logger = new Logger('service-initializing');
        this._originalConfig = config;
        this._processArguments = new ProcessArguments();
        this._stateMachine = this.createStateMachine();
        let processedConfig = (new ConfigPatternEngine()).process(config);
        this.assertConfig(processedConfig);
        if (!this.isFailed) {
            Logger.useConfig(config);
            this.logger = new Logger(this.debugName);
            this.logger.info(`current node environment (${Environment.nodeEnv})`);
            this.logger.info('uberservice library build version: ' + buildVersion.buildVersion);
            this._bus = this.createBus(processedConfig.bus);
            this._domains = {};
            this._boundedContexts = [];
            if (this._bus.stateMachine.states.current != 'failure')
                this._bus.on(UberBus.StateChangeCallback, (...args) => {
                    this.busStateChanged(...args);
                });
            this._stateMachine.onStateChanged((oldState, newState) => {
                this.logger.debug(util.format('service state changed from [%s] to [%s]', oldState, newState));
            });
            this.subscribeSystem();
            this.stateMachine.states.constructed();
            this._pendingTimers = new Map();
        }
    }

    /**
     * @private
     * @returns { UberStateMachine }
     */
    createStateMachine() {
        return new UberStateMachine()
            .state('uninitialized', { initial: true })
            .state('waitStart')
            .state('waitBusAcquire', {
                enter: () => {
                    this._bus.startAcquire();
                }
            })
            .state('waitBus')
            .state('ready', {
                enter: () => {
                    this.enterReady();
                }
            })
            .state('revival', {
                enter: () => {
                    this.enterRevival();
                }
            })
            .state('failure', {
                enter: (error) => {
                    this.logger.error(`microservice entering failure state due to ${_.get(error, 'message', 'unknown error')}`);
                    this.logger.error(`cannot resume operations, stopping microservice with error code 1`);
                    this.stop(1);
                }
            })
            .state('suspending', {
                enter: () => {
                    this.suspend();
                }
            })
            .state('suspended')
            .state('resuming', {
                enter: () => {
                    this.resume();
                }
            })
            .state('stopping', () => {
                enter: () => {
                    this.leaveReady();
                }
            })
            .event('invalidConfig', 'uninitialized', 'failure')
            .event('constructed', 'uninitialized', 'waitStart')
            .event('started', 'waitStart', 'waitBusAcquire')
            .event('busAcquire', ['waitBusAcquire', 'revival', 'ready'], 'waitBus')
            .event('busReAcquire', ['waitBusAcquire', 'revival', 'ready'], 'waitBus')
            .event('unrecoverableFailure', ['uninitialized', 'waitBusAcquire', 'waitStart', 'waitBus', 'revival', 'ready'], 'failure')
            .event('busReady', 'waitBus', 'revival')
            .event('revived', 'revival', 'ready')
            .event('suspend', 'ready', 'suspending')
            .event('suspendCompleted', 'suspending', 'suspended')
            .event('resume', 'suspended', 'resuming')
            .event('resumeCompleted', 'resuming', 'ready')
            .event('stop', 'ready', 'stopping');
    }

    /**
     * @public
     * @param config
     * @param path
     * @param defaultValue
     * @returns {*}
     */
    checkConfigProperty(config, path, defaultValue) {
        if (!defaultValue && !_.has(config, path)) {
            const error = new GenericError(`config property ${path} couldn\'t be empty`);
            this.logger.error(error.message);
            this.stateMachine.states.invalidConfig(error);
        }
        return _.get(config, path, defaultValue);
    }

    assertConfig(config) {
        if (!this.checkConfigProperty(config, 'microservice'))
            return false;

        this._name = this.checkConfigProperty(config, 'microservice.name');
        if (!this._name)
            return false;
        this._debugName = this.checkConfigProperty(config, 'microservice.debugName');
        if (!this._debugName)
            return false;

        return true;
    }

    unrecoverableFailure(error) {
        this._stateMachine.states.unrecoverableFailure(error);
        this.logger.error('unrecoverable failure: ' + error.message);
    }

    /**
     * Kill process
     * @private
     */
    kill() {
        this.logger.error('Cannot stop service gracefully. Something really bad happened, guys! Killing process :(');
        this.logger.error('  _____');
        this.logger.error(' /     \\');
        this.logger.error('| () () |');
        this.logger.error(' \\  ^  /');
        this.logger.error('  |||||');
        this.logger.error('');
        this.logger.flush();
        process.exit(1);
    }

    /**
     * Subscribe system and process events
     * @private
     * @returns {undefined}
     */
    subscribeSystem() {
        process.on('SIGINT', () => {
            this.logger.warn('received SIGINT, exiting process');
            this.stop(0);
        });

        this._stopCalled = false;
        this._rejectionCounter = new ToleranceCounter(10, 5000);
        this._rejectionCounter.onExcess((tolerance) => {
            if (!this._stopCalled) {
                this.logger.error('Exceeded maximum number (%d) of unhandled rejections. Stopping service.', tolerance);
                this._stopCalled = true;
                process.nextTick(() => { this.stop(1) });
            } else {
                this.kill();
            }
        });

        process.on('unhandledRejection', (reason) => {
            this.logger.error('Possibly unhandled rejection: %s', reason.stack);
            if (this._stopCalled)
                return this.kill();
            this.logger.error('Tolerance: %d of %d', this._rejectionCounter.increment(), this._rejectionCounter.tolerance);
        });

        this.logger.debug('subscribed process unhandled rejections (tolerance %d, timeout %d)', this._rejectionCounter.tolerance, this._rejectionCounter.timeout);

        this._exceptionCounter = new ToleranceCounter(10, 5000);
        this._exceptionCounter.onExcess((tolerance) => {
            if (!this._stopCalled) {
                this.logger.error('Exceeded maximum number (%d) of unhandled exceptions. Stopping service.', tolerance);
                this._stopCalled = true;
                process.nextTick(() => { this.stop(1) });
            } else {
                this.kill();
            }
        });

        process.on('uncaughtException', (err) => {
            this.logger.error('Possibly unhandled exception: %s', err.stack);
            if (this._stopCalled)
                return this.kill();
            this.logger.error('Tolerance: %d of %d', this._exceptionCounter.increment(), this._exceptionCounter.tolerance);
        });

        this.logger.debug('subscribed process unhandled exceptions (tolerance %d, timeout %d)', this._exceptionCounter.tolerance, this._exceptionCounter.timeout);
    }

    /**
     * Unsubscribe process events
     * @private
     */
    unsubscribeSystem() {
        process.removeAllListeners('SIGINT');
        process.removeAllListeners('uncaughtException');
        process.removeAllListeners('unhandledRejection');
    }

    /**
     * Subscribe all bus domains
     * @private
     * @returns {Promise} of success subscription
     */
    subscribeBus() {
        this.logger.info('subscribing service bus domains');

        const promises = [];

        _.forEach(this._domains, (domain, name) => {
            this.logger.debug('subscribing domain %s', name);
            promises.push(domain.subscribeProcessors());
        });

        return Promise.all(promises);
    }

    /**
     * Unsubscribe all bus domains
     * @private
     * @returns {Promise} of success unsubscription
     */
    unsubscribeBus() {
        this.logger.info('unsubscribing service bus domains');

        const promises = [];

        _.forEach(this._domains, (domain, name) => {
            this.logger.debug('unsubscribe domain %s', name);
            promises.push(domain.disableProcessors());
        });

        return Promise.all(promises);
    }

    /**
     * Processor query method
     * @private
     * @param query
     * @returns {Promise}
     */
    processQueryStatus(query) {
        return this.onMonitorStatusQuery(query);
    }

    /**
     * Service sub-domain wipe command processing
     */
    processCommandWipe() {
        return this.wipe(true);
    }

    /**
     * Service sub-domain restart command processing
     */
    processCommandRestart() {
        this.restart();
    }

    /**
     * Service sub-domain suspend command processing
     */
    processCommandSuspend() {
        return this.stateMachine.states.suspend();
    }

    /**
     * Service sub-domain suspend resume processing
     */
    processCommandResume() {
        return this.stateMachine.states.resume();
    }

    /**
     * Override to attach custom status fields for monitor
     * @protected
     * @virtual
     * @param query { Object } - monitor query
     * @returns {Promise} - Promise of service status
     */
    onMonitorStatusQuery(query) {
        return Promise.resolve(this.status);
    }

    /**
     * Override to create bus
     * @protected
     * @param {Object} config Bus configuration
     * @returns {UberBus|MockServiceBus} bus instance
     */
    createBus(config) {
        this.logger.info('creating service bus');
        return new UberBus(config, this._name);
    }

    /**
     * Called by entry point
     * @internal
     * @virtual
     * @returns {undefined}
     */
    start() {
        if (this.isFailed) {
            this.logger.error('could not start service due failure state');
            this.kill();
            return;
        }

        this.logger.info('starting up, wait start promise');
        return this.startPromise()
            .then(() => {
                this.logger.info('start promise resolved, notify state machine');
                this.stateMachine.states.started();
            });
    }

    startPromise() {
        return Promise.resolve();
    }

    /**
     * Override to attach own domain processors. This is the only valid place to do this.
     * @protected
     * @virtual
     * @returns {Promise}
     */
    initProcessors() {
        this.logger.debug('assign monitor domain processor');
        return this.useDomainProcessor(MicroService.ServiceMonitorDomainName, this)
            .then(() => {
                return this.getDomain(MicroService.ServiceMonitorDomainName).awake();
            })
            .then(() => {
                this.logger.debug('assign service subdomain (%s) domain processor', this.serviceSubDomain);
                return this.useDomainProcessor(this.serviceSubDomain, this);
            })
            .then(() => {
                this.logger.debug('assign bounded contexts dynamic processors');
                return Promise.map(this._boundedContexts, (context) => {
                    return Promise.map(context.getProcessors(), (entry) => {
                        this.logger.info(`bind bounded context processor for scope ${entry.scope}`);
                        return this.useDomainProcessor(entry.scope, entry.processor);
                    });
                });
            });
    }

    /**
     * Override to create bounded contexts
     * @protected
     * @virtual
     * @returns {Promise}
     */
    initBoundedContexts() {
        this.logger.debug('init bounded contexts');
        return Promise.resolve();
    }

    /**
     * Override to free service specific resources
     * @param {Number} code Exit code
     * @returns {undefined}
     */
    stop(code) {
        this.logger.info('stopping with code: %s', code);

        return Promise.resolve()
            .then(() => {
                this.logger.info(`clearing pending ${this._pendingTimers.size} timers`);

                this._pendingTimers.forEach((data, timeout) => {
                    clearTimeout(timeout);
                    if (_.isFunction(data.reject))
                        data.reject();
                });

                this._pendingTimers = new Map();

                this.logger.info('clearing domains');


                return this.unsubscribeBus()
            })
            .then(() => {
                return this.bus.close();
            })
            .then(() => {
                this.logger.info('unsubscribe process events');
                this.unsubscribeSystem();
                if (code != 0)
                    this.logger.info('bad exit code (%d), will force process to exit', code);

                this.logger.info('disposing SystemLoad');
                SystemLoad.dispose();

                this.logger.info('flushing logs');
                return this.logger.flush();
            })
            .then(() => {
                if (code != 0)
                    process.exit(code);
            })
            .catch((error) => {
                this.logger.error(`cannot stop microservice due to error ${error.message}`);
                this.kill();
            });
    }

    /**
     * Service entering revival state
     * @protected
     * @virtual
     */
    enterRevival() {
        this.logger.info('entering REVIVAL state, init ddd contexts');
        return this.initBoundedContexts()
            .then((result) => {
                if (!_.isEmpty(result)) {
                    this._boundedContexts = [].concat(result);
                    this.logger.info(`assigned ${_.size(this._boundedContexts)} bounded contexts`);
                    return;
                }
                this.logger.info(`revival - no bounded contexts for service`);
            })
            .then(() => {
                this.logger.info('revival - initializing domain processors');
                return this.initProcessors();
            })
            .then(() => {
                this.logger.info('revival - subscribing bus');
                return this.subscribeBus();
            })
            .then(() => {
                if (Environment.isDevelopment) {
                    this.logger.info('dropping all projections (except sagas) on start in development environment');
                    return Promise.map(this._boundedContexts, (context) => {
                        return context.resetProjections(false);
                    });
                }
            })
            .then(() => {
                return Promise.map(this._boundedContexts, (context) => {
                    this.logger.info('revival - reviving bounded context projections');
                    return context.reviveProjections();
                });
            })
            .then(() => {
                this.logger.info('revival - completed');
                this.stateMachine.states.revived();
            });
    }

    /**
     * Entering ready state
     * @returns {Promise}
     */
    enterReady() {
        this.logger.info('entering READY state');
        const promises = [];

        this.logger.info('awaking all domains...');

        _.forEach(this._domains, (domain) => {
            promises.push(domain.awake());
        });

        return Promise.all(promises)
            .then(() => {
                this.logger.info('begin consume event stream');
                return this._bus.consume();
            });
    }

    /**
     * Leaving ready state
     * @returns {Promise}
     */
    leaveReady() {
        this.logger.debug('leaving READY state');

        const promises = [];

        promises.push(this.unsubscribeBus());

        return Promise.all(promises)
            .then(() => {
                this.logger.info('cancel consume event stream');
                this._bus.cancel();
            });
    }

    /**
     * Bus state handler
     * @param {string} oldState Old bus state
     * @param {string} newState New bus state
     */
    busStateChanged(oldState, newState) {
        if (newState == 'ready') {
            this._stateMachine.states.busReady();
        } else if (newState == 'acquire') {
            if (oldState == 'ready')
                this._stateMachine.states.busReAcquire();
            else
                this._stateMachine.states.busAcquire();
        } else if (newState == 'failure') {
            this.logger.warn('bus failure');
            this._stateMachine.states.unrecoverableFailure();
        }
    }

    /**
     * Override to wipe service data
     * @protected
     * @virtual
     * @returns {Promise}
     */
    wipe() {
        this.logger.warn('WIPE OUT SERVICE PERSISTENCE');
        this.logger.warn('reset projections for bounded contexts');

        return Promise.map(this._boundedContexts, (context) => {
            return context.resetProjections(true);
        })
        .then(() => {
            return 'wiped out';
        });
    }

    /**
     * Override to do additional restart operations
     * @protected
     * @virtual
     */
    restart() {
        const serviceClass = this.constructor;
        const serviceConfig = this._originalConfig;

        setTimeout(function() {
            (new serviceClass(serviceConfig)).start();
        }, 1000);

        this.stop(0);
    }

    /**
     * Override to do additional suspend operations
     */
    suspend() {
        this.logger.info('suspending...');
        this.logger.info('locking domains...');

        const promises = [];

        _.forEach(this._domains, (domain, name) => {
            if ((name !== MicroService.ServiceMonitorDomainName) && (name !== this.serviceSubDomain))
                promises.push(domain.suspend());
        });

        return Promise.all(promises)
            .then(() => {
                this.stateMachine.states.suspendCompleted();
            });
    }

    /**
     * Override to do additional resume operations
     */
    resume() {
        this.logger.info('resuming...');
        this.logger.info('unlocking domains...');

        const promises = [];

        _.forEach(this._domains, (domain, name) => {
            if ((name !== MicroService.ServiceMonitorDomainName) && (name !== this.serviceSubDomain))
                promises.push(domain.unlock());
        });

        return Promise.all(promises)
            .then(() => {
                this.stateMachine.states.resumeCompleted();
            });
    }

    /**
     * Returns domain by name. Only valid way to get domain within microservice.
     * @internal
     * @param {string} domainName Full qualified domain name
     * @returns {Domain}
     */
    getDomain(domainName) {
        if (!this._domains[domainName]) {
            this.logger.debug('no domain %s in cache, create one', domainName);
            this._domains[domainName] = new Domain(this._bus, domainName);
            if (this.stateMachine.states.current == 'ready')
                this._domains[domainName].awake();
        }
        return this._domains[domainName];
    }

    /**
     * Domain.userProcessor wrapper
     * @param {string} domainName Full qualified domain name
     * @param {Object} processor Processor instance
     * @returns {Promise} of success processor attach
     */
    useDomainProcessor(domainName, processor) {
        let domain = this.getDomain(domainName);
        if (!domain) {
            this.logger.error('useDomainProcessor - cant get domain %s', domainName);
            return Promise.reject(new Error(`cant get domain ${domainName}`));
        }
        return domain.useProcessor(processor);
    }

    /**
     * Adds new pending timeout
     * @protected
     */
    setTimeout(handler, reject, timeout) {
        const pending = setTimeout(() => {
            this._pendingTimers.delete(pending);
            handler();
        }, timeout);
        this._pendingTimers.set(pending, { reject: reject });
        return pending;
    }
}