import _ from 'lodash';
import util from 'util';
import Promise from 'bluebird';
import { Logger } from './logger';
import { GenericError, GatewayTimeoutError, RequestTimeoutError } from '@badgeteam/liberr';
import { UberBus } from './uberBus';
import { UberUtils } from './uberUtils';

export class Domain {
    /**
     * Returns domain by full object name (route)
     * @public
     * @param name - Full event, command or query name
     * @returns {Object}
     */
    static splitName(name) {
        if (!name)
            return name;

        const parts = name.split('.');

        return {
            domain: parts.slice(0, -2).join('.'),
            type: parts.slice(-2, -1).join('.'),
            name: parts.slice(-1).join('.')
        }
    }

    /**
     * @returns {string} - This domain name
     */
    get name() {
        return this._name;
    }

     /**
     * @private
     * @return {boolean} - Returns true if bus ready
     */
    get isBusReady() {
        return this._bus.stateMachine.states.current == 'ready';
    }

    /**
     * @public
     * @return {boolean} - Suspend flag
     */
    get isSuspended() {
        return this._suspended;
    }

    /**
     * @public
     * @constructor
     * @param {UberBus|MockServiceBus} bus - Message bus
     * @param {string} name - Domain name (e.g. com.onbadge.service)
     */
    constructor(bus, name) {
        const nameParts = _.split(name, '.');
        if (_.size(nameParts) > 2)
            this._logger = new Logger('domain(' + nameParts.slice(2).join('.') + ')');
        else
            this._logger = new Logger('domain(' + name + ')');
        this._logger.debug('constructing');
        this._bus = bus;
        this._suspended = true;
        this._processors = new Map();
        this._subscriptions = new Map();
        if (!name)
            throw new Error('empty domain name');
        this._name = name;
        let domainParts = this._name.split('.');
        if (domainParts.length < 3)
            throw new Error(this._name + ' is invalid domain (format example com.onbadge.service.[sub1].[sub2]');
        this._logger.debug('constructed');
    }

    /**
     * @private
     * @param {Object} processor
     * @param {Array} typeFilter
     */
    saveSubscriptions(processor, typeFilter) {
        let types = this._subscriptions.get(processor);
        if (!types)
            types = [];

        types = _.union(types, typeFilter);

        this._subscriptions.set(processor, types);
    }

    /**
     * @private
     * @param {Object} processor
     * @param {Array} typeFilter
     * @returns {Array}
     */
    checkSubscriptions(processor, typeFilter) {
        let types = this._subscriptions.get(processor);
        if (!types)
            return typeFilter;
        return _.difference(typeFilter, types);
    }


    /**
     * Route formatter
     * @private
     * @param {string} type - Message type
     * @param {string} pattern - Message name or pattern (wildcard)
     * @returns {string} - Bus route
     */
    getRoute(type, pattern) {
        return util.format('%s.%s.%s', this._name, type, pattern);
    }

    /**
     * Returns event route by name
     * @public
     * @param {string} name - Name of domain event
     * @returns {string} - Route
     */
    getEventRoute(name) {
        return this.getRoute(UberBus.Event, name);
    }

    /**
     * Returns command route by name
     * @public
     * @param {string} name - Name of domain command
     * @returns {string} - Route
     */
    getCommandRoute(name) {
        return this.getRoute(UberBus.Command, name);
    }

    /**
     * Returns query route by name
     * @public
     * @param {string} name - Name of domain query
     * @returns {string} - Route
     */
    getQueryRoute(name) {
        return this.getRoute(UberBus.Query, name);
    }

    /**
     * Returns query notice by name
     * @public
     * @param {string} name - Name of domain query
     * @returns {string} - Route
     */
    getNoticeRoute(name) {
        return this.getRoute(UberBus.Notice, name);
    }

    /**
     * Publishes event to domain with no response
     * @public
     * @param {string|Object} name - Command name or object with name & flags
     * @param {Object} payload - Event payload
     * @param {number} version Event version (only for persistent events)
     * @returns {Promise} of event delivery
     */
    event(name, payload) {
        return this._bus.sendEvent(this._name, name, payload);
    }

    /**
     * Publishes notice to domain with no response
     * @public
     * @param {string|Object} name - Command name or object with name & flags
     * @param {Object} payload - Event payload
     * @returns {Promise} of event delivery
     */
    notice(name, payload) {
        return this._bus.sendNotice(this._name, name, payload);
    }

    /**
     * Makes bus query and returns a promise of result
     * @public
     * @param {string|Object} name - Command name or object with name & flags
     * @param {Object} payload - Query payload
     * @param {Function} [filter] - Result filter callback
     * @param {Number} [timeout] - Response wait timeout in ms
     * @returns { Promise } of event delivery
     */
    query(name, payload, filter, timeout) {
        return this._bus.sendQuery(this._name, name, payload, filter, timeout);
    }

    /**
     * Sequence of bus queries
     * @public
     * @param {string|Object} name - Command name or object with name & flags
     * @param {Object} payload - Query payload (default - {})
     * @param {Function} assertFunc - Query response assert function ( assertFunc(response) : boolean )
     * @param {Number} [tries] - Max queries number (default - 5)
     * @param {Number} [delay] - Delay between tries in ms (default - 500)
     * @param {Function} [errorClass] - Error class to return (default - GatewayTimeoutError)
     */
    repeatQuery(name, payload, assertFunc, tries = 5, delay = 250, errorClass = GatewayTimeoutError) {
        if (!_.isFunction(assertFunc))
            return GenericError.logPromise(this._logger, 'repeatQuery - assertFunc is not a function');

        if (!_.isNumber(tries))
            return GenericError.logPromise(this._logger, 'repeatQuery - tries is not a number');

        if (!_.isNumber(delay))
            return GenericError.logPromise(this._logger, 'repeatQuery - delay is not a number');

        if (!_.isFunction(errorClass))
            return GenericError.logPromise(this._logger, 'repeatQuery - errorClass is not a function (class description)');

        let remainTries = tries;

        const doQuery = () => {
            return this.query(name, payload)
                .then((response) => {
                    if (assertFunc(response))
                        return response;
                    return null;
                })
                .catch(RequestTimeoutError, (error) => { })
                .then((response) => {
                    if (_.isEmpty(response)) {
                        if (--remainTries <= 0)
                            return GatewayTimeoutError.promise(`no query ${name} scope ${this._name} valid response received after ${tries} tries with ${delay} ms delay`);

                        return Promise.delay(delay)
                            .then(() => {
                                return doQuery();
                            });
                    }
                    return response;
                });
        };
        return doQuery();
    };


    /**
     * Sends command to domain and returns a promise of result
     * @public
     * @param {string|Object} name - Command name or object with name & flags
     * @param {Object} payload - Command payload
     * @param {Number} [timeout] - Response wait timeout
     * @returns {Promise} of result payload
     */
    command(name, payload, timeout) {
        return this._bus.sendCommand(this._name, name, payload, timeout);
    }

    /**
     * Sends command or query depends on type param to domain and returns a promise of result
     * @public
     * @param {string|Object} name - Command/query name or object with name & flags
     * @param {Object} payload - Command/query payload
     * @param {Number} [timeout] - Response wait timeout
     * @returns {Promise} of result payload
     */
    exec(type, name, payload, timeout) {
        if (type == UberBus.Command)
            return this.command(name, payload, timeout);
        if (type == UberBus.Query)
            return this.query(name, payload, null, timeout);
        return GenericError.promise(`unknown type [${type}] for [exec] domain operation`);
    }

    /**
     * Enumerate processor methods respect to class hierarchy
     * @private
     * @param {Array} typeFilter - Filter types (command, event, etc)
     * @param {Object} processor - Processor instance
     * @param {Object} classType - Current class
     * @param {Function} callback - Founded entries callback function
     * @return {undefined}
     */
    enumerateProcessorMethods(typeFilter, processor, callback) {
        UberUtils.enumerateAllMethods(processor, 'process', (key, entryName) => {
            _.forEach(typeFilter, (type) => {
                if (_.startsWith(entryName, type)) {
                    entryName = entryName.replace(type, '');
                    entryName = entryName.charAt(0).toLowerCase() + entryName.slice(1);
                    callback(key, type, entryName);
                    return false;
                }
            });
        });
    }

    /**
     * Subscribe processor handlers methods to bus
     * Handler format 'process[Type][Name]'
     * @private
     * @param {Array} typeFilter - Methods filtering
     * @param {Object} processor - Processor type (Command, Query, Event)
     * @return {undefined}
     */
    subscribeProcessor(typeFilter, processor) {
        if (!this.isBusReady) {
            this._logger.warn('subscribeProcessor - bus not ready');
            return Promise.resolve();
        }

        const processorInfo = this._processors.get(processor);
        if (processorInfo) {
            if (!processorInfo.listeners)
                processorInfo.listeners = {};
            if (!processorInfo.listenersInfo)
                processorInfo.listenersInfo = {};

            const promises = [];
            const validTypeFilter = this.checkSubscriptions(processor, typeFilter);

            if (_.isEmpty(validTypeFilter))
                return Promise.resolve();

            const methodEnumeratorCallback = (key, type, entryName) => {
                const listenerKey = type + entryName;

                processorInfo.listeners[listenerKey] = (payload) => {
                    return processor[key](payload, this);
                };
                promises.push(this._bus.listen(this._name, type, entryName, processorInfo.listeners[listenerKey])
                    .then((listenInfo) => {
                        this._logger.debug('subscribeProcessor - set processor listener info (%s.%s)', type, entryName);
                        processorInfo.listenersInfo[listenerKey] = listenInfo;
                    }));
            };

            this.enumerateProcessorMethods(validTypeFilter, processor, methodEnumeratorCallback);

            return Promise.all(promises)
                .then(() => {
                    this.saveSubscriptions(processor, validTypeFilter);
                });
        } else {
            this._logger.warn('subscribeProcessor - no processor info');
            return Promise.resolve();
        }
    }

    /**
     * Unsubscribe process handlers methods from bus
     * @private
     * @param {Object} processor Processor instance
     */
    unsubscribeProcessor(processor) {
        const processorInfo = this._processors.get(processor);

        if (processorInfo && processorInfo.listeners) {
            const promises = [];

            _.forEach(processorInfo.listenersInfo, (listenerInfo) => {
                this._logger.debug('removing bus listener for entry (%s)', listenerInfo.routingKey);
                promises.push(
                    this._bus.removeListener(listenerInfo)
                        .then(() => {
                            this._logger.debug('unsubscribeProcessor - bus listener (%s) removed', listenerInfo.routingKey);
                            return Promise.resolve();
                        }));
            });

            delete processorInfo.listeners;
            delete processorInfo.listenersInfo;

            return Promise.all(promises)
                .then(() => {
                    this._logger.debug('unsubscribeProcessor - all processor entries removed from bus');
                })
                .catch((err) => {
                    this._logger.error('unsubscribe processor error %s', err.stack);
                });
        }

        this._logger.debug('unsubscribeProcessor - no processor info or active listeners founded, skipping bus remove');

        return Promise.resolve();
    }

    /**
     * Command this domain to use processor
     * @public
     * @param {Object} processor - Processor instance or null to disable current processor
     * @return {Promise}
     */
    useProcessor(processor) {
        if (!processor) {
            this._logger.warn('useProcessor - empty processor passed');
            return Promise.resolve();
        }

        if (!this._processors.get(processor)) {
            this._logger.debug('useProcessor - processor subscription attempt');
            this._processors.set(processor, {
                enabled: true
            });
            if (this.isBusReady) {
                if (this._suspended) {
                    this._logger.debug('useProcessor - bus ready but processor suspended, subscribe only event channel');
                    return this.subscribeProcessor([UberBus.Event], processor);
                }
                this._logger.debug('useProcessor - bus ready, subscribe all channels');
                return this.subscribeProcessor([UberBus.Event, UberBus.Command, UberBus.Notice, UberBus.Query], processor);
            }
            return Promise.resolve();
        } else {
            this._logger.warn('useProcessor - already using this processor');
            return Promise.resolve();
        }
    }

    /**
     * Disable processor (removes all bus dependencies)
     * @param {Object} processor
     * @returns {Promise}
     */
    disableProcessor(processor) {
        if (processor && this._processors.get(processor)) {
            return this.unsubscribeProcessor(processor)
                .then(() => {
                    this._processors.delete(processor);
                });
        } else {
            this._logger.warn('disableProcessor - empty or unassigned processor');
            return Promise.resolve();
        }
    }

    /**
     * Subscribe processors to bus
     */
    subscribeProcessors() {
        if (this.isBusReady) {
            const promises = [];
            const typeFilter = this._suspended ? [UberBus.Event] : [UberBus.Event, UberBus.Command, UberBus.Notice, UberBus.Query];

            for (const processor of this._processors.keys())
                promises.push(this.subscribeProcessor(typeFilter, processor));

            return Promise.all(promises);
        }
        this._logger.warn('subscribeProcessors - bus not ready');
        return Promise.resolve();
    }

    /**
     * Disable all assigned processors
     * @returns {Promise}
     */
    disableProcessors() {
        const promises = [];

        for (const processor of this._processors.keys())
            promises.push(this.unsubscribeProcessor(processor));

        return Promise.all(promises);
    }

    /**
     * Awake suspended operations (commands, queries)
     * @returns {Promise}
     */
    awake() {
        if (this._suspended) {
            this._suspended = false;

            this._logger.debug('awakening...')

            const promises = [];

            if (this.isBusReady) {
                this._logger.debug('bus ready, subscribing %d processors', this._processors.size);

                for (const processor of this._processors.keys())
                    promises.push(this.subscribeProcessor([UberBus.Command, UberBus.Notice, UberBus.Query], processor));
            }

            return Promise.all(promises);
        }
        this._logger.warn('awake - already awakened');
        return Promise.resolve();
    }
}
