import { Logger } from './logger';
import { UberUtils } from './uberUtils';
import { GenericError } from '@badgeteam/liberr';

export class Processor {
    /**
     * @protected
     * @returns {Logger}
     */
    get logger() {
        return this._logger;
    }

    /**
     * @public
     */
    constructor(scope = 'com.onbadge') {
        this._scope = scope;
        this._logger = new Logger(UberUtils.classNameToLogName(this.constructor));
    }

    /**
     * @protected
     * @param {Object} payload - payload from the event handler
     * @param {*} fields - required field or fields to handle the event
     * @returns Promise<Object>
     */
    pick(payload, ...fields) {
        return UberUtils.promisePick(payload, ...fields);
    }

    /**
     * @protected
     * @param {Function} domainProvider
     * @param {String} type
     * @param {String} domainCommandName
     * @param {Object} payload
     * @returns {Promise}
     */
    exec(domainProvider, type, domainCommandName, payload) {
        const nameParts = domainCommandName.split('.');
        if (nameParts.length != 2)
            return GenericError.promise(`domain command name should have [subdomain].[command] format but was [${domainCommandName}]`);
        return domainProvider([this._scope, nameParts[0]].join('.')).exec(type, nameParts[1], payload);
    }
}