import _ from 'lodash';
import util from 'util';
import Promise from 'bluebird';


export class UberStateMachine {
    constructor() {
        this._states = {
            current: null
        };
    }

    get states() { return this._states; }

    state(name, options) {
        if (!name)
            throw new Error('the name of the state should be a valid unique string');
        if (name in this._states)
            throw new Error(util.format('the name of the state is already defined: [%s]', name));

        let state = _.defaults({
            name: name,
            events: { }
        }, options, { initial: false });
        this._states[name] = state;

        if (this._states.current && state.initial)
            throw new Error(util.format('the machine already have the initial state [%s]: [%s]', this._state, name));
        if (state.initial)
            this._states.current = name;

        return this;
    }

    event(name, from, to) {
        if (!name)
            throw new Error('the name of the event should be a valid unique string');
        if (name in this._states && !_.isFunction(this._states[name]))
            throw new Error(util.format('the name of the event function is already used as state: [%s]', name));
        if (!_.isString(to))
            throw new Error(util.format('the name of the [to] state should be a string: [%s]', to));
        if (!(to in this._states))
            throw new Error(util.format('the name of the [to] state is not defined: [%s]', to));

        [].concat(from).forEach((fromStateName) => {
            if (!(fromStateName in this._states))
                throw new Error(util.format('the name of the [from] state is not defined: [%s]', fromStateName));
            let state = this._states[fromStateName];

            if (name in state.events)
                throw new Error(util.format('the name of the event for the state [%s] is already defined: [%s]', fromStateName, name));
            state.events[name] = to;
        });

        if (!_.isFunction(this._states[name]))
            this._states[name] = (...args) => { this.trigger(name, ...args); };

        return this;
    }

    trigger(eventName, ...args) {
        if (!this._states.current)
            throw new Error(util.format('no state is marked as \'initial\''));

        let prevState = this._states[this._states.current];
        if (eventName in prevState.events) {
            if (_.isFunction(prevState.leave))
                prevState.leave(...args);
            this._states.current = prevState.events[eventName];
            let nextState = this._states[this._states.current];
            if (_.isFunction(nextState.enter))
                nextState.enter(...args);

            if (_.isFunction(this._states.stateChanged))
                this._states.stateChanged(prevState.name, nextState.name);
        }
    };

    onStateChanged(callback) {
        if (_.isFunction(this._states.stateChanged))
            throw new Error('stateChanged callback is already defined');
        this._states.stateChanged = callback;
    }
}
