import _ from 'lodash';

export class Environment {
    /**
     * @private
     */
    static isInitialized = false;

    /**
     * @public
     */
    static init() {
        if (_.isNil(process.env.NODE_ENV))
            process.env.NODE_ENV = 'development';
        this.isInitialized = true;
    }

    /**
     * @public
     * @returns {boolean}
     */
    static get isProduction() {
        if (!this.isInitialized)
            this.init();

        return (process.env.NODE_ENV === 'production');
    }

    /**
     * @public
     * @returns {boolean}
     */
    static get isDevelopment() {
        if (!this.isInitialized)
            this.init();

        return (process.env.NODE_ENV === 'development');
    }

    /**
     * @public
     * @returns {string}
     */
    static get nodeEnv() {
        if (!this.isInitialized)
            this.init();

        return process.env.NODE_ENV;
    }

    /**
     * @public
     */
    static setProduction() {
        process.env.NODE_ENV = 'production';
    }

    /**
     * @public
     */
    static setDevelopment() {
        process.env.NODE_ENV = 'development';
    }
}
