import EventEmitter from 'events';

export class ToleranceCounter {
    /**
     * @public
     * @returns {Number} - Tolerance level
     */
    get tolerance() {
        return this._tolerance;
    }

    /**
     * @public
     * @returns {Number} - Timeout (ms)
     */
    get timeout() {
        return this._timeout;
    }

    /**
     * @public
     * @constructor
     * @param {Number} tolerance - Tolerance level
     * @param {Number} timeout - Tolerance timeout (ms)
     */
    constructor(tolerance, timeout) {
        this._tolerance = tolerance;
        this._timeout = timeout;
        this._counter = 0;
        this._emitter = new EventEmitter();
        this._timer = null;
    }

    /**
     * @private
     */
    setTimer() {
        if (!this._timer) {
            this._timer = setTimeout(() => {
                this._counter = 0;
                this._timer = null;
            }, this._timeout);
        }
    }

    /**
     * @private
     */
    clearTimer() {
        if (this._timer) {
            clearTimeout(this._timer);
            this._timer = null;
        }
    }

    /**
     * Increment counter
     * @public
     * @returns {Number} - Current counter value
     */
    increment() {
        this._counter++;

        if (this._counter > this._tolerance) {
            this.clearTimer();
            this._emitter.emit('excess', this._tolerance);
        } else {
            this.setTimer();
        }

        return this._counter;
    }

    /**
     * Add excess listener callback
     * @param {Function} callback
     * @returns {undefined}
     */
    onExcess(callback) {
        this._emitter.on('excess', callback);
    }
}
