import _ from 'lodash';
import Promise from 'bluebird';
import uuid from 'node-uuid';
import { Domain } from '../domain';
import { EventProjection } from '../ddd/eventProjection';
import { MemoryStateStore } from '../ddd/memoryStateStore';
import { UberBus } from '../uberBus';

class HostProjection extends EventProjection {
    static injectComponent(component) {
        if (_.includes(this, 'getComponents'))
            delete this['getComponents'];

        Object.defineProperty(this, 'getComponents', {
            enumerable: true,
            configurable: true,
            value: function () {
                return component;
            }.bind(this)
        });
    }

    static injectMethod(name, impl) {
        if (_.includes(this.prototype, name))
            delete this[name];

        Object.defineProperty(this.prototype, name, {
            enumerable: true,
            configurable: true,
            value: function (...args) {
                return impl(...args);
            }
        });
    }


    get componentChanges() {
        return this._componentChanges;
    }

    componentChanged(name, key, state) {
        if (!_.isArray(this._componentChanges))
            this._componentChanges = [];

        this._componentChanges.push({ name, key, state });
    }
}

class TesterBase {
    /**
     * @constructor
     * @param {Function} projectionClass
     * @param {Array} [events]
     * @param {String} [root]
     * @param {*} [initializeArgs]
     */
    constructor(projectionClass, events, root = 'com.onbadge', ...initializeArgs) {
        this._stateStore = new MemoryStateStore();
        this._projectionClass = projectionClass;
        this._events = events || [];
        this._root = root;
        this._fixVersion = 0;
        this._fixStreamVersion = 0;
        this._fixAggregateId = uuid.v4().toString();
        this._initializeArgs = initializeArgs;
    }

    /**
     * @protected
     */
    invokeHandler(name, prefix, type, data) {
        const localName = _.replace(this.route(name, type), this._root);
        const parsedName = Domain.splitName(localName);

        let domainName = '';
        _.forEach(_.split(parsedName.domain, '.'), (part) => {
            domainName += _.upperFirst(part);
        });

        const handlerName = `${prefix}${_.upperFirst(domainName)}${_.upperFirst(type)}${_.upperFirst(parsedName.name)}`;
        const noHandler = { error: '__no__handler__'};

        if (!_.isFunction(this._projection[handlerName]))
            return Promise.resolve(noHandler);

        return this._projection[handlerName](data);
    }

    /**
     * @private
     */
    route(name, type) {
        let parts = name.split('.');
        parts.splice(-1, 0, type);
        return parts.join('.');
    }

    /**
     * @private
     */
    invokeEventHandlers(events) {
        return Promise.each(events, (event) => {
                const clone = _.cloneDeep(event);
                if (_.isEmpty(clone.aggregateId))
                    clone.aggregateId = this._fixAggregateId;

                if (_.isEmpty(clone.version))
                    clone.version = ++this._fixVersion;

                if (_.isEmpty(clone.streamVersion))
                    clone.streamVersion = ++this._fixStreamVersion;

                return this.invokeHandler(event.name, 'handle', UberBus.Event, event);
            })
            .then(() => {
                return this;
            });
    }

    /**
     * @private
     */
    makeProjection(projectionClass) {
        this._projection = new projectionClass(this, this._stateStore, ...this._initializeArgs);
        return this._projection.build();
    }

    /**
     * @public
     * @returns {Promise}
     */
    make() {
        return this.makeProjection(this._projectionClass)
            .then(() => {
                return this.invokeEventHandlers(this._events);
            });
    }

    /**
     * @public
     * @param {Array|Object} events
     * @returns {Promise}
     */
    sendEvents(events) {
        return this.invokeEventHandlers([].concat(events));
    }
}


export class ProjectionTester extends TesterBase {
    /**
     * @public
     */
    get projection() {
        return this._projection;
    }

    /**
     * @public
     * @param {Function} componentClass
     */
    getComponent(componentClass) {
        return this._projection.getComponent(componentClass);
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} payload
     * @returns {Promise}
     */
    execQuery(name, payload) {
        return this.invokeHandler(name, 'respond', UberBus.Query, payload);
    }

    /**
     * @public
     * @param {Function} componentClass
     * @param {string} key
     * @param {Object} state
     */
    componentStateChanged(componentClass, key, state) {
        if (_.isFunction(this._projection['componentStateChanged']))
            return this._projection['componentStateChanged'](key, state, this.getComponent(componentClass));
        return Promise.resolve();
    }

    /**
     * @public
     * @param {Function} componentClass
     * @param {string} name
     * @param {string} key
     * @param {Object} state
     * @returns {*}
     */
    componentChanged(componentClass, name, key, state) {
        if (_.isFunction(this._projection['componentChanged']))
            return this._projection['componentChanged'](name, key, state, this.getComponent(componentClass));
        return Promise.resolve();
    }
}

export class ProjectionComponentTester extends TesterBase {
    /**
     * @constructor
     * @param {Function} componentClass
     * @param {Array} [events]
     * @param {String} [root]
     */
    constructor(componentClass, events, root = 'com.onbadge') {
        HostProjection.injectComponent(componentClass);
        super(HostProjection, events, root);
        this._componentClass = componentClass;
    }

    /**
     * @public
     */
    get component() {
        return this._projection.getComponent(this._componentClass);
    }

    /**
     * @public
     */
    findComponentChange(name, key, state = null) {
        return _.find(this._projection.componentChanges, _.matches(_.merge({ name, key }, _.isNil(state) ? { } : { state })));
    }

    /**
     * @public
     */
    mockProjectionMethod(name, impl) {
        HostProjection.injectMethod(name, impl);
    }
}
