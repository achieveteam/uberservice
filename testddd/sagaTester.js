import _ from 'lodash';
import Promise from 'bluebird';
import util from 'util';
import chai from 'chai';
import chaiSubset from 'chai-subset';
import { RequestTimeoutError } from '@badgeteam/liberr';
import { ProjectionTester } from './projectionTester';

chai.use(chaiSubset);
const expect = chai.expect;

export class SagaTester extends ProjectionTester {
    /**
     * @public
     */
    get executedCommands() {
        return this._executedCommands;
    }

    /**
     * @public
     */
    get executedQueries() {
        return this._executedQueries;
    }


    /**
     * @public
     * @constructor
     * @param {Function} sagaClass
     * @param {Array} [events]
     * @param {String} [root]
     * @param {*} [initializeArgs]
     */
    constructor(sagaClass, events, root, ...initializeArgs) {
        super(sagaClass, events, root, ...initializeArgs);
        this._queryHandlers = new Map();
        this._commandHandlers = new Map();
        this._executedCommands = [];
        this._executedQueries = [];
    }

    /**
     * @private
     */
    checkEntry(list, name, payload, exactMatch) {
        const item = _.find(list, (entry) => {
            const nameEqual = _.isEqual(name, entry.name);
            const payloadEqual = _.isNil(payload) ||
                (exactMatch ? _.isEqual(payload, entry.payload) : _.isMatch(entry.payload, payload));

            return nameEqual && payloadEqual;
        });
        return !_.isNil(item);
    }

    /**
     * @private
     */
    executeHandler(handlerMap, name, payload) {
        const handler = handlerMap.get(name);
        if (_.isFunction(handler))
            return Promise.resolve(handler(payload));
        return RequestTimeoutError.promise(`no handler for ${name} assigned, emulate request timeout error`);
    }

    /**
     * @public
     * @param {String} name
     * @param {Function} handler
     */
    setQueryHandler(name, handler) {
        this._queryHandlers.set(name, handler);
    }

    /**
     * @public
     * @param {String} name
     * @param {Function} handler
     */
    setCommandHandler(name, handler) {
        this._commandHandlers.set(name, handler);
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} [payload]
     * @param {boolean} [exactMatch]
     * @returns {boolean}
     */
    queryExecuted(name, payload = null, exactMatch = true) {
        return this.checkEntry(this._executedQueries, name, payload, exactMatch);
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} [payload}
     * @param {boolean} [exactMatch]
     * @returns {boolean}
     */
    commandExecuted(name, payload = null, exactMatch = true) {
        return this.checkEntry(this._executedCommands, name, payload, exactMatch);
    }

    /**
     * @public
     * @returns {undefined}
     */
    expectCommandExecuted(name, payload = null, exactMatch = true) {
        if (exactMatch) {
            const match = _.find(this.executedCommands, { name });
            expect(match.payload, `expect command executed ${name}`).to.be.eql(payload);
        }
        else
            expect(this.executedCommands, `expect command executed ${name}`).to.containSubset([_.omitBy({ name, payload }, _.isNil)]);
    }

    /**
     * @public
     * @returns {undefined}
     */
    expectCommandNotExecuted(name, payload = null, exactMatch = true) {
        if (exactMatch) {
            const match = _.find(this.executedCommands, { name });
            if (!_.isEmpty(match))
                expect(match.payload, `expect command NOT executed ${name}`).to.be.not.eql(payload);
        }
        else
            expect(this.executedCommands, `expect command NOT executed ${name}`).to.not.containSubset([_.omitBy({ name, payload }, _.isNil)]);
    }

    /**
     * @public
     * @returns {undefined}
     */
    expectQueryExecuted(name, payload = null, exactMatch = true) {
        expect(this.queryExecuted(name, payload, exactMatch), `expected query ${name}${_.isNil(payload) ? '' : ' with payload ' + util.inspect(payload)}`).to.be.true;
    }

    /**
     * @public
     * @returns {undefined}
     */
    expectQueryNotExecuted(name, payload = null, exactMatch = true) {
        expect(this.queryExecuted(name, payload, exactMatch), `unexpected query ${name}${_.isNil(payload) ? '' : ' with payload ' + util.inspect(payload)}`).to.be.false;
    }

    /**
     * @public
     * @returns {undefined}
     */
    clear() {
        this._executedCommands = [];
        this._executedQueries = [];
    }

    /**
     * BoundedContext support
     * @internal
     * @param {String} query
     * @param {Object} payload
     * @returns {Promise}
     */
    query(query, payload) {
        this._executedQueries.push({
            name: query,
            payload: payload
        });

        return this.executeHandler(this._queryHandlers, query, payload);
    }

    /**
     * BoundedContext support
     * @internal
     * @param {String} command
     * @param {Object} payload
     * @returns {Promise}
     */
    command(command, payload) {
        this._executedCommands.push({
            name: command,
            payload: payload
        });

        return this.executeHandler(this._commandHandlers, command, payload);
    }
}
