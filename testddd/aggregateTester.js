import _ from 'lodash';
import uuid from 'node-uuid';
import Promise from 'bluebird';
import chai from 'chai';
import chaiSubset from 'chai-subset';
import util from 'util';

chai.use(chaiSubset);

const expect = chai.expect;

export class AggregateTester {
    get generatedEvents() {
        return this._generatedEvents;
    }

    get aggregate() {
        return this._aggregate;
    }

    get aggregateId() {
        return this._aggregate.id;
    }

    /**
     * @constructor
     * @param {Function} aggregateClass
     * @param {Array} [events]
     */
    constructor(aggregateClass, events) {
        this._aggregateClass = aggregateClass;
        this._events = events || [];
        this._generatedEvents = [];
    }

    /**
     * @public
     * @returns {Promise}
     */
    make() {
        this._aggregate = new this._aggregateClass(this, uuid.v4().toString());
        let version = 0;
        return Promise.each(this._events, (event) => {
            return this._aggregate.applyEvent(event.name, event.data, ++version);
        })
        .then(() => {
            return this;
        });
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} [payload]
     * @param {Boolean} [autoSave]
     * @returns {Promise}
     */
    execCommand(name, payload, autoSave = true) {
        let commandResult = undefined;
        return Promise.resolve(this._aggregate[`command${_.upperFirst(name)}`](payload))
            .then((result) => {
                commandResult = result;
                if (autoSave)
                    return this._aggregate.save();
            })
            .then(() => {
                return {
                    result: commandResult,
                    tester: this
                };
            });
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} [data]
     * @return {undefined}
     */
    expectEventGenerated(name, data) {
        expect(this.generatedEvents, `event ${name} should be generated`).to.containSubset([_.omitBy({ name, data }, _.isNil)]);
    }

    /**
     * @public
     * @param {String} name
     * @param {Object} [data]
     * @return {undefined}
     */
    expectEventNotGenerated(name, data) {
        expect(this.generatedEvents, `event ${name} should not be generated`).to.not.containSubset([_.omitBy({ name, data }, _.isNil)]);
    }

    /**
     * @internal
     */
    pushEvents(aggregate, events) {
        const clone = _.cloneDeep(events);
        let version = aggregate.version;
        _.forEach(clone, (event) => {
            event.version = ++version;
        });

        this._generatedEvents = this._generatedEvents.concat(clone);
        return Promise.resolve({
            aggregateVersion: version
        });
    }
}
