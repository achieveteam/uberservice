import Promise from 'bluebird';
import { Domain } from '../domain';
import { MicroService } from '../microService';
import { MockServiceBus } from './mockServiceBus';
import { MockDomainProcessor } from './mockDomainProcessor';

export class MockMicroService extends MicroService {
    static ProcessorDomain = 'com.domain.service';

    /**
     * @override
     */
    constructor(config) {
        super(config);
        this._promises = {};
        this._resolvers = {};
    }

    /**
     * Mock domain processor
     * @returns {MockDomainProcessor}
     */
    get domainProcessor() {
        return this._domainProcessor;
    }

    /**
     * Mock domain
     * @returns {Domain}
     */
    get processorDomain() {
        return this.getDomain(MockMicroService.ProcessorDomain);
    }

    /**
     * Revival state promise
     * @returns {Promise}
     */
    get revivalStatePromise() {
        const promise = this.getPromise('revivalState');

        if (this.stateMachine.states.current === 'revival')
            this._resolvers['revivalState']();

        return promise;
    }

    /**
     * Ready state promise
     * @returns {Promise}
     */
    get readyStatePromise() {
        const promise = this.getPromise('readyState');

        if (this.stateMachine.states.current === 'ready')
            this._resolvers['readyState']();

        return promise;
    }

    /**
     * @private
     */
    getPromise(name) {
        if (!this._promises[name]) {
            this._promises[name] = new Promise((resolve) => {
                this._resolvers[name] = resolve;
            });
        }
        return this._promises[name];
    }

    /**
     * @override
     */
    createBus(config) {
        return new MockServiceBus(config);
    }

    /**
     * @override
     */
    onMonitorStatusQuery(query) {
        return super.onMonitorStatusQuery(query)
            .then((status) => {
                status.allOk = true;
                return status;
            });
    }

    /**
     * @override
     */
    initProcessors() {
        return super.initProcessors()
            .then(() => {
                this._domainProcessor = new MockDomainProcessor();
                return this.useDomainProcessor(MockMicroService.ProcessorDomain, Domain.All, this._domainProcessor);
            });
    }

    /**
     * @override
     */
    enterRevival() {
        return super.enterRevival()
            .then(() => {
                if (this._resolvers['revivalState'])
                    this._resolvers['revivalState']();
            });
    }

    /**
     * @override
     */
    enterReady() {
        return super.enterReady()
            .then(() => {
                if (this._resolvers['readyState'])
                    this._resolvers['readyState']();
            });
    }

    stop(code) { return Promise.resolve(code); }
}