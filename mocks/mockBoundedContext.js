import { BoundedContext } from '../ddd/boundedContext';
import { MockEventStore } from '../mocks/mockEventStore';

export class MockBoundedContext extends BoundedContext {
    /**
     * @returns {MockEventStore}
     */
    get mockEventStore() {
        return this._eventStore;
    }

    constructor() {
        super(new MockEventStore(), null, null, 'mock-bounded-context', 'com.test-domain');
    }
}
