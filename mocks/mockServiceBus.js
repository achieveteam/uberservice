import Promise from 'bluebird';
import _ from 'lodash';
import { UberBus } from '../uberBus';
import { Domain } from '../domain';

export class MockServiceBus extends UberBus {
    /**
     * @public
     * @constructor
     */
    constructor(config = {}) {
        super(config);
        this.sentEvents = [];
        this.sentQueries = [];
        this.sentCommands = [];
        this.sentNotices = [];
        this._domains = {};
    }

    /**
     * @private
     * Returns fixed domain name
     */
    fixDomain(domain) {
        return domain.split('.').join('_').split('-').join('_');
    }

    /**
     * Attach message listener
     */
    listen(domain, type, name, callback) {
        domain = this.fixDomain(domain);
        if (!this._domains[domain])
            this._domains[domain] = {};
        if (!this._domains[domain][type])
            this._domains[domain][type] = {};
        if (_.isFunction(this._domains[domain][type][name]))
            return Promise.reject('duplicate listener');
        this._domains[domain][type][name] = callback;
        return Promise.resolve(`${domain}.${type}.${name}`);
    }

    /** Removes registered listener **/
    removeListener(listener) {
        if (_.get(this._domains, listener))
            delete this._domains[listener];
        return Promise.resolve();
    }


    /**
     * Send command stub. Writes call arguments to sentCommands property;
     * @returns {Promise} - Resolved promise
     */
    sendCommand(domain, name, payload, timeout = 1000) {
        this.sentCommands.push({
            domain: domain,
            name: name,
            payload: payload,
            timeout: timeout
        });

        const responder = _.get(this._domains, `${this.fixDomain(domain)}.${UberBus.Command}.${name}`, null);
        if (responder)
            return responder(payload, timeout);

        return Promise.resolve();
    }

    /**
     * Sending event stub. Writes call arguments to sentEvents property.
     */
    sendEvent(domain, name, payload, version) {
        this.sentEvents.push({
            domain: domain,
            name: name,
            payload: payload,
            version: version
        });

        const responder = _.get(this._domains, `${this.fixDomain(domain)}.${UberBus.Event}.${name}`, null);
        if (responder)
            return responder(payload, version);

        return Promise.resolve();

    }

    /**
     * Send query stub. Writes call arguments to sentQueries property.
     */
    sendQuery(domain, name, payload, filter, timeout) {
        this.sentQueries.push({
            domain: domain,
            name: name,
            payload: payload,
            filter: filter,
            timeout: timeout
        });

        const responder = _.get(this._domains, `${this.fixDomain(domain)}.${UberBus.Query}.${name}`, null);
        if (responder)
            return responder(payload, filter, timeout);

        return Promise.resolve();
    }

    /**
     * Send notice stub. Writes call arguments to sentNotices property.
     */
    sendNotice(domain, name, payload) {
        this.sentNotices.push({
            domain: domain,
            name: name,
            payload: payload
        });

        const responder = _.get(this._domains, `${this.fixDomain(domain)}.${UberBus.Notice}.${name}`, null);
        if (responder)
            return responder(payload, filter, timeout);

        return Promise.resolve();
    }

    /**
     * Returns registered listeners
     */
    getListeners(domain, type) {
        return _.get(this._domains, `${this.fixDomain(domain)}.${type}`, null);
    }

    /**
     * Revival events
     */
    handleRevivalEvent(event) {
        const parts = Domain.splitName(event.name);
        const chain = Promise.resolve();

        const responder = _.get(this._domains, `${this.fixDomain(parts.domain)}.${UberBus.Event}.${parts.name}`, null);

        if (responder)
            chain.then(() => {
                return responder(event.payload);
            });


        if (this._eventSequencer)
            chain.then(() => {
                return this._eventSequencer.setVersion(event.version);
            });

        return chain;
    }


    /**
     * Send notice stub. Writes call arguments to sentNotices property;
     */


    /**
     * Send busConstructed state machine signal. Bus now in waiting state.
     * @public
     */
    emulateBusConstructed() {
        this.stateMachine.states.busConstructed();
    }

    /**
     * Send busConnected state machine signal. Bus now in ready state.
     * @public
     */
    emulateBusConnected() {
        this.stateMachine.states.busConnected();
    }

    /**
     * Send connectionError state machine signal. Bus now in failure state.
     * @public
     */
    emulateConnectionError() {
        this.stateMachine.states.connectionError();
    }

    /**
     * Remove all responders of all types
     */
    clearResponders() {
        this.commandResponders = {};
        this.queryResponders = {};
    }

    // Stubs
    assertConfig() { }
    startAcquire() { }
    acquire(attempt = 0) { }
    dispose() { }
    consume() { }
    cancel() { }
    close() { }
    protectEventHandlerPromise(promise) { }
}