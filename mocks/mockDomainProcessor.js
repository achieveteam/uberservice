import Promise from 'bluebird';

class BaseDomainProcessor {
    processCommandParent(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processQueryParent(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processEventParent(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processNoticeParent(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
}

export class MockDomainProcessor extends BaseDomainProcessor {
    processCommandTestCommand(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processEventTestEvent(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processQueryTestQuery(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
    processNoticeTestNotice(payload) {
        this.payload = payload;
        return Promise.resolve();
    }
}