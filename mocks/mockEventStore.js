import _ from 'lodash';
import Promise from 'bluebird';
import { ConflictError } from '@badgeteam/liberr';
import { EventStore } from '../ddd/eventStore';

export class MockEventStore extends EventStore{
    /**
     * @public
     * @returns {Array}
     */
    get eventQueue() {
        return this._eventQueue;
    }

    /** @public */
    get pushAggregateEventsOverride() {
        return this._pushAggregateEventsOverride;
    }
    set pushAggregateEventsOverride(value) {
        this._pushAggregateEventsOverride = value;
    }

    /**
     * @public
     * @constructor
     */
    constructor() {
        super(null);
        this._eventQueue = [];
        this._pushAggregateEventsOverride = null;
    }

    /**
     * Pushed events to stream
     * @public
     * @param {string} aggregateId
     * @param {number} aggregateVersion
     * @param {Array} events
     * @returns {Promise}
     */
    pushAggregateEvents(aggregateId, aggregateVersion, events) {
        if (_.isFunction(this.pushAggregateEventsOverride)) {
            return this.pushAggregateEventsOverride(aggregateId, aggregateVersion, events);
        } else {
            return this.getAggregateEvents(aggregateId)
                .then((result) => {
                    let storedEvents = result.events;
                    let storeVersion = _.isEmpty(storedEvents) ? 0 : _.last(storedEvents).version;
                    if (storeVersion > aggregateVersion)
                        return ConflictError.promise('store version is greater than aggregate version');

                    _.forEach(events, (event) => {
                        const clone = _.cloneDeep(event);
                        clone.version = ++storeVersion;
                        clone.aggregateId = aggregateId;
                        clone.streamVersion = _.size(this._eventQueue) + 1;
                        this._eventQueue.push(clone);
                    });

                    return {
                        aggregateVersion: storeVersion
                    };
                });
        }
    }

    /**
     * @public
     * @returns {Promise}
     */
    getAggregateEvents(aggregateId) {
        const events = [];

        _.forEach(this._eventQueue, (event) => {
            if (_.isEqual(event.aggregateId, aggregateId))
                events.push(event);
        });

        return Promise.resolve({
            aggregateId: aggregateId,
            events: _.orderBy(events, ['version'], ['asc'])
        });
    }

    /**
     * @public
     * @param {EventStream} stream
     * @returns {Object}
     */
    requestEventStream(stream) {
        let streamIndex = stream.currentPosition;
        let batchSize = _.isNull(stream.batchSize) ? 200 : stream.batchSize;
        let result = [];
        let endOfStream = false;
        do {
            if (streamIndex >= this._eventQueue.length) {
                endOfStream = true;
                break;
            }

            let event = this._eventQueue[streamIndex++];

            if (!_.isNull(stream.eventFilter) && !_.includes(stream.eventFilter, event.name))
                continue;

            result.push(event);

        } while(_.size(result) < batchSize);

        return Promise.resolve({
            startPosition: stream.currentPosition,
            batchSize: stream.batchSize,
            isEndOfStream: endOfStream,
            lastStreamVersion: _.size(this._eventQueue),
            events: result
        });
    }


    /**
     * @public
     */
    reset() {
        this._eventQueue = [];
    }

    /**
     * @public
     * @returns {boolean}
     */
    containEvent(name, aggregateId, data) {
        let founded = false;
        _.forEach(this._eventQueue, (event) => {
            if (_.isEqual(event.name, name) && _.isEqual(event.aggregateId, aggregateId) && _.isEqual(event.eventData, data)) {
                founded = true;
                return false;
            }
        });
        return founded;
    }
}
