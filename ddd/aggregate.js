import _ from 'lodash';
import Promise from 'bluebird';
import { Logger } from '../logger';
import { UberUtils } from '../uberUtils';

export class Aggregate {
    static Scope = null;

    static EventHandlerPrefix = 'handle';

    /**
     * @private
     * @param {string} eventName
     * @returns {string}
     */
    static getEventHandlerName(eventName) {
        return `${this.EventHandlerPrefix}${_.upperFirst(eventName)}`;
    }

    /**
     * @internal
     * @type {string}
     */
    get scope() {
        return this.constructor.Scope;
    }

    /**
     * @public
     * @type {string}
     */
    get id() {
        return this._id;
    }

    /**
     * @public
     * @returns {Number}
     */
    get version() {
        return this._version;
    }

    /**
     * @protected
     * @returns {Logger}
     */
    get logger() {
        return this._logger;
    }

    /**
     * @protected
     * @returns {boolean}
     */
    get isNew() {
        return (this._version === 0);
    }

    /**
     * @internal
     * @param {BoundedContext} context
     * @param {string} id - aggregate id (guid)
     */
    constructor(context, id) {
        if (_.isEmpty(this.constructor.Scope))
            throw new Error('Scope not defined');

        if (_.isEmpty(id))
            throw new Error('Empty aggregate id');

        this._context = context;
        this._eventBuffer = [];
        this._id = id;
        this._version = 0;
        this._logger = new Logger(`${UberUtils.classNameToLogName(this.constructor)}[${this.id}]`);
        this.initialize();
    }

    /**
     * @protected
     * @virtual
     */
    initialize() { }

    /**
     * @protected
     * @param {Object} payload - payload from the event handler
     * @param {*} fields - required field or fields to handle the event
     * @returns {Promise}
     */
    pick(payload, ...fields) {
        return UberUtils.promisePick(payload, ...fields);
    }

    /**
     * @protected
     * @param {string} name - aggregate event name (short)
     * @param {object} [data] - event data
     */
    emitEvent(name, data) {
        this._eventBuffer.push({
            name: name,
            data: _.isEmpty(data) ? { } : data
        });

        this.applyEvent(name, data);
    }

    /**
     * @internal
     * @param {string} name - event name
     * @param {object} data - event data
     * @param {Number} [version] - event version
     */
    applyEvent(name, data, version = null) {
        const eventHandlerName = this.constructor.getEventHandlerName(name);

        if (_.isFunction(this[eventHandlerName])) {
            try {
                this[eventHandlerName](data);
            } catch (error) {
                this.logger.error(`event ${name} handler error: ${error.message}`);
                throw error;
            }
        }

        if (!_.isNull(version))
            this._version = version;
    }

    /**
     * @public
     * @returns {Promise}
     */
    save() {
        if (_.size(this._eventBuffer)) {
            const parcel = this._eventBuffer;
            this._eventBuffer = [];
            return this._context.pushEvents(this, parcel)
                .then((result) => {
                    this._version = _.get(result, 'aggregateVersion', this._version);
                });
        }
        return Promise.resolve();
    }
}
