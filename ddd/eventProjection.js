import _ from 'lodash';
import Promise from 'bluebird';
import { Logger } from '../logger';
import { UberUtils } from '../uberUtils';
import { EventProjectionStateStore } from './eventProjectionStateStore';
import { GenericError } from '@badgeteam/liberr';

export class EventProjection {
    /**
     * @public
     * @returns {*}
     * @constructor
     */
    static getEventCounterName() {
        return `${_.toLower(this.name)}-event-counter`;
    }

    /**
     * @protected
     */
    static getComponents() {
        return [];
    }

    /**
     * Returns current projection event counter
     * @public
     * @returns {Number}
     */
    get eventCounter() {
        return this._eventCounter;
    }

    /**
     * @protected
     * @return {Logger}
     */
    get logger() {
        return this._logger;
    }

    /**
     * @protected
     * @returns {BoundedContext}
     */
    get context() {
        return this._context;
    }

    /**
     * @internal
     * @returns {MongoStateStore|MemoryStateStore}
     */
    get stateStore() {
        return this._stateStore;
    }

    /**
     * @internal
     * @returns {Array}
     */
    get components() {
        return this._components;
    }

    /**
     * @constructor
     * @public
     * [0] {BoundedContext} context
     * [1] {MemoryStateStore|MongoStateStore} stateStore - projection state store instance
     * ... will pass to initialize
     */
    constructor(...args) {
        this._logger = new Logger(`${UberUtils.classNameToLogName(this.constructor)}`);
        this._context = args[0];
        this._stateStore = args[1];
        this._logger.debug(`constructed`);
        this._logger.debug(`initial projection version ${this._eventCounter}`);
        this._projectionStores = {};
        this._components = [];
        this._initializeArgs = _.slice(args, 2);
    }

    /**
     * @internal
     * @returns {Promise}
     */
    build() {
        this.logger.info(`requesting event counter`);
        return this._stateStore.getCounter(this.constructor.getEventCounterName())
            .then((counter) => {
                this._eventCounter = counter;
                this.logger.info(`event counter set to ${counter}`);
                return Promise.map([].concat(this.constructor.getComponents()), (componentClass) => {
                    this.logger.info(`building component ${componentClass.name}`);
                    const component = new componentClass(this);
                    return component.build()
                        .then(() => {
                            this.bindComponentEventHandlers(component);
                            this._components.push(component);
                        });
                });
            })
            .then(() => {
                this.logger.info(`initializing projection`);
                return this.initialize(...this._initializeArgs);
            })
            .then(() => {
                return this;
            });
    }

    /**
     * @private
     */
    bindComponentEventHandlers(component) {
        UberUtils.enumerateAllMethods(component, 'handle', (method) => {
            if (!_.includes(this, method)) {
                Object.defineProperty(this, method, {
                    enumerable: true,
                    configurable: true,
                    value: function (payload) {
                        return component[method](payload);
                    }.bind(this)
                });
            } else {
                return GenericError.logPromise(this._logger, `component ${component.constructor.name} event handler ${method} overlap!`);
            }
        });
    }

    /**
     * Override this method to initialize event projection
     * @protected
     */
    initialize(...args) { }

    /**
     * Returns component instance
     * @protected
     * @param {Function} componentClass
     */
    getComponent(componentClass) {
        return _.find(this._components, (component) => { return _.isEqual(componentClass, component.constructor); });
    }

    /**
     * State store factory
     * @internal
     * @param {string} name
     * @param {Array} [indexes]
     * @param {Object} [component]
     * @returns {EventProjectionStateStore}
     */
    createStore(name, indexes, component = null) {
        const realName = `${this.constructor.name}${_.upperFirst(name)}`;
        let store = _.get(this._projectionStores, realName, null);

        if (!store) {
            store = new EventProjectionStateStore(component || this, this._stateStore, realName, indexes || []);
            _.set(this._projectionStores, realName, store);
        }

        return store;
    }

    /**
     * @protected
     * @param {Object} payload - payload from the event handler
     * @param {*} fields - required field or fields to handle the event
     * @returns Promise<Object>
     */
    pick(payload, ...fields) {
        return UberUtils.promisePick(payload, ...fields);
    }

    /**
     * Used during projection restore procedure
     * @internal
     * @param {Number} counter - new counter value
     * @returns {Promise}
     */
    setEventCounter(counter) {
        this._eventCounter = counter;
        return this._stateStore.setCounter(this.constructor.getEventCounterName(), counter);
    }

    /**
     * Called then revival procedure completed
     * @internal
     * @returns {Promise}
     */
    revived() {
        this._logger.info(`REVIVED to version ${this._eventCounter}`);
        return Promise.resolve();
    }

    /**
     * Called on bounded context wipe procedure. Do not call directly.
     * @internal
     * @returns {Promise}
     */
    resetStates() {
        this._logger.info('reset all states');

        return Promise.all(_.map(this._projectionStores, (value) => {
                return this._stateStore.removeStates(value.name, {}, value.indexes);
            }))
            .then(() => {
                this._logger.info('drop event counter');
                return this.setEventCounter(0);
            });
    }
}
