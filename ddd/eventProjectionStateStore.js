import _ from 'lodash';
import Promise from 'bluebird';

export class EventProjectionStateStore {
    /**
     * @public
     */
    get name() {
        return this._name;
    }

    /**
     * @public
     */
    get indexes() {
        return this._stateIndexes;
    }

    /**
     * @public
     * @constructor
     * @param {EventProjection|EventProjectionComponent} projection
     * @param {MemoryStateStore|MongoStateStore} stateStore
     * @param {string} name
     * @param {Array} stateIndexes
     */
    constructor(projection, stateStore, name, stateIndexes) {
        this._projection = projection;
        this._name = name;
        this._stateStore = stateStore;
        this._stateIndexes = stateIndexes;
    }

    /**
     * @deprecated Use getState instead
     * Load persistent projection state by key
     * @public
     * @param {String} id - projection state key
     * @returns {Promise} of projection state
     */
    loadState(id) {
        return this._stateStore.getState(this._name, id, this._stateIndexes);
    }

    /**
     * @depreceted Use setState instead
     * Persists projection state by key
     * @public
     * @param {String} id
     * @param {Object} state
     * @returns {Promise} of successful saving
     */
    saveState(id, state) {
        return this._stateStore.setState(this._name, id, state, this._stateIndexes)
    }

    /**
     * @deprecated Use patchState with mongo update object instead
     * Updates current state with id using update object
     * @public
     * @param {String} id
     * @param {Object} update
     * @returns {Promise}
     */
    updateState(id, update) {
        return this._stateStore.updateState(this._name, id, { $set: update }, this._stateIndexes);
    }

    /**
     * Persists projection state by key
     * @public
     * @param {String} id
     * @param {Object} state
     * @returns {Promise} of successful saving
     */
    setState(id, state) {
        return this._stateStore.setState(this._name, id, state, this._stateIndexes)
    }

    /**
     * Updates current state with id using update object
     * @public
     * @param {String} id
     * @param {Object} update
     * @returns {Promise}
     */
    patchState(id, update) {
        return this._stateStore.updateState(this._name, id, update, this._stateIndexes);
    }

    /**
     * Updates state with query object using update object
     * @public
     * @param {Object} query
     * @param {Object} update
     * @returns {Promise}
     */
    patchStates(query, update) {
        return this._stateStore.updateStates(this._name, query, update, this._stateIndexes);
    }

    /**
     * Removes state
     * @public
     * @param {String} id
     * @returns {Promise}
     */
    removeState(id) {
        return this._stateStore.removeState(this._name, id, this._stateIndexes);
    }

    /**
     * Removes states by query
     * @public
     * @param {Object} query
     * @returns {Promise}
     */
    removeStates(query) {
        return this._stateStore.removeStates(this._name, query, this._stateIndexes);
    }

    /**
     * Get persistent projection state by key
     * @public
     * @param {String} id - projection state key
     * @returns {Promise} of projection state
     */
    getState(id) {
        return this._stateStore.getState(this._name, id, this._stateIndexes);
    }

    /**
     * Get persistent projection state by queries with params
     * @public
     * @param {Object|undefined} [query] - query (all states by default)
     * @param {Object|undefined} [fields] - fields selector
     * @param {Array|undefined} [sort] - sort fields array (see mongo doc) [{ id: 1}] by default
     * @param {Number|undefined} [offset] - batch offset
     * @param {Number|undefined} [limit] - batch size (zero will be possibly limited by store)
     * @returns {Promise} of states
     */
    getStates(query, fields, sort, offset, limit) {
        return this._stateStore.getStates(this._name, query, fields, sort, offset, limit, this._stateIndexes);
    }

    /**
     * Iterate with callback through all items by query
     * @public
     * @param {Function} callback - iterate callback
     * @param {Object|undefined} [query] - query (all states by default)
     * @param {Object|undefined} [fields] - fields selector
     * @param {Array|undefined} [sort] - sort fields array (see mongo doc) [{ id: 1}] by default
     * @returns {Promise}
     */
    eachState(callback, query, fields, sort) {
        return this._stateStore.eachState(this._name, callback, query, fields, sort, this._stateIndexes);
    }
}

