import _ from 'lodash';
import Promise from 'bluebird';
import { Logger } from '../logger';
import { UberUtils } from '../uberUtils';

export class EventProjectionComponent {
    /**
     * @protected
     * @return {Logger}
     */
    get logger() {
        return this._logger;
    }

    /**
     * @protected
     * @return {EventProjection|*}
     */
    get projection() {
        return this._projection;
    }

    /**
     * @constructor
     * @internal
     * @param {EventProjection} projection
     */
    constructor(projection) {
        this._projection = projection;
        this._logger = new Logger(`${UberUtils.classNameToLogName(this.constructor)}`);
    }

    /**
     * @internal
     */
    build() {
        return Promise.resolve(this.initialize());
    }

    /**
     * @protected
     */
    initialize() {
        return Promise.resolve();
    }

    /**
     * @protected
     * @param {string} name
     * @param {Array} [indexes]
     * @returns {EventProjectionStateStore}
     */
    createStore(name, indexes) {
        return this._projection.createStore(`-${_.toLower(this.constructor.name)}-${name}`, indexes, this);
    }

    /**
     * @protected
     * @param {string} name
     * @param {string} key
     * @param {Object} state
     */
    notifyChanged(name, key, state) {
        if (_.isFunction(this._projection['componentChanged']))
            return this._projection['componentChanged'](name, key, state, this);
    }

    /**
     * @internal
     */
    storeStateChanged(store, key, state) {
        if (_.isFunction(this._projection['componentStateChanged']))
            return this._projection['componentStateChanged'](key, state, this, store);
    }

    /**
     * @protected
     * @param {Object} payload - payload from the event handler
     * @param {*} fields - required field or fields to handle the event
     * @returns Promise<Object>
     */
    pick(payload, ...fields) {
        return UberUtils.promisePick(payload, ...fields);
    }
}
