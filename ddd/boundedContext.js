import _ from 'lodash';
import Promise from 'bluebird';
import { Aggregate } from './aggregate';
import { EventProjection } from './eventProjection';
import { GenericError, RequestTimeoutError, BadRequestError, ConflictError } from '@badgeteam/liberr';
import { Logger } from '../logger';
import { DynamicProcessor } from './dynamicProcessor';
import { UberBus } from '../uberBus';
import { UberUtils } from '../uberUtils';
import { Domain } from '../domain';

export class BoundedContext {
    /** @public **/
    get monitorInfo() {
        _.set(this._monitorInfo, 'busProcessorCount', this._processors.size);
        return this._monitorInfo;
    }

    /** @returns {Boolean} */
    get performanceLogEnabled() {
        return this._performanceLogEnabled;
    }
    /** @param {Boolean} value */
    set performanceLogEnabled(value) {
        this._performanceLogEnabled = value;
    }

    /**
     * @public
     * @constructor
     * @param {EventStore|MockEventStore} eventStore - event store instance
     * @param {MemoryStateStore|MongoStateStore} stateStore - projections state store
     * @param {Function} domainProvider - function returns Domain object by name
     * @param {string} [name] - optional repository name (for logging)
     * @param {string} [scope] - optional context root scope
     */
    constructor(eventStore, stateStore, domainProvider, name = 'bounded-context', scope = 'com.onbadge') {
        this._eventStore = eventStore;
        this._stateStore = stateStore;
        this._domainProvider = domainProvider;
        this._logger = new Logger(name);
        this._processors = new Map();
        this._projections = new Map();
        this._projectionEvents = new Map();
        this._aggregateFactories = new Map();
        this._queryHandlers = new Map();
        this._commandHandlers = new Map();
        this._scope = scope;
        this._performanceLogEnabled = true;
        this._monitorInfo = {
            busProcessorCount: 0,
            boundedCount: {
                aggregates: 0,
                projections: 0,
                sagas: 0
            },
            reviveTime: {
                sagas: {
                    secs: 0,
                    mins: 0,
                    events: 0
                },
                projections: {
                    secs: 0,
                    mins: 0,
                    events: 0
                }
            }
        };
    }

    /**
     * @private
     */
    getProcessor(scope) {
        const globalScope = this.getGlobalScope(scope);
        let processor = this._processors.get(globalScope);
        if (_.isEmpty(processor)) {
            this._logger.debug(`no dynamic event processor for scope ${globalScope}, create one`);
            processor = new DynamicProcessor();
            this._processors.set(globalScope, processor);
        }
        return processor;
    }

    /**
     * Returns scope, type and name by handler name
     * @private
     * @returns {Object}
     */
    scopedNameByHandlerName(handlerName, handlerType, prefix) {
        const regexp = new RegExp(`^${prefix}(.*?)${_.upperFirst(handlerType)}(.*)$`);
        const match = regexp.exec(handlerName);

        if (_.size(match) < 3)
            return null;

        const scope = _.lowerFirst(match[1] || '');
        const message = match[2];

        return {
            scope: scope,
            handlerType: handlerType,
            message: message
        };
    }

    /**
     * @private
     * @param {String} fullName - event full name
     * @param {String} prefix - handler prefix ('handle')
     * @returns {String}
     */
    handlerNameByFullName(fullName, prefix) {
        const parts = Domain.splitName(fullName);
        const localScope = _.upperFirst(_.replace(parts.domain, this._scope + '.', ''));
        const type = _.upperFirst(parts.type);
        const name = _.upperFirst(parts.name);

        return `${prefix}${localScope}${type}${name}`;
    }

    /**
     * @private
     */
    enumerateProjectionMethods(prefix, instance, handlerType, callback) {
        const projectionClass = instance.constructor;

        UberUtils.enumerateAllMethods(instance, prefix, (handlerName) => {
            const scopedName = this.scopedNameByHandlerName(handlerName, handlerType, prefix);

            if (_.isEmpty(scopedName) || _.isEmpty(scopedName.message)) {
                this._logger.warn(`enumerateProjectionMethods: projection class ${projectionClass.name} possibly bad ${handlerType} handler ${handlerName}`);
                return;
            }

            const processor = this.getProcessor(scopedName.scope);

            callback(processor, scopedName.scope, scopedName.message, scopedName.handlerType, handlerName);

            this._logger.debug(`${handlerType} handler ${handlerName} registered for projection ${projectionClass.name}`);
        });
    }

    /**
     * @private
     */
    performOperation(name, type, handlers, payload, timeout) {
        let measureName;
        if (this.performanceLogEnabled) {
            measureName = `${type}-${name}`;
            this._logger.measureStart(measureName);
        }

        payload = payload || {};

        const localHandler = handlers.get(name);
        if (_.isFunction(localHandler)) {
            return Promise.resolve(localHandler(payload))
                .then(result => {
                    if (this.performanceLogEnabled)
                        this._logger.measureEnd(measureName, `${measureName} executed locally in {ms} ms ({s} secs)`);
                    return result;
                });
        }

        const parts = _.reverse(_.split(name, '.'));
        const operationName = _.head(parts);
        const operationScope = _.join(_.reverse(_.tail(parts)), '.');
        const scopeDomain = this._domainProvider(this.getGlobalScope(operationScope));

        if (_.isFunction(scopeDomain[type]))
            return scopeDomain.exec(type, operationName, payload, timeout)
                .then(result => {
                    if (this.performanceLogEnabled)
                        this._logger.measureEnd(measureName, `${measureName} executed remotely in {ms} ms ({s} secs)`);
                    return result;
                });

        this._logger.measureEnd(measureName);

        return GenericError.logPromise(this._logger, `performOperation - bad operation type ${type} or invalid scope domain object`);
    }

    /**
     * @private
     */
    bindProjectionInternal(projectionClass, isSaga, ...initializeArgs) {
        if (_.isNull(projectionClass))
            return GenericError.logPromise(this._logger, 'bindProjection: bad projection class');

        if (!EventProjection.isPrototypeOf(projectionClass))
            return GenericError.logPromise(this._logger, `bindProjection: projection class ${projectionClass.name} is not a subclass of ${EventProjection.name}`);

        if (this._projections.get(projectionClass))
            return GenericError.logPromise(this._logger, `bindProjection: projection class ${projectionClass.name} already related to context`);

        this._logger.debug(`binding projection class ${projectionClass.name}`);

        const instance = new projectionClass(this, this._stateStore, ...initializeArgs);

        this._projections.set(projectionClass, { instance: instance, isSaga: isSaga });
        this._projectionEvents.set(projectionClass, []);

        return instance.build()
            .then(() => {
                // register all events
                this.enumerateProjectionMethods('handle', instance, UberBus.Event, (processor, scope, eventName, handlerType, handlerName) => {
                    processor.addHandler(eventName, handlerType, (payload) => {
                        const eventVersion = _.get(payload, 'streamVersion', null);

                        if (_.toInteger(eventVersion) == 0) {
                            this._logger.warn(`empty event stream version in event handler for projection ${projectionClass.name}`);
                            return Promise.resolve();
                        }

                        const projection = this.getProjection(projectionClass);

                        if (projection.eventCounter >= eventVersion) {
                            this._logger.warn(`event stream version ${eventVersion} less or equal than current projection ${projectionClass.name} version ${projection.eventCounter}, skip handling`);
                            return Promise.resolve();
                        }

                        let result = Promise.resolve(projection[handlerName](payload));

                        return result.then(() => {
                            this._logger.debug(`projection ${projectionClass.name} event handler ${handlerName} invoked successfully, advance projection event counter`);
                            return projection.setEventCounter(eventVersion);
                        }).catch((error) => {
                            this._logger.error(`projection ${projectionClass.name} event handler ${handlerName} error: ${error.message}`);
                            throw error;
                        });
                    });
                    this._projectionEvents.get(projectionClass).push(`${this.getGlobalScope(scope)}.${_.lowerFirst(handlerType)}.${_.lowerFirst(eventName)}`);
                });

                // register all queries
                this.enumerateProjectionMethods('respond', instance, UberBus.Query, (processor, scope, queryName, handlerType, handlerName) => {
                    const handler = (payload) => {
                        return Promise.resolve(this.getProjection(projectionClass)[handlerName](payload))
                            .catch((error) => {
                                this._logger.error(`projection ${projectionClass.name} query handler ${handlerName} error: ${error.message}`);
                                throw error;
                            });
                    };

                    processor.addHandler(queryName, handlerType, handler);

                    const localHandlerName = `${scope}.${_.lowerFirst(queryName)}`;

                    if (_.isFunction(this._queryHandlers.get(localHandlerName)))
                        this._logger.warn(`already have local query handler (${localHandlerName})`);

                    this._queryHandlers.set(localHandlerName, handler);
                    this._logger.debug(`registered local query handler (${localHandlerName})`)
                });

                const prop = isSaga ? 'sagas' : 'projections';
                _.set(this._monitorInfo.boundedCount, prop, _.get(this._monitorInfo.boundedCount, prop) + 1);

                return instance;
            });
    }

    /**
     * Returns global scope
     * @public
     * @param {String} [localScope] - optional local scope to append
     * @returns {String}
     */
    getGlobalScope(localScope) {
        return `${this._scope}${_.isEmpty(localScope) ? '' : '.' + localScope}`;
    }

    /**
     * @public
     * @param {Function} aggregateClass - class of aggregate
     * @param {string} aggregateId - aggregate id
     * @returns {Promise} of aggregate
     */
    getAggregate(aggregateClass, aggregateId) {
        if (!_.isFunction(aggregateClass))
            return GenericError.logPromise(this._logger, `getAggregate: bad aggregate class`);

        if (!Aggregate.isPrototypeOf(aggregateClass))
            return GenericError.logPromise(this._logger, `getAggregate: aggregate class ${aggregateClass.name} is not a subclass of ${Aggregate.name}`);

        let aggregate = null;
        try {
            const factory = this._aggregateFactories.get(aggregateClass);
            if (_.isFunction(factory))
                aggregate = factory(this, aggregateId);
            else
                aggregate = new aggregateClass(this, aggregateId);
        }
        catch(error) {
            return GenericError.logPromise(this._logger, `getAggregate: failed to create aggregate ${aggregateClass.name} instance, error ${error.message}`);
        }

        let eventQueryMeasure, eventApplyMeasure;

        if (this.performanceLogEnabled){
            eventQueryMeasure = `${aggregateClass.name}[${aggregateId}]-event-query`;
            eventApplyMeasure = `${aggregateClass.name}[${aggregateId}]-event-apply`;
            this._logger.measureStart(eventQueryMeasure);
        }

        return this._eventStore.getAggregateEvents(aggregate.id)
            .then((result) => {
                const events = _.get(result, 'events', []);

                if (this.performanceLogEnabled) {
                    this._logger.measureEnd(eventQueryMeasure, `${eventQueryMeasure} ${_.size(events)} events completed in {ms} ms ({s} secs)`);
                    this._logger.measureStart(eventApplyMeasure);
                }

                _.forEach(events, (event) => {
                    const scopedName = _.replace(event.name,`${this.getGlobalScope(aggregate.scope)}.event.`, '');
                    aggregate.applyEvent(scopedName, event.eventData, event.version);
                });

                if (this.performanceLogEnabled)
                    this._logger.measureEnd(eventApplyMeasure, `${eventApplyMeasure} ${_.size(events)} completed in {ms} ms ({s} secs)`);

                return aggregate;
            });
    }

    /**
     * @public
     * @param {Function} aggregateClass - aggregate class (constructor)
     * @param {Function} [factory] - aggregate factory function
     * @returns {Promise}
     */
    bindAggregate(aggregateClass, factory = null) {
        if (_.isNull(aggregateClass))
            return GenericError.logPromise(this._logger, 'bindAggregate: bad aggregate class');

        if (!Aggregate.isPrototypeOf(aggregateClass))
            return GenericError.logPromise(this._logger, `bindAggregate: aggregate class ${aggregateClass.name} is not a subclass of ${Aggregate.name}`);

        this._logger.debug(`binding aggregate class ${aggregateClass.name}`);

        const processor = this.getProcessor(aggregateClass.Scope);
        const instance = new aggregateClass(this, 'enumerate_class_members');

        UberUtils.enumerateClassMethods(instance, 'command', (handlerName, commandName) => {
            const handler = (payload) => {
                const aggregateId = _.get(payload, 'aggregateId', null);
                if (_.isEmpty(aggregateId))
                    return GenericError.logPromise(this._logger, `empty aggregateId in command handler for aggregate class ${aggregateClass.name}`);

                let eventPushConflictCount = 0;
                const eventPushConflictMax = 7;

                const performAggregateCommand = () => {
                    return this.getAggregate(aggregateClass, aggregateId)
                        .then((aggregate) => {
                            let commandResult = null;
                            let measureName;
                            if (this.performanceLogEnabled) {
                                measureName = `${aggregateClass.name}[${aggregateId}]-push-events`;
                                this._logger.measureStart(measureName);
                            }
                            return Promise.resolve(aggregate[handlerName](payload)).then((result) => {
                                    commandResult = result || {};
                                    return aggregate.save();
                                })
                                .then(() => {
                                    if (this.performanceLogEnabled)
                                        this._logger.measureEnd(
                                            measureName,
                                            `${measureName} completed in {ms} ms ({s} secs) with ${eventPushConflictCount} conflicts`
                                        );
                                    return commandResult;
                                })
                                .catch((error) => {
                                    if ((error instanceof ConflictError) &&
                                        (_.get(error, 'details.eventPushConflict', false)) &&
                                        (++eventPushConflictCount <= eventPushConflictMax)) {
                                        this._logger.warn(`${aggregateClass.name} (${aggregateId}) ` +
                                            `- command [${commandName}] conflict (retries left ${eventPushConflictMax - eventPushConflictCount}): ` +
                                            `${error.message}`);
                                        return performAggregateCommand();
                                    }
                                    this._logger.error(`${aggregateClass.name} (${aggregateId}) - command [${commandName}]: ${error.message}`);
                                    throw error;
                                });
                        });
                };

                return performAggregateCommand();
            };

            processor.addHandler(commandName, UberBus.Command, handler);
            this._logger.debug(`handler ${handlerName} registered for aggregate ${aggregateClass.name}`);

            const localHandlerName = `${aggregateClass.Scope}.${_.lowerFirst(commandName)}`;

            if (_.isFunction(this._commandHandlers.get(localHandlerName)))
                this._logger.warn(`already have local command handler (${localHandlerName})`);

            this._commandHandlers.set(localHandlerName, handler);
            this._logger.debug(`registered local command handler (${localHandlerName})`);

        });

        if (!_.isNil(factory))
            this._aggregateFactories.set(aggregateClass, factory);

        this._monitorInfo.boundedCount.aggregates++;

        return Promise.resolve();
    }

    /**
     * @public
     * @param {Function} projectionClass - projection class (constructor)
     * @param {*} [initializeArgs] - projection additional initialization arguments
     * @returns {Promise}
     */
    bindProjection(projectionClass, ...initializeArgs) {
        return this.bindProjectionInternal(projectionClass, false, ...initializeArgs);
    }

    /**
     * @public
     * @param {Function} sagaClass - saga class (constructor)
     * @param {*} [initializeArgs] - saga additional initialization arguments
     * @returns {Promise}
     */
    bindSaga(sagaClass, ...initializeArgs) {
        return this.bindProjectionInternal(sagaClass, true, ...initializeArgs);
    }

    /**
     * @public
     */
    getProjection(projectionClass) {
        return _.get(this._projections.get(projectionClass), 'instance', null);
    }

    /**
     * @public
     * @returns {Array} of { scope, processor } objects
     */
    getProcessors() {
        const result = [];

        this._processors.forEach((value, key) => {
            result.push({
                scope: key,
                processor: value
            });
        });

        return result;
    }

    /**
     * Projection restore loop
     * @private
     * @param {EventProjection} projection
     * @param {boolean} isSaga
     * @returns {Promise}
     */
    reviveProjection(projection, isSaga) {
        this._logger.info(`start reviving projection ${projection.constructor.name}, event filter (${_.join(this._projectionEvents.get(projection.constructor), ', ')})`);
        const eventStream = this._eventStore.getEventStream(projection.eventCounter, this._projectionEvents.get(projection.constructor));

        const restoreFunc = () => {
            return eventStream.readBatch()
                .then((result) => {
                    if (_.size(result) > 0) {
                        this._logger.debug(`applying ${_.size(result)} events from stream to projection ${projection.constructor.name}`);

                        return Promise.each(result, (event) => {
                            const handlerName = this.handlerNameByFullName(event.name, 'handle');

                            if (_.isFunction(_.get(projection, handlerName, null))) {
                                let attempt = 0;
                                const maxAttempts = 20;
                                const delay = 500;

                                const makeAttempt = (event) => {
                                    attempt++;
                                    if (attempt >= maxAttempts) {
                                        this._logger.warn(`saga ${projection.constructor.name} event ${event.name} handler exceeded max recoverable attempts ${maxAttempts}, skipping event`);
                                        return false;
                                    }
                                    this._logger.warn(`assume recoverable error, next attempt #${attempt} after ${delay} msecs`);
                                    return true;
                                };

                                const retryFunc = () => {
                                    return Promise.resolve(projection[handlerName](event))
                                        .catch(RequestTimeoutError, ConflictError, (error) => {
                                            if (isSaga) {
                                                this._logger.warn(`saga ${projection.constructor.name} event ${event.name} handler was rejected with error (${error.message})`);
                                                if (makeAttempt())
                                                    return Promise.delay(delay).then(retryFunc());
                                            }
                                            throw error;
                                        })
                                        .catch((error) => {
                                            this._logger.warn(`projection ${projection.constructor.name} event ${event.name} handler was rejected on revive with error (${error.message})`);
                                            if (!isSaga)
                                                throw error;
                                        });
                                };
                                return retryFunc();
                            }
                        }).then(() => {
                            _.get(this._monitorInfo.reviveTime, isSaga ? 'sagas' : 'projections').events += _.size(result);
                        });
                    }
                })
                .then(() => {
                    if (projection.eventCounter != eventStream.currentPosition) {
                        this._logger.debug(`set projection ${projection.constructor.name} event counter to ${projection.eventCounter}`);
                        return projection.setEventCounter(eventStream.currentPosition);
                    }
                })
                .then(() => {
                    if (eventStream.isEndOfStream) {
                        return projection.setEventCounter(eventStream.lastVersion)
                            .then(() => {
                                this._logger.debug(`projection ${projection.constructor.name} event counter set to ${projection.eventCounter} (last stream version)`);
                                return projection.revived();
                            });
                    }
                    return restoreFunc();
                })
                .catch(RequestTimeoutError, (error) => {
                    const delay = 1000;
                    this._logger.warn(`projection ${projection.constructor.name} event stream request timeout error, assume event store down, retry in ${delay} msecs`);
                    return new Promise((resolve) => {
                        setTimeout(() => {
                            resolve(restoreFunc());
                        }, delay);
                    });
                });
        };

        return restoreFunc();
    }

    /**
     * @private
     * @returns {Promise}
     */
    promiseReviveAll(isSaga) {
        const promises = [];
        this._projections.forEach((instanceInfo) => {
            if (instanceInfo.isSaga == isSaga)
                promises.push(this.reviveProjection(instanceInfo.instance, isSaga));
        });

        const measureName = isSaga ? 'sagas' : 'projections';
        this._logger.measureStart(measureName);
        return Promise.all(promises)
            .then(() => {
                const reviveTime = this._logger.measureEnd(
                    measureName,
                    `all ${measureName} revive promises resolved in {s} secs ({min} mins)`
                );

                const timings = _.get(this._monitorInfo.reviveTime, measureName);
                timings.secs = reviveTime.s;
                timings.mins = reviveTime.min;
            });
    }

    /**
     * @public
     * @returns {Promise}
     */
    reviveProjections() {
        return Promise.each([false, true], (isSaga) => this.promiseReviveAll(isSaga));
    }

    /**
     * @public
     * @param {boolean} includeSagas - reset sagas too
     * @returns {Promise}
     */
    resetProjections(includeSagas = false) {
        const promises = [];
        this._projections.forEach((instanceInfo) => {
            if (!instanceInfo.isSaga || (instanceInfo.isSaga && includeSagas)) {
                this._logger.warn(`reset ${instanceInfo.isSaga ? 'saga' : 'projection'} ${instanceInfo.instance.constructor.name} states!`);
                promises.push(instanceInfo.instance.resetStates());
            }
        });
        return Promise.all(promises);
    }

    /**
     * Sends query to specified projection. If no projection in this context query will be redirected to bus.
     * @public
     * @param {String} query - query (e.g. users.getUser)
     * @param {Object} [payload] - query payload
     * @param {Integer} [timeout] - operation timeout
     * @returns {Promise}
     */
    query(query, payload, timeout) {
        return this.performOperation(query, UberBus.Query, this._queryHandlers, payload, timeout);
    }

    /**
     * @public
     * @param {String} command - command name (e.g. user.create)
     * @param {Object} [payload] - command payload
     * @param {Integer} [timeout] - operation timeout
     */
    command(command, payload, timeout) {
        return this.performOperation(command, UberBus.Command, this._commandHandlers, payload, timeout);
    }

    /**
     * Pushes events to event store
     * @internal
     * @param {Aggregate|*} aggregate - source of events
     * @param {Array} events - generated events array
     * @returns {Promise}
     */
    pushEvents(aggregate, events) {
        if (_.size(events) > 0) {
            const parcel = [];

            _.forEach(events, (event) => {
                parcel.push({
                    name: `${this.getGlobalScope(aggregate.scope)}.event.${event.name}`,
                    eventData: event.data
                })
            });

            return this._eventStore.pushAggregateEvents(aggregate.id, aggregate.version, parcel);
        }
        return Promise.resolve();
    }
}
