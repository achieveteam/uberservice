import _ from 'lodash';
import Promise from 'bluebird';
import Nedb from 'nedb';

export class MemoryStateStore {

    /**
     * @public
     * @constructor
     */
    constructor() {
        this._collections = new Map();
        this._stateIndexes = new Map();

        this._counters = new Nedb();
        this._counters.ensureIndex({ fieldName: 'name', unique: true });
    }

    /**
     * @public
     * @param {String} name
     * @returns {Nedb}
     */
    getCollection(name) {
        return this._collections.get(name);
    }

    /**
     * @private
     */
    ensureCollection(name, indexes) {
        return new Promise((resolve, reject) => {
            let collection = this._collections.get(name);
            if (_.isNil(collection)) {
                collection = new Nedb();
                // if (_.isArray(indexes)) {
                //     this._stateIndexes.set(name, indexes);
                //     _.forEach(indexes, (entry) => {
                //         _.forEach(entry.index, (value, key) => {
                //             collection.ensureIndex({fieldName: key, /* unique: _.get(entry, 'options.unique', false) */ unique: false}, (err) => {
                //                 if (err)
                //                     return reject(err);
                //             });
                //         });
                //
                //     });
                // }
                this._collections.set(name, collection);
            }
            return resolve(collection);
        });
    }

    /**
     * @internal
     * @param {String} name - counter name
     * @returns {Promise}
     */
    getCounter(name) {
        return new Promise((resolve, reject) => {
            this._counters.find({name: name}, {value: 1}, (err, docs) => {
                if (err)
                    return reject(err);

                if (_.size(docs) > 0)
                    return resolve(docs[0].value);

                return resolve(0);
            });
        });
    }

    /**
     * @internal
     * @param {String} name - counter name
     * @param {Number} value - counter value
     * @returns {Promise}
     */
    setCounter(name, value) {
        return new Promise((resolve, reject) => {
            this._counters.update({ name: name }, { $set: { value: value } }, { upsert: true }, (err) => {
                if (err)
                    return reject(err);
                return resolve(value);
            })
        });
    }

    /**
     * @public
     */
    getStateIndexes(name) {
        return this._stateIndexes.get(name);
    }

    /**
     * @internal
     * @param {String} name
     * @param {String} id
     * @param {Object} state
     * @param {Array} [stateIndexes]
     */
    setState(name, id, state, stateIndexes) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    const updatedDoc = _.merge({ id }, state);
                    collection.update({ id }, updatedDoc, { upsert: true }, (err) => {
                        if (err)
                            return reject(err);
                        return resolve(updatedDoc);
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {String} id
     * @param {Array} [stateIndexes]
     * @returns {Promise}
     */
    getState(name, id, stateIndexes) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    collection.findOne({ id }, { _id: 0 }, (err, doc) => {
                        if (err)
                            return reject(err);
                        return resolve(doc || { id });
                    });
                });
            });
    }

    /**
     * Returns state list by sort fields with offset and limit
     * @internal
     * @param {String} name - object name (class name)
     * @param {Object} [query] - mongo query criteria
     * @param {Object} [fields] - fields selector
     * @param {Array|Object} [sort] - sorting fields (see mongo doc)
     * @param {Number} [offset] - states batch offset
     * @param {Number} [limit] - states batch size
     * @param {Array} [stateIndexes] - additional state indexes
     * @return {Promise} of state array
     */
    getStates(name, query={}, fields={}, sort={ id: 1 }, offset=0, limit=100, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    let chain = collection.find(query, { _id: 0, ...fields });

                    chain = chain.sort(sort);

                    if (_.isNumber(offset))
                        chain = chain.skip(offset);

                    if (_.isNumber(limit))
                        chain = chain.limit(limit);

                    chain.exec((err, docs) => {
                        if (err)
                            return reject(err);
                        return resolve(docs);
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {String} id
     * @param {Array} [stateIndexes]
     */
    removeState(name, id, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    collection.remove({ id }, { }, (err) => {
                        if (err)
                            return reject(err);
                        return resolve();
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {Object} [query]
     * @param {Array} [stateIndexes]
     */
    removeStates(name, query={}, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    collection.remove(query, { multi: true }, (err) => {
                        if (err)
                            return reject(err);
                        return resolve();
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {String} id
     * @param {Object} [update]
     * @param {Array} [stateIndexes]
     */
    updateState(name, id, update={}, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    collection.update({ id }, { $set: { id }, ...update }, { upsert: true }, (err) => {
                        if (err)
                            return reject(err);
                        return resolve();
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {Object} [query]
     * @param {Object} [update]
     * @param {Array} [stateIndexes]
     */
    updateStates(name, query={}, update={}, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) => {
                return new Promise((resolve, reject) => {
                    collection.update(query, update, { multi: true }, (err) => {
                        if (err)
                            return reject(err);
                        return resolve();
                    });
                });
            });
    }

    /**
     * @internal
     * @param {String} name
     * @param {Function} callback
     * @param {Object} [query]
     * @param {Object} [fields]
     * * @param {Object} [sort]
     * @param {Array} [stateIndexes]
     */
    eachState(name, callback, query={}, fields={}, sort={}, stateIndexes=[]) {
        return this.ensureCollection(name, stateIndexes)
            .then((collection) =>
                new Promise((resolve, reject) => collection.find(query, { _id: 0, ...fields }).sort(sort).exec((err, docs) => {
                    if (err)
                        return reject(err);
                    return resolve(Promise.each(docs, (doc) => Promise.resolve(callback(doc))));
                }))
            );
    }
}
