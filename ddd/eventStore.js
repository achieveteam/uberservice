import _ from 'lodash';
import Promise from 'bluebird';

export class EventStream {
    /**
     * Current stream position
     * @returns {Number}
     */
    get currentPosition() {
        return this._currentPosition;
    }

    /**
     * Stream batch size
     * @returns {Number}
     */
    get batchSize() {
        return this._batchSize;
    }

    /**
     * Event filter
     * @returns {Array}
     */
    get eventFilter() {
        return this._eventFilter;
    }

    /**
     * End of event stream flag
     * @returns {boolean}
     */
    get isEndOfStream() {
        return this._isEndOfStream;
    }

    /**
     * Last stream event version
     * @returns {Number}
     */
    get lastVersion() {
        return this._lastVersion;
    }

    /**
     * @constructor
     * @public
     * @param {EventStore} eventStore
     * @param {Number} startPosition
     * @param {Number} batchSize
     * @param {Array} eventFilter
     */
    constructor(eventStore, startPosition, batchSize, eventFilter) {
        this._eventStore = eventStore;
        this._currentPosition = startPosition;
        this._batchSize = batchSize;
        this._eventFilter = eventFilter;
        this._isEndOfStream = false;
        this._lastVersion = NaN;
    }

    /**
     * Returns batch of events from event store
     * @public
     * @returns {Promise} of events array
     */
    readBatch() {
        if (this.isEndOfStream)
            return Promise.resolve([]);

        return this._eventStore.requestEventStream(this)
            .then((response) => {
                const events = _.get(response, 'events', []);

                this._isEndOfStream = response.isEndOfStream;
                this._lastVersion = response.lastStreamVersion;

                if (_.size(events) > 0)
                    this._currentPosition = _.last(events).streamVersion;
                else
                    this._currentPosition = this._lastVersion;

                return events;
            });
    }
}


export class EventStore {
    /**
     * @public
     * @returns {Domain}
     */
    get eventStoreDomain() {
        return this._eventStoreDomain;
    }

    /**
     * @public
     * @constructor
     * @param {Domain} eventStoreDomain - event store domain name to bind
     */
    constructor(eventStoreDomain) {
        this._eventStoreDomain = eventStoreDomain;
    }

    /**
     * Pushed events to stream
     * @public
     * @param {string} aggregateId
     * @param {number} aggregateVersion
     * @param {Array} events
     * @returns {Promise}
     */
    pushAggregateEvents(aggregateId, aggregateVersion, events) {
        return this._eventStoreDomain.command('pushAggregateEvents', {
            aggregateId: aggregateId,
            aggregateVersion: aggregateVersion,
            events: events
        });
    }

    /**
     * Returns aggregate events sorted by version
     * @public
     * @param {string} aggregateId
     * @return {Promise} of events
     */
    getAggregateEvents(aggregateId) {
        return this._eventStoreDomain.query('getAggregateEvents', { aggregateId: aggregateId });
    }

    /**
     * Returns event stream
     * @public
     * @param {Number} startPosition
     * @param {Number} batchSize
     * @param {Array} eventFilter
     * @returns {EventStream}
     */
    getEventStream(startPosition, eventFilter = null, batchSize = null) {
        return new EventStream(this, startPosition, batchSize, eventFilter);
    }

    /**
     * @internal
     * @param {EventStream} stream
     * @returns {Promise}
     */
    requestEventStream(stream) {
        const queryParams = {
            startPosition: stream.currentPosition
        };
        if (!_.isNull(stream.batchSize))
            queryParams.batchSize = stream.batchSize;

        if (!_.isNull(stream.eventFilter))
            queryParams.eventFilter = stream.eventFilter;

        return this._eventStoreDomain.query('getEventStream', queryParams);
    }
}
