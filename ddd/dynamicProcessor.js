import _ from 'lodash';
import EventEmitter from 'events';
import { UberBus, Domain, GenericError, Logger } from '../index';

export class DynamicProcessor {
    /**
     * @public
     * @constructor
     */
    constructor() {
        this._eventEmitter = new EventEmitter();
        this._logger = new Logger('dynamic-processor');
        this._broadcastMap = new Map();
    }

    /**
     * @private
     */
    getHandlerName(name, type) {
        return `process${_.upperFirst(type)}${_.upperFirst(name)}`;
    }

    /**
     * @private
     */
    broadcast(handlerName, payload) {
        const handlers = this._broadcastMap.get(handlerName);
        const promises = [];

        if (_.size(handlers) == 1)
            return handlers[0](payload);

        _.forEach(handlers, (handler) => {
            promises.push(handler(payload));
        });

        return Promise.all(promises);
    }

    /**
     * @public
     * @param {string} name - operation name
     * @param {string} type - operation type (Event, Command, etc)
     * @param {Function} handler
     */
    addHandler(name, type, handler) {
        const handlerName = this.getHandlerName(name, type);

        if (!_.includes(this, handlerName))
            Object.defineProperty(this, handlerName, {
                enumerable: true,
                configurable: true,
                value: function(payload) {
                    return this.broadcast(handlerName, payload);
                }.bind(this)
            });

        let list = this._broadcastMap.get(handlerName);

        if (_.isEmpty(list)) {
            list = [];
            this._broadcastMap.set(handlerName, list);
        }

        list.push(handler);
    }

    /**
     * @public
     * @param {string} name - operation name
     * @param {string} type - operation type
     * @param {Function} handler
     */
    removeHandler(name, type, handler) {
        const handlerName = this.getHandlerName(name, type);

        let list = this._broadcastMap.get(handlerName);

        if (!_.isEmpty(list)) {
            list = _.remove(list, handler);
            this._broadcastMap.set(handlerName, list);
        }

        if (_.size(list) == 0)
            delete this[handlerName];
    }
}
