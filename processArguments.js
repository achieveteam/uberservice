import _ from 'lodash';

export class ProcessArguments {
    get namedArguments() {
        return this._namedArguments;
    }
    get singleArguments() {
        return this._singleArguments;
    }

    constructor() {
        this._namedArguments = {};
        this._singleArguments = [];

        let currentKey = '';

        process.argv.forEach((val, index, array) => {
            if (index > 1) {
                if (_.startsWith(val, '-')) {
                    currentKey = _.trimStart(val, '-');
                    this._namedArguments[currentKey] = true;
                } else {
                    if (currentKey) {
                        this._namedArguments[currentKey] = val;
                        currentKey = '';
                    } else {
                        this._singleArguments.push(val);
                    }
                }
            }
        });
    }
}
