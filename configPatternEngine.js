import _ from 'lodash';
import { Logger } from './logger';


export class ConfigPatternEngine {
    constructor() {
        this._logger = new Logger('config-pattern-engine');
    }

    processEnvironment(value) {
        let regexp = /%env.(.*?)%/g;
        let envName = regexp.exec(value)[1];
        let envVal = process.env[envName];
        if (!envVal) {
            this._logger.error('environment variable ' + envName + ' not exist or has empty value! Passing !!!' + envName + ' undefined!!! to config');
            this._logger.error('wrong configuration can lead to unexpected behaviour!');
            envVal = envName + ' undefined';
        }
        return envVal;
    }

    processPattern(pattern) {
        let regexp = /(%.*?%)/gi;
        let matches = [];
        let match;

        while ((match = regexp.exec(pattern))) {
            matches.push(match[1]);
        }

        let result = pattern;

        _.forEach(matches, (value) => {
            if (value.match(/env.*/)) {
                let env = this.processEnvironment(value);
                result = result.replace(value, env);
                this._logger.debug('%s value was set', value);
            }
        });

        return result;
    }

    processObject(object) {
        _.forIn(object, (value, key, obj) => {
            if (_.isString(value))
                obj[key] = this.processPattern(value);
            else
                this.processObject(value);
        });
        return object;
    }

    process(config) {
        this._logger.debug('processing config');
        return this.processObject(_.cloneDeep(config));

    }
}
