import _ from 'lodash';
import * as chai from 'chai';
import { MockBoundedContext } from '../mocks/mockBoundedContext';
import { Aggregate } from '../ddd/aggregate';

let expect = chai.expect;
chai.expect();

class FakeAggregate extends Aggregate {
    static Scope = 'fake';

    initialize() {
        this.initialized = true;
    }

    doOperation() {
        this.emitEvent('operationPerformed', { name: 'name' });
    }

    doEmptyData() {
        this.emitEvent('emptyData', null);
    }
    
    doPickNested(payload) {
        return this.pick(payload, ['nested.name', 'root'])
            .then((obj) => {
                this.emitEvent('pickNested', obj);
            });
    }

    handleOperationPerformed(data) {
        this.name = data.name;
    }

    handleEmptyData(data) {
        this.emptyData = data;
    }
}


describe('Aggregate', () => {
    /** @type {MockBoundedContext} **/
    let context = null;
    /** @type {FakeAggregate} **/
    let aggregate = null;

    beforeEach(() => {
        context = new MockBoundedContext();
        return context.getAggregate(FakeAggregate, 'id')
            .then((instance) => {
                aggregate = instance;
            });
    });

    it('initialize called during construction', () => {
        expect(aggregate.initialized).to.be.true;
    });

    it('properties', () => {
        expect(aggregate.id).to.be.equal('id');
        expect(aggregate.scope).to.be.equal('fake');
        expect(aggregate.version).to.be.equal(0);
    });

    it('events emitting', () => {
        aggregate.doOperation();
        expect(aggregate.name).to.be.equal('name');
    });

    it('push buffered events on save', () => {
        aggregate.doOperation();
        aggregate.doEmptyData();
        return aggregate.save()
            .then(() => {
                expect(context.mockEventStore.containEvent('com.test-domain.fake.event.operationPerformed', 'id', { name: 'name' })).to.be.true;
                expect(context.mockEventStore.containEvent('com.test-domain.fake.event.emptyData', 'id', { })).to.be.true;
            });
    });

    it('no empty event data', () => {
        aggregate.doEmptyData();
        return aggregate.save()
            .then(() => {
                expect(context.mockEventStore.containEvent('com.test-domain.fake.event.emptyData', 'id', { })).to.be.true;
            });
    });

    it('new aggregate version on save', () => {
        expect(aggregate.version).to.be.equal(0);
        aggregate.doOperation();
        expect(aggregate.version).to.be.equal(0);
        return aggregate.save()
            .then(() => {
                expect(aggregate.version).to.be.equal(1);
            });
    });

    it('pick nested props', () => {
        return aggregate.doPickNested({
                root: 'root',
                junk: 'junk',
                nested: {
                    name: 'name',
                    junk: 'junk'
                }
            })
            .then(() => { return aggregate.save(); })
            .then(() => {
                expect(context.mockEventStore.containEvent('com.test-domain.fake.event.pickNested', 'id', {
                    root: 'root',
                    nested: {
                        name: 'name'
                    }
                })).to.be.true;
            });
    });
});
