import * as chai from 'chai';
import { ConfigPatternEngine } from '../configPatternEngine';


let expect = chai.expect;
chai.expect();

describe('ConfigPatternEngine', () => {
    let engine = null;

    beforeEach(() => {
        engine = new ConfigPatternEngine();
    });

    describe('environment variables patterns', () => {
        let sourceConfig = {
            url: 'ampq://%env.TEST_HOST_NAME%:%env.TEST_HOST_PORT%',
            number: 124,
            boolean: true,
            nested: {
                prop: 'the %env.TEST_NESTED_PROP% is ok',
                number: 124
            }
        };

        beforeEach(() => {
            process.env.TEST_HOST_NAME = 'test-host-name';
            process.env.TEST_HOST_PORT = '12345';
            process.env.TEST_NESTED_PROP = 'some-nested-prop';
        });


        it('url', () => {
            let config = engine.process(sourceConfig);

            expect(config.url).to.be.equal('ampq://' + process.env.TEST_HOST_NAME + ':' + process.env.TEST_HOST_PORT);
        });

        it('nested', () => {
            let config = engine.process(sourceConfig);

            expect(config.nested.prop).to.be.equal('the ' + process.env.TEST_NESTED_PROP + ' is ok');
        });
    });
    describe('bug with mongo', () => {
        let sourceConfig = {
            microservice : {
                name: 'event-store',
                debugName: 'event-store'
            },
            bus: {
                debugName: 'event-store-bus',
                url: 'amqp://%env.DOCKER_HOST_IP%',
                socketOptions: {}
            },
            remoteLogger: {
                token: '29145e75-258d-39af-8b12-2bac59f2a2ca'
            },
            db: {
                url: 'mongodb://%env.DOCKER_HOST_IP%/eventstore',
                reconnectInterval: 1000
            }
        };

        beforeEach(() => {
            process.env.DOCKER_HOST_IP = 'test-host-name';
        });

        it('db', () => {
            let config = engine.process(sourceConfig);

            expect(config.db.url).to.be.equal('mongodb://test-host-name/eventstore');
        });
    });

});

