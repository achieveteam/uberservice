import _ from 'lodash';
import Promise from 'bluebird';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { Environment } from '../environment';

let expect = chai.expect;
chai.use(chaiAsPromised);
chai.expect();

describe('Environment', () => {
    let currentNodeEnv = null;

    beforeEach(() => {
        currentNodeEnv = process.env.NODE_ENV;
    });

    afterEach(() => {
        process.env.NODE_ENV = currentNodeEnv;
    });

    it('init', () => {
        delete process.env.NODE_ENV;
        Environment.init();
        expect(Environment.isDevelopment).to.be.true;
    });

    it('init in production', () => {
        process.env.NODE_ENV = 'production';
        Environment.init();
        expect(Environment.isDevelopment).to.be.false;
        expect(Environment.isProduction).to.be.true;
    });

    it('isProduction', () => {
        process.env.NODE_ENV = 'production';
        expect(Environment.isProduction).to.be.true;
        process.env.NODE_ENV = 'staging';
        expect(Environment.isProduction).to.be.false;
    });

    it('isDevelopment', () => {
        process.env.NODE_ENV = 'development';
        expect(Environment.isDevelopment).to.be.true;
        process.env.NODE_ENV = 'staging';
        expect(Environment.isDevelopment).to.be.false;
    });

    it('nodeEnv', () => {
        process.env.NODE_ENV = 'development';
        expect(Environment.nodeEnv).to.be.equal('development');
        process.env.NODE_ENV = 'staging';
        expect(Environment.nodeEnv).to.be.equal('staging');
    });
});
