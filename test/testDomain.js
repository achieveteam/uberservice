import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import _ from 'lodash';
import Promise from 'bluebird';
import { Domain } from '../domain';
import { GatewayTimeoutError } from '@badgeteam/liberr';
import { MockServiceBus } from '../mocks/mockServiceBus';
import { MockDomainProcessor } from '../mocks/mockDomainProcessor';

let expect = chai.expect;
chai.use(chaiAsPromised);
chai.expect();

describe('Domain', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {Domain} **/
    let domain = null;
    let domainName = 'com.name.root.sub1.sub2';

    beforeEach(() => {
        bus = new MockServiceBus({});
        domain = new Domain(bus, domainName);
        bus.emulateBusConstructed();
        bus.emulateBusConnected();
    });

    describe('construction', () => {
        it('properties', () => {
            expect(domain.name).to.be.equal(domainName);
        });
    });

    describe('sending messages', () => {
        it('command', () => {
            let timeout = 1234;

            domain.command('testCommand', {
                a: 'a'
            }, timeout);

            expect(bus.sentCommands.length).to.be.equal(1);

            let busCall = bus.sentCommands[0];

            expect(busCall.domain).to.be.equal(domain.name);
            expect(busCall.name).to.be.equal('testCommand');
            expect(busCall.timeout).to.be.equal(timeout);
            expect(busCall.payload).to.be.eql({ a: 'a' });
        });
        it('query', () => {
            let filter = function() { };
            let timeout = 1234;

            domain.query('testQuery', {
                a: 'a'
            }, filter, timeout);

            expect(bus.sentQueries.length).to.be.equal(1);

            let busCall = bus.sentQueries[0];

            expect(busCall.domain).to.be.equal(domain.name);
            expect(busCall.name).to.be.equal('testQuery');
            expect(busCall.filter).to.be.equal(filter);
            expect(busCall.timeout).to.be.equal(timeout);
            expect(busCall.payload).to.be.eql({ a: 'a' });
        });
        it('notice', () => {
            let filter = function() { };
            let timeout = 1234;

            domain.notice('testNotice', {
                a: 'a'
            });

            expect(bus.sentNotices.length).to.be.equal(1);

            let busCall = bus.sentNotices[0];

            expect(busCall.domain).to.be.equal(domain.name);
            expect(busCall.name).to.be.equal('testNotice');
            expect(busCall.payload).to.be.eql({ a: 'a' });
        });
        it('event', () => {
            domain.event('testEvent', {
                a: 'a'
            });

            expect(bus.sentEvents.length).to.be.equal(1);

            let busCall = bus.sentEvents[0];

            expect(busCall.domain).to.be.equal(domain.name);
            expect(busCall.name).to.be.equal('testEvent');
            expect(busCall.version);
            expect(busCall.payload).to.be.eql({ a: 'a' });
        });
        it('event incompatible version type', () => {
            domain.event('testEvent', {
                a: 'a'
            }, '2');

            let busCall = bus.sentEvents[0];
            expect(busCall.version).to.not.exist;
        });
    });

    describe('repeatQuery', () => {
        let repeats;

        beforeEach(() => {
            repeats = 2;
            bus.listen(domainName, MockServiceBus.Query, 'repeatedQuery', () => {
                if (--repeats <= 0)
                    return Promise.resolve('ok');
                return Promise.resolve('fail');
            });
        });

        it('Success: with two repeats', () => {
            const assertFunc = (response) => {
                return _.isEqual(response, 'ok');
            };

            return expect(domain.repeatQuery('repeatedQuery', {}, assertFunc, 2, 10)).to.be.eventually.equal('ok');
        });

        it('GatewayTimeout: with one repeat', () => {
            const assertFunc = (response) => {
                return _.isEqual(response, 'ok');
            };

            return expect(domain.repeatQuery('repeatedQuery', {}, assertFunc, 1, 10)).to.be.rejectedWith(GatewayTimeoutError);
        });
    });

    describe('processors', () => {
        let assertBusListener = (processor, listeners, name) => {
            let busListener = null;

            _.forEach(listeners, (listener, entryName) => {
                if (entryName === name) {
                    busListener = listener;
                    return false;
                }
            });

            expect(busListener).to.exist;

            delete processor.payload;

            busListener({ a: 'a'});

            expect(processor.payload).to.be.eql({ a: 'a' });
        };

        it('command processor', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Command);
                    expect(_.size(listeners)).to.be.equal(2);
                    assertBusListener(processor, listeners, 'testCommand');
                    assertBusListener(processor, listeners, 'parent');
                });
        });

        it('query processor', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                    assertBusListener(processor, listeners, 'testQuery');
                    assertBusListener(processor, listeners, 'parent');
                });
        });

        it('notice processor', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Notice);
                    expect(_.size(listeners)).to.be.equal(2);
                    assertBusListener(processor, listeners, 'testNotice');
                    assertBusListener(processor, listeners, 'parent');
                });
        });

        it('event processor', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Event);
                    expect(_.size(listeners)).to.be.equal(2);
                    assertBusListener(processor, listeners, 'testEvent');
                    assertBusListener(processor, listeners, 'parent');
                });
        });

        it('event processor - locked domain', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Event);
                    expect(_.size(listeners)).to.be.equal(2);
                    assertBusListener(processor, listeners, 'testEvent');
                    assertBusListener(processor, listeners, 'parent');
                });
        });

        it('post subscribe', () => {
            let localBus = new MockServiceBus();
            localBus.emulateBusConstructed();

            let localDomain = new Domain(localBus, domainName);
            let processor = new MockDomainProcessor();

            return localDomain.useProcessor(processor)
                .then(() => {
                    return localDomain.awake();
                })
                .then(() => {
                    const listeners = localBus.getListeners(localDomain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(0);
                    localBus.emulateBusConnected();
                    return localDomain.subscribeProcessors();
                })
                .then(() => {
                    const listeners = localBus.getListeners(localDomain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                });
        });

        it('awakening', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Query))).to.be.equal(0);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Command))).to.be.equal(0);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Notice))).to.be.equal(0);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Event))).to.be.equal(2);
                    return domain.awake();
                })
                .then(() => {
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Query))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Command))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Notice))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Event))).to.be.equal(2);
                });
        });

        it('disable all processors', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                    return domain.disableProcessors();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                });
        });

        it('disable specific processor', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                    return domain.disableProcessor(processor);
                })
                .then(() => {
                    const listeners = bus.getListeners(domain.name, MockServiceBus.Query);
                    expect(_.size(listeners)).to.be.equal(2);
                });
        });

        it('duplicate subscribe', () => {
            let processor = new MockDomainProcessor();

            return domain.useProcessor(processor)
                .then(() => {
                    return domain.awake();
                })
                .then(() => {
                    return domain.subscribeProcessors();
                })
                .then(() => {
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Query))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Command))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Notice))).to.be.equal(2);
                    expect(_.size(bus.getListeners(domain.name, MockServiceBus.Event))).to.be.equal(2);
                });
        });

    });

    describe('domain routes', () => {
        it('event route', () => {
            expect(domain.getEventRoute('testEvent')).to.be.eql(`${domain.name}.${MockServiceBus.Event}.testEvent`);
        });
        it('command route', () => {
            expect(domain.getCommandRoute('testCommand')).to.be.eql(`${domain.name}.${MockServiceBus.Command}.testCommand`);
        });
        it('query route', () => {
            expect(domain.getQueryRoute('testQuery')).to.be.eql(`${domain.name}.${MockServiceBus.Query}.testQuery`);
        });
        it('notice route', () => {
            expect(domain.getNoticeRoute('testNotice')).to.be.eql(`${domain.name}.${MockServiceBus.Notice}.testNotice`);
        });
    });
});