import * as chai from 'chai';
import { ToleranceCounter } from '../toleranceCounter';

let expect = chai.expect;
chai.expect();


describe('ToleranceCounter', () => {
    let counter = null;

    beforeEach(() => {
        counter = new ToleranceCounter(1, 100);
    });

    it('construction', () => {
        expect(counter.tolerance).to.be.equal(1);
        expect(counter.timeout).to.be.equal(100);
    });

    it('excess limit', () => {
        let exceeded = null;

        counter.onExcess((tolerance) => {
            exceeded = tolerance;
        });

        counter.increment();

        expect(exceeded).to.not.exist;

        counter.increment();

        expect(exceeded).to.be.equal(1);
    });

    it('timeout', (done) => {
        let exceeded = false;

        counter.onExcess(() => {
            exceeded = true;
        });

        counter.increment();

        expect(exceeded).to.be.false;

        setTimeout(() => {
            counter.increment();
            if (exceeded)
                done(new Error('timeout not working'));
            done();
        }, 110);
    });
});
