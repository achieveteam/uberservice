import _ from 'lodash';
import * as chai from 'chai';
import { MicroService } from '../microService';
import { MockMicroService } from '../mocks/mockMicroService';
import { MockServiceBus } from '../mocks/mockServiceBus';

let expect = chai.expect;
chai.expect();

describe('MicroService - StateMachine', () => {
    /** @type {MockMicroService} **/
    let service = null;

    beforeEach(() => {
        service = new MockMicroService({
            microservice: {
                name: 'test-name',
                debugName: 'test-debug-name'
            }, bus: { }
        });
    });
    afterEach(() => {
        process.removeAllListeners();
    });
    describe('constructor', () => {
        it('states collection', () => {
            expect(service.stateMachine.states).to.contain.all.keys(['uninitialized', 'waitBus', 'ready', 'failure']);
        });
    });

    describe('events', () => {
        it('initial', () => {
            expect(service.stateMachine.states.current).to.be.equal('waitStart');
        });
        it('started', () => {
            service.stateMachine.states.started();
            expect(service.stateMachine.states.current).to.be.equal('waitBusAcquire');
        });
        it('busAcquire', () =>  {
            service.stateMachine.states.started();
            service.stateMachine.states.busAcquire();
            expect(service.stateMachine.states.current).to.be.equal('waitBus');
        });
        it('busReady', () =>  {
            service.stateMachine.states.started();
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.busReady();
            expect(service.stateMachine.states.current).to.be.equal('revival');
        });
        it('revived', () =>  {
            service.stateMachine.states.started();
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.busReady();
            service.stateMachine.states.revived();
            expect(service.stateMachine.states.current).to.be.equal('ready');
        });
        it('reAcquire', () => {
            service.stateMachine.states.started();
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.busReady();
            service.stateMachine.states.busReAcquire();
            expect(service.stateMachine.states.current).to.be.equal('waitBus');
        });
        it('init - failure', () => {
            service.stateMachine.states.unrecoverableFailure();
            expect(service.stateMachine.states.current).to.be.equal('failure');
        });
        it('waitBus - failure', () => {
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.unrecoverableFailure();
            expect(service.stateMachine.states.current).to.be.equal('failure');
        });
        it('waitBus - failure', () => {
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.busReady();
            service.stateMachine.states.unrecoverableFailure();
            expect(service.stateMachine.states.current).to.be.equal('failure');
        });
        it('waitBus - failure', () => {
            service.stateMachine.states.busAcquire();
            service.stateMachine.states.busReady();
            service.stateMachine.states.unrecoverableFailure();
            expect(service.stateMachine.states.current).to.be.equal('failure');
        });
    });
});

describe('MicroService', () => {
    /** @type {MockMicroService} **/
    let microService = null;
    let serviceName = 'test-name';
    let debugName = 'test-name-debug';

    beforeEach(() => {
        microService = new MockMicroService({
            microservice: {
                name: serviceName,
                debugName: debugName
            }, bus: {}
        });
    });

    afterEach(() => {
        process.removeAllListeners();
    });
    describe('constructor', () => {
        it('name valid', () => {
            expect(microService.name).to.be.equal(serviceName);
        });
        it('debug name valid', () => {
            expect(microService.debugName).to.be.equal(debugName);
        });
        it('initial state', () => {
            expect(microService.stateMachine.states.current).to.be.equal('waitStart');
        });
    });

    describe('state changes depends on bus', () => {
        it('wait bus', () => {
            return microService.start()
                .then(() => {
                    microService.bus.emulateBusConstructed();
                    expect(microService.stateMachine.states.current).to.be.equal('waitBus');
                });
        });
        it('bounded to bus', () => {
            return microService.start()
                .then(() => {
                    microService.bus.emulateBusConstructed();
                    microService.bus.emulateBusConnected();
                    expect(microService.stateMachine.states.current).to.be.equal('revival');
                });
        });
    });

    describe('handle high-level failures', () => {
        it('unrecoverableFailure', () => {
            expect(microService.stateMachine.states.current).to.be.not.equal('failure');
            microService.unrecoverableFailure(new Error());
            expect(microService.stateMachine.states.current).to.be.equal('failure');
        });
        it('failure property', () => {
            expect(microService.isFailed).to.be.false;
            microService.unrecoverableFailure(new Error());
            expect(microService.isFailed).to.be.true;
        });
    });

    describe('monitor status', () => {
        it('onMonitorStatusQuery', () => {
            return microService.onMonitorStatusQuery({})
                .then((status) => {
                    expect(status).to.have.all.keys(_.merge(microService.status, { allOk: true }));
                });
        });
    });

    describe('revival sequence', () => {
        it('domains awake', () => {
            const domain = microService.processorDomain;
            expect(domain.isSuspended).to.be.true;

            return microService.start()
                .then(() => {
                    microService.bus.emulateBusConstructed();
                    microService.bus.emulateBusConnected();

                    return microService.revivalStatePromise;
                })
                .then(() => {
                    expect(domain.isSuspended).to.be.true;
                    return microService.readyStatePromise;
                })
                .then(() => {
                    expect(domain.isSuspended).to.be.false;
                });
        });

        it('domains unlocked if state is ready', () => {
            return microService.start()
                .then(() => {
                    microService.bus.emulateBusConstructed();
                    microService.bus.emulateBusConnected();

                    return microService.readyStatePromise;
                })
                .then(() => {
                    expect(microService.getDomain('com.domain.another').isSuspended).to.be.false;
                });
        });
    });
});


