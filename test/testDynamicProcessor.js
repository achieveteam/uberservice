import _ from 'lodash';
import * as chai from 'chai';
import { Domain } from '../domain';
import { DynamicProcessor } from '../ddd/dynamicProcessor';
import { MockServiceBus } from '../mocks/mockServiceBus';

const expect = chai.expect;
chai.expect();

describe('DynamicProcessor', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {Domain} **/
    let domain = null;
    /** @type {DynamicProcessor} **/
    let processor = null;

    beforeEach(() => {
        bus = new MockServiceBus();
        bus.emulateBusConstructed();
        bus.emulateBusConnected();
        domain = new Domain(bus, 'com.test.domain.groups');
        processor = new DynamicProcessor();
    });

    it('add handler by name', () => {
        let called = false;

        processor.addHandler('someEvent', 'event', () => {
            called = true;
        });

        processor.processEventSomeEvent();
        expect(called).to.be.true;
    });

    it('multiple event handlers', () => {
        let aCalled = false;
        let bCalled = false;

        processor.addHandler('someEvent', 'event', () => {
            aCalled = true;
        });
        processor.addHandler('someEvent', 'event', () => {
            bCalled = true;
        });

        processor.processEventSomeEvent();
        expect(aCalled).to.be.true;
        expect(bCalled).to.be.true;
    });

    it('remove handler', () => {
        let aCalled = false;

        const handler = () => {
            aCalled = true;
        };

        processor.addHandler('someEvent', 'event', handler);
        processor.removeHandler('someEvent', 'event', handler);

        try {
            processor.processEventSomeEvent();
        } catch(err) {
            return;
        }
        expect('method must be removed').to.not.exist;
    });

    it('using as domain processor', () => {
        let callPayload = null;

        processor.addHandler('someEvent', 'event', (payload) => {
            callPayload = payload;
        });




        return domain.useProcessor(processor)
            .then(() => {
                return domain.event('someEvent', { test: 'test' }, 1);
            })
            .then(() => {
                expect(callPayload).to.be.eql({ test: 'test' });
            });
    });
});
