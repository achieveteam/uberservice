import { UberStateMachine } from '../uberStateMachine';

import * as chai from 'chai';
let expect = chai.expect;


describe('UberStateMachine API', () => {
    describe('state', () => {
        it('no name exception', () => {
            expect(() => {
                new UberStateMachine().state('');
            }).to.throw(Error);
        });
        it('null name exception', () => {
            expect(() => {
                new UberStateMachine.state(null);
            }).to.throw(Error);
        });
        it('with name', () => {
            expect(new UberStateMachine()
                .state('test-state-name').states).to.contain.all.keys(['test-state-name']);
        });
        it('name is already defined', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name')
                    .state('test-state-name');
            }).to.throw(Error);
        });
        it('onEnter special input assigned', () => {
            let enterCallback = () => {
            };
            expect(new UberStateMachine()
                .state('test-state-name', {
                    enter: enterCallback
                }).states['test-state-name'].enter).to.be.equal(enterCallback);
        });
        it('onLeave special input assigned', () => {
            let leaveCallback = () => {
            };
            expect(new UberStateMachine()
                .state('test-state-name', {
                    enter: leaveCallback
                }).states['test-state-name'].enter).to.be.equal(leaveCallback);
        });
        it('initial state', () => {
            let machine = new UberStateMachine()
                .state('test-state-name1', {initial: false})
                .state('test-state-name2', {initial: true});
            expect(machine.states['test-state-name1'].initial).to.be.false;
            expect(machine.states['test-state-name2'].initial).to.be.true;
        });
        it('initial state is already defined', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1', {initial: true})
                    .state('test-state-name2', {initial: true})
            }).to.throw(Error);
        });
    });

    describe('event', () => {
        it('no name exception', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .event('', 'test-state-name1', 'test-state-name2');
            }).to.throw(Error);
        });
        it('null name exception', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .event(null, 'test-state-name1', 'test-state-name2');
            }).to.throw(Error);
        });
        it('with name', () => {
            let machine = new UberStateMachine()
                .state('test-state-name1')
                .state('test-state-name2')
                .event('testEvent', 'test-state-name1', 'test-state-name2');
            expect(machine.states).to.have.property('testEvent').that.is.a('function');
            expect(machine.states['test-state-name1'].name).to.be.equals('test-state-name1');
            expect(machine.states['test-state-name1'].events['testEvent']).to.be.equals('test-state-name2');
        });
        it('name is already defined as state', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .state('test-state-name3')
                    .event('test-state-name2', 'test-state-name1', 'test-state-name2');
            }).to.throw(Error);
        });
        it('more than one event with the same name', () => {
            let machine = new UberStateMachine()
                .state('test-state-name1')
                .state('test-state-name2')
                .event('testEvent1', 'test-state-name1', 'test-state-name2')
                .event('testEvent2', 'test-state-name1', 'test-state-name2');
            expect(machine.states['test-state-name1'].events['testEvent1']).to.be.equals('test-state-name2');
            expect(machine.states['test-state-name1'].events['testEvent2']).to.be.equals('test-state-name2');
        });
        it('to state is not defined', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .event('testEvent', 'test-state-name1', 'test-state-name2');
            }).to.throw(Error);
        });
        it('from state is not defined', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name2')
                    .event('testEvent', 'test-state-name1', 'test-state-name2');
            }).to.throw(Error);
        })
        it('for the state is already defined', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .state('test-state-name3')
                    .event('testEvent', 'test-state-name1', 'test-state-name2')
                    .event('testEvent', 'test-state-name1', 'test-state-name3');
            }).to.throw(Error);
        });
        it('from the state to state is already defined', () => {
            let machine = new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .event('testEvent1', 'test-state-name1', 'test-state-name2')
                    .event('testEvent2', 'test-state-name1', 'test-state-name2');
            expect(machine.states).to.have.property('testEvent1').that.is.a('function');
            expect(machine.states).to.have.property('testEvent2').that.is.a('function');
            expect(machine.states['test-state-name1'].name).to.be.equals('test-state-name1');
            expect(machine.states['test-state-name1'].events['testEvent1']).to.be.equals('test-state-name2');;
            expect(machine.states['test-state-name1'].events['testEvent2']).to.be.equals('test-state-name2');
        });
        it('to states can\'t be an array', () => {
            expect(() => {
                new UberStateMachine()
                    .state('test-state-name1')
                    .state('test-state-name2')
                    .state('test-state-name3')
                    .event('testEvent1', 'test-state-name1', ['test-state-name2', 'test-state-name3']);
            }).to.throw(Error);
        });
        it('from states as array', () => {
            let machine = new UberStateMachine()
                .state('test-state-name1')
                .state('test-state-name2')
                .state('test-state-name3')
                .event('testEvent', ['test-state-name1', 'test-state-name2'], 'test-state-name3');
            expect(machine.states).to.have.property('testEvent').that.is.a('function');
            expect(machine.states['test-state-name1'].name).to.be.equals('test-state-name1');
            expect(machine.states['test-state-name1'].events['testEvent']).to.be.equals('test-state-name3');
            expect(machine.states['test-state-name2'].name).to.be.equals('test-state-name2');
            expect(machine.states['test-state-name2'].events['testEvent']).to.be.equals('test-state-name3');
        });
    });
});

describe('UberStateMachine', () => {
    let tester = null;

    beforeEach(() => {
        class UberStateMachineTester {
            constructor() {
                this.uninitialized_onEnter = false;
                this.uninitialized_onLeave = false;
                this.ready_onEnter = false;
                this.ready_onLeave = false;
                this.leave_bug_onEnter = false;
                this.ready_onEnter_argument = null;

                this.machine = new UberStateMachine()
                    .state('uninitialized', {
                        initial: true,
                        enter: () => {
                            this.uninitialized_onEnter = true;
                        },
                        leave: () => {
                            this.uninitialized_onLeave = true;
                        }
                    })
                    .state('ready', {
                        enter: (argument) => {
                            this.ready_onEnter = true;
                            this.ready_onEnter_argument = argument;
                        },
                        leave: () => {
                            this.ready_onLeave = true;
                        }
                    })
                    .state('working')
                    .state('leave_bug', {
                        enter: () => {
                            this.leave_bug_onEnter = true;
                        }
                    })
                    .event('init', 'uninitialized', 'ready')
                    .event('leaveBug', 'uninitialized', 'leave_bug')
                    .event('doStuff', 'ready', 'working')
                    .event('failure', ['working', 'ready'], 'uninitialized')
                    .event('complete', 'working', 'ready');
            }
        }

        tester = new UberStateMachineTester();
    });

    describe('bugs', () => {
        it('enter without leave not called bug', () => {
            expect(tester.leave_bug_onEnter).to.be.false;
            tester.machine.states.leaveBug();
            expect(tester.leave_bug_onEnter).to.be.true;
        });
    });

    describe('constructor', () => {
        it('valid states', () => {
            expect(tester.machine.states).to.contain.all.keys('uninitialized', 'ready', 'working');
        });
        it('valid initial state', () => {
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.uninitialized.name);
        });
    });
    describe('handle events', () => {
        it('init', () => {
            tester.machine.states.init();
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.ready.name);
        });
        it('doStuff before init', () => {
            tester.machine.states.doStuff();
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.uninitialized.name);
        });
        it('doStuff after init', () => {
            tester.machine.states.init();
            tester.machine.states.doStuff();
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.working.name);
        });
        it('failure', () => {
            tester.machine.states.init();
            tester.machine.states.failure();
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.uninitialized.name);
        });
        it('failure on working', () => {
            tester.machine.states.init();
            tester.machine.states.doStuff();
            tester.machine.states.failure();
            expect(tester.machine.states.current).to.be.equal(tester.machine.states.uninitialized.name);
        });
        it('onEnter/onLeave uninitialized', () => {
            expect(tester.uninitialized_onEnter).to.be.false;
            expect(tester.uninitialized_onLeave).to.be.false;
            tester.machine.states.init();
            expect(tester.uninitialized_onLeave).to.be.true;
        });
        it('onEnter/onLeave ready', () => {
            expect(tester.ready_onEnter).to.be.false;
            expect(tester.ready_onLeave).to.be.false;
            tester.machine.states.init();
            expect(tester.ready_onEnter).to.be.true;
            expect(tester.ready_onLeave).to.be.false;
            tester.machine.states.doStuff();
            expect(tester.ready_onLeave).to.be.true;
        });
        it('onEnter ready with argument', () => {
            let argument = 'test'
            expect(tester.ready_onEnter_argument).to.be.not.equal(argument);
            tester.machine.states.init(argument);
            expect(tester.ready_onEnter_argument).to.be.equal(argument);
        });
        it('onStateChanged', () => {
            let oldState = tester.machine.states.current;
            let eventOldState = null;
            let eventNewState = null;

            tester.machine.onStateChanged((oldState, newState) => {
                eventOldState = oldState;
                eventNewState = newState;
            });

            tester.machine.states.init();

            expect(oldState).to.be.equal(eventOldState);
            expect(tester.machine.states.current).to.be.equal(eventNewState);

            expect(() => {
                tester.machine.onStateChanged((oldState, newState) => { });
            }).to.throw(Error);
        });
        it('event for the state is not found', () => {
            let state = tester.machine.states.current;
            tester.machine.states.complete();
            expect(tester.machine.states.current).to.be.equal(state);
        });
    });
});

