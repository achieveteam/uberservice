import _ from 'lodash';
import Promise from 'bluebird';
import * as chai from 'chai';
import { MemoryStateStore } from '../ddd/memoryStateStore';
import { EventProjection } from '../ddd/eventProjection';
import { EventProjectionComponent } from '../ddd/eventProjectionComponent';

let expect = chai.expect;
chai.expect();

class Component extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('default', [ { key: 1 }]);
    }

    handleContactEventEmailChanged(payload) {
        return this._store.updateState(payload.aggregateId, { email: payload.email })
            .then(() => this.notifyChanged('email', payload.aggregateId, payload.email));
    }
}

class FakeProjection extends EventProjection {
    static getComponents() {
        return Component;
    }

    get component() {
        return this.getComponent(Component);
    }

    get componentStateChanges() {
        return this._componentStatesChanges;
    }

    get componentChanges() {
        return this._componentChanges;
    }

    initialize() {
        this._store = this.createStore('default', [ { key: 1 } ]);
        this._componentStatesChanges = [];
        this._componentChanges = [];
    }

    handleUsersEventEvent() {
        return this._store.updateState('key', { someEventPerformed: true });
    }

    handleUsersEventUserCreate(payload) {
        return this._store.updateState(payload.aggregateId, { });
    }

    componentStateChanged(key, state, component, store) {
        this._componentStatesChanges.push({ key, state, component, store});
    }

    componentChanged(name, key, state, component) {
        this._componentChanges.push({ name, key, state, component });
    }

    respondUsersQueryGet() {
        return this._store.getStates();
    }

    testLoadState(id) {
        return this._store.loadState(id);
    }

    testSaveState(id, state) {
        return this._store.saveState(id, state);
    }

    testGetStates() {
        return this._store.getStates();
    }

    testRemoveState(id) {
        return this._store.removeState(id);
    }
}

describe('EventProjection', () => {
    /** @type {MemoryStateStore} **/
    let stateStore = null;
    /** @type {FakeProjection} **/
    let projection = null;

    beforeEach(() => {
        stateStore = new MemoryStateStore();
        projection = new FakeProjection(null, stateStore);
        return projection.build();
    });

    it('constansts', () => {
        expect(FakeProjection.getEventCounterName()).to.be.equal('fakeprojection-event-counter');
    });

    it('event counter', () => {
        expect(projection.eventCounter).to.be.equal(0);
        return projection.setEventCounter(1)
            .then(() => {
                expect(projection.eventCounter).to.be.equal(1);
                return stateStore.getCounter(FakeProjection.getEventCounterName());
            })
            .then((counter) => {
                expect(counter).to.be.equal(1);
            });
    });

    it('state changes', () => {
        return projection.handleUsersEventEvent()
            .then(() => {
                return stateStore.getState(FakeProjection.name + 'Default', 'key');
            })
            .then((state) => {
                expect(state.someEventPerformed).to.be.true;
            });
    });

    it('getStates', () => {
        return Promise.all([
            projection.handleUsersEventUserCreate({aggregateId: 'id1'}),
            projection.handleUsersEventUserCreate({aggregateId: 'id2'})
        ])
        .then(() => {
            return projection.respondUsersQueryGet();
        })
        .then((states) => {
            expect(_.size(states)).to.be.equal(2);
        });
    });

    it('state indexes on getStates', () => {
        return projection.testGetStates()
            .then(() => {
                expect(stateStore.getStateIndexes(FakeProjection.name)).to.be.eql(FakeProjection.StateIndexes);
            });
    });

    it('state indexes on load', () => {
        return projection.testLoadState('id')
            .then(() => {
                expect(stateStore.getStateIndexes(FakeProjection.name)).to.be.eql(FakeProjection.StateIndexes);
            })
    });

    it('state indexes on save', () => {
        return projection.testSaveState('id')
            .then(() => {
                expect(stateStore.getStateIndexes(FakeProjection.name)).to.be.eql(FakeProjection.StateIndexes);
            })
    });

    it('remove all states', () => {
        return projection.resetStates()
            .then(() => {
                expect(stateStore.getStateIndexes('FakeProjection')).to.not.exist;
            });
    });

    it('remove specific state', () => {
        return projection.handleUsersEventEvent()
            .then(() => {
                return projection.testRemoveState('key');
            })
            .then(() => {
                return stateStore.getState(FakeProjection.name, 'key');
            })
            .then((state) => {
                expect(state.someEventPerformed).to.not.exist;
            });
    });

    it.skip('component event handler & change callback', () => {
        return projection['handleContactEventEmailChanged']({ aggregateId: 'id1', email: 'ya@ya.com' })
            .then(() => {
                expect(_.size(projection.componentStateChanges)).to.be.equal(1);
                expect(projection.componentStateChanges[0].key).to.be.equal('id1');
                expect(projection.componentStateChanges[0].state).to.be.eql({ id: 'id1', email: 'ya@ya.com'});
                expect(projection.componentStateChanges[0].component).to.be.equal(projection.component);
            });
    });

    it('component changes', () => {
        return projection['handleContactEventEmailChanged']({ aggregateId: 'id1', email: 'ya@ya.com' })
            .then(() => {
                expect(_.size(projection.componentChanges)).to.be.equal(1);
                expect(projection.componentChanges[0].name).to.be.eql('email');
                expect(projection.componentChanges[0].key).to.be.eql('id1');
                expect(projection.componentChanges[0].state).to.be.eql('ya@ya.com');
                expect(projection.componentChanges[0].component).to.be.equal(projection.component);
            });
    });

    it('component projection property', () => {
        expect(projection.component.projection).to.be.equal(projection);
    });
});

