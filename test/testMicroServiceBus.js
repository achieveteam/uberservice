import * as chai from 'chai';
import { MockServiceBus } from '../mocks/mockServiceBus';

let expect = chai.expect;
chai.expect();

describe('MicroServiceBus - StateMachine', () => {
    let bus = null;

    beforeEach(() => {
        bus = new MockServiceBus();
    });

    describe('constructor', () => {
        it('states collection', () => {
            expect(bus.stateMachine.states).to.contain.all.keys(['uninitialized', 'acquire', 'ready', 'failure']);
        });
    });

    describe('events', () => {
        it('initial', () => {
            expect(bus.stateMachine.states.current).to.be.equal('uninitialized');
        });
        it('busConstructed', () => {
            bus.stateMachine.states.busConstructed();
            expect(bus.stateMachine.states.current).to.be.equal('acquire');
        });
        it('invalidConfig', () => {
            bus.stateMachine.states.invalidConfig();
            expect(bus.stateMachine.states.current).to.be.equal('failure');
        });
        it('busConnected', () => {
            bus.stateMachine.states.busConstructed();
            bus.stateMachine.states.busConnected();
            expect(bus.stateMachine.states.current).to.be.equal('ready');
        });
        it('initial - connectionError', () => {
            bus.stateMachine.states.connectionError();
            expect(bus.stateMachine.states.current).to.be.equal('failure');
        });
        it('ready - connectionError', () => {
            bus.stateMachine.states.busConstructed();
            bus.stateMachine.states.busConnected();
            bus.stateMachine.states.connectionError();
            expect(bus.stateMachine.states.current).to.be.equal('acquire');
        });
        it('acquire - connectionError', () => {
            bus.stateMachine.states.busConstructed();
            bus.stateMachine.states.connectionError();
            expect(bus.stateMachine.states.current).to.be.equal('acquire');
        });
    });
});

describe('MicroServiceBus', () => {
    let bus = null;
    let busName = 'test-name';
    let stateChanged = null;

    beforeEach(() => {
        bus = new MockServiceBus({ debugName: busName });
        stateChanged = null;
    });

    let cbStateChanged = (oldState, newState) => {
        stateChanged = {
            oldState: oldState,
            newState: newState
        };
    };

    describe('constructor', () => {
        it('logger name taken from config', () => {
            expect(bus._logger.name).to.be.equal(busName);
        });
        it('state set to uninitialized', () => {
            expect(bus.stateMachine.states.current).to.be.equal('uninitialized');
        });
    });

    describe('callbacks', () => {
        it('state changed', () => {
            expect(stateChanged).to.be.null;
            bus.on(MockServiceBus.StateChangeCallback, cbStateChanged);
            let oldState = bus.stateMachine.states.current;
            let newState = 'acquire';
            bus.stateMachine.states.busConstructed();
            expect(stateChanged.oldState).to.be.equal(oldState);
            expect(stateChanged.newState).to.be.equal(newState);
        });
    });

});