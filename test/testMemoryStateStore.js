import _ from 'lodash';
import Promise from 'bluebird';
import * as chai from 'chai';
import { MemoryStateStore } from '../ddd/memoryStateStore';

let expect = chai.expect;
chai.expect();

describe('MemoryStateStore', () => {
    /** @type {MemoryStateStore} **/
    let stateStore = null;

    beforeEach(() => {
        stateStore = new MemoryStateStore();
    });

    describe('counters', () => {
        it('counter initial value', () => {
            return expect(stateStore.getCounter('testCounter')).to.be.eventually.equal(0);
        });

        it('update counter', () => {
            return stateStore.setCounter('testCounter', 5)
                .then(() => {
                    return expect(stateStore.getCounter('testCounter')).to.be.eventually.equal(5);
                });
        });

        it('multiple updates', () => {
            return stateStore.setCounter('testCounter', 5)
                .then(() => {
                    return stateStore.setCounter('testCounter', 10);
                })
                .then(() => {
                    return expect(stateStore.getCounter('testCounter')).to.be.eventually.equal(10);
                });
        });
    });

    describe('states', () => {
        it('get-set', () => {
            return stateStore.setState('SomeObject', 'id', {
                    state: 'state'
                })
                .then(() => {
                    return expect(stateStore.getState('SomeObject', 'id')).to.be.eventually.eql({ id: 'id', state: 'state'});
                });
        });

        it('empty object initial state', () => {
            return expect(stateStore.getState('SomeObject', 'id')).to.be.eventually.eql({ id: 'id' });
        });

        it('remove all', () => {
            return stateStore.setState('SomeObject', 'id', { state: 'state' })
                .then(() => {
                    return stateStore.removeStates('SomeObject');
                })
                .then(() => {
                    return expect(stateStore.getState('SomeObject', 'id')).to.be.eventually.eql({ id: 'id'});
                });
        });

        it('compound unique state indexes (not implemented in nedb)', () => {
            return stateStore.setState('SomeObject', 'id', { indexA: 'a', indexB: 'b' }, [{
                index: {
                    indexA: 1,
                    indexB: 1
                },
                options: {
                    unique: true
                }
            }]).then(() => {
                return stateStore.setState('SomeObject', 'id2', { indexA: 'a', indexB: 'c' });
            });
        });

        it('remove state', () => {
            return stateStore.setState('SomeObject', 'id', { state: 'state' })
                .then(() => {
                    return stateStore.removeState('SomeObject', 'id');
                })
                .then(() => {
                    return expect(stateStore.getState('SomeObject', 'id')).to.be.eventually.eql({ id: 'id'});
                });
        });

        it('remove all', () => {
            return stateStore.setState('SomeObject', 'id', { state: 'state' })
                .then(() => {
                    return stateStore.removeStates('SomeObject');
                })
                .then(() => {
                    return expect(stateStore.getState('SomeObject', 'id')).to.be.eventually.eql({ id: 'id'});
                });
        });

        it('remove specific all', () => {
            return Promise.all([
                    stateStore.setState('SomeObject', 'id1', { state: 'state1' }),
                    stateStore.setState('SomeObject', 'id2', { state: 'state1' }),
                    stateStore.setState('SomeObject', 'id3', { state: 'state2'})
                ])
                .then(() => stateStore.removeStates('SomeObject', { state: 'state1' }))
                .then(() => expect(stateStore.getStates('SomeObject')).to.be.eventually.eql([{ id: 'id3', state: 'state2' }]));
        });
    });

    describe('for each', () => {
        it('eachState', () => {
            const ids = [];
            return Promise.each([
                    { id: 'id1', state: 'state1'},
                    { id: 'id2', state: 'state1'},
                    { id: 'id3', state: 'state2'}
                ], ({ id, state }) => stateStore.setState('SomeObject', id, { state }))
                .then(() => stateStore.eachState('SomeObject', ({ id, state }) => {
                    ids.push(id);
                    expect(state).to.be.equals(undefined);
                }, { state: 'state1' }, { state: 0 }, { id: -1 }))
                .then(() => {
                    expect(ids.length).to.be.equals(2);
                    expect(ids[0]).to.be.equals('id2');
                    expect(ids[1]).to.be.equals('id1');
                });
        });
    });

    describe('updates', () => {
        it('updateState with not existing item', () => {
            return stateStore.updateState('SomeObject', 'id1', { $set: { state: 'state1' } })
                .then(() => expect(stateStore.getStates('SomeObject')).to.be.eventually.eql([
                    { id: 'id1', state: 'state1' }
                ]));
        });

        it('updateStates', () => {
            return Promise.each([
                    { id: 'id1', state: 'state1'},
                    { id: 'id2', state: 'state1'},
                    { id: 'id3', state: 'state2'}
                ], ({ id, state }) => stateStore.setState('SomeObject', id, { state }))
                .then(() => stateStore.updateStates('SomeObject', { state: 'state1' }, { $set: { state: 'state3' } }))
                .then(() => expect(stateStore.getStates('SomeObject')).to.be.eventually.eql([
                    { id: 'id1', state: 'state3' },
                    { id: 'id2', state: 'state3' },
                    { id: 'id3', state: 'state2' }
                ]));
        });

        it('updateState', () => {
            return Promise.each([
                    { id: 'id1', state: 'state1'},
                    { id: 'id2', state: 'state1'},
                    { id: 'id3', state: 'state2'}
                ], ({ id, state }) => stateStore.setState('SomeObject', id, { state }))
                .then(() => stateStore.updateState('SomeObject', 'id2', { $set: { state: 'state3' } }))
                .then(() => expect(stateStore.getStates('SomeObject')).to.be.eventually.eql([
                    { id: 'id1', state: 'state1' },
                    { id: 'id2', state: 'state3' },
                    { id: 'id3', state: 'state2' }
                ]));
        });
    });

    describe('getStates', () => {
        beforeEach(() => {
            return Promise.all([
                stateStore.setState('SomeObject', 'id1', { name: 'a', junk: 'junk' } ),
                stateStore.setState('SomeObject', 'id2', { name: 'b', junk: 'junk' } )
            ]);
        });

        it('default', () => {
            return stateStore.getStates('SomeObject')
                .then((states) => {
                    expect(_.size(states)).to.be.equal(2);
                    expect(states[0].id).to.be.equal('id1');
                    expect(states[1].id).to.be.equal('id2');
                });
        });

        it('sort descend', () => {
            return stateStore.getStates('SomeObject', {}, {}, { name: -1 }, 0, 2)
                .then((states) => {
                    expect(states[0].name).to.be.equal('b');
                    expect(states[1].name).to.be.equal('a');
                });
        });

        it('sort ascend', () => {
            return stateStore.getStates('SomeObject', {}, {}, { name: 1 }, 0, 2)
                .then((states) => {
                    expect(states[0].name).to.be.equal('a');
                    expect(states[1].name).to.be.equal('b');
                });
        });

        it('limit', () => {
            return stateStore.getStates('SomeObject', {}, {}, { id: 1 }, 0, 1)
                .then((states) => {
                    expect(_.size(states)).to.be.equal(1);
                });
        });

        it('offset', () => {
            return stateStore.getStates('SomeObject', {}, {}, { id: 1 }, 1, 1)
                .then((states) => {
                    expect(_.size(states)).to.be.equal(1);
                    expect(states[0].id).to.be.equal('id2');
                });
        });

        it('fields selector - include', () => {
            return stateStore.getStates('SomeObject', { name: 'b' }, { name: 1 })
                .then((states) => {
                    expect(_.size(states)).to.be.equal(1);
                    expect(states[0].name).to.be.equal('b');
                    expect(states[0].junk).to.not.exist;
                });
        });

        it('fields selector - exclude', () => {
            return stateStore.getStates('SomeObject', { name: 'b' }, { junk: 0 })
                .then((states) => {
                    expect(_.size(states)).to.be.equal(1);
                    expect(states[0].name).to.be.equal('b');
                    expect(states[0].junk).to.not.exist;
                });
        });
    });
});


