import _ from 'lodash';
import Promise from 'bluebird';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import * as exports from '../index';
import * as liberr from '@badgeteam/liberr';

let expect = chai.expect;
chai.use(chaiAsPromised);

describe('Export', () => {
    it('liberr export', () => {
        _.forEach(liberr, (libClass) => {
            if (_.isFunction(libClass))
                expect(exports[libClass.name], `${libClass.name} should be exported within index.js`).to.exist;
        });
    });
});
