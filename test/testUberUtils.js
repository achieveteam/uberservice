import _ from 'lodash';
import * as chai from 'chai';
import { UberUtils } from '../uberUtils';
import { BadRequestError } from '@badgeteam/liberr';

const expect = chai.expect;
chai.expect();

class BaseClass {
    methodBaseClass() { }
}

class ExperimentalClass extends BaseClass {
    methodExperimentalClass() { }
}

describe('UberUtils', () => {
    /** @type {ExperimentalClass} **/
    let subject = null;

    beforeEach(() => {
        subject = new ExperimentalClass();

        const methodDynamic = function() { };

        Object.defineProperty(subject, 'methodDynamic', {
            enumerable: true,
            configurable: true,
            value: methodDynamic.bind(subject)
        });
    });


    it('enumerate class methods', () => {
        const methods = [];
        const entries = [];

        UberUtils.enumerateClassMethods(subject, 'method', (key, entryName) => {
            methods.push(key);
            entries.push(entryName);
        });

        expect(methods).to.include('methodBaseClass');
        expect(entries).to.include('baseClass');

        expect(methods).to.include('methodExperimentalClass');
        expect(entries).to.include('experimentalClass');

        expect(methods).to.not.include('methodDynamic');
    });

    it('enumerate instance methods', () => {
        const methods = [];
        const entries = [];

        UberUtils.enumerateInstanceMethods(subject, 'method', (key, entryName) => {
            methods.push(key);
            entries.push(entryName);
        });

        expect(methods).to.not.include('methodBaseClass');
        expect(entries).to.not.include('methodExperimentalClass');

        expect(methods).to.include('methodDynamic');
        expect(entries).to.include('dynamic');
    });

    it('ensure non empty fields', () => {
        expect(UberUtils.ensureNonEmptyFields({ fieldA: 'a', fieldB: 'b' })).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ fieldA: 'a', fieldB: 'b' }, 'fieldA')).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ fieldA: 'a', fieldB: 'b' }, 'fieldA', 'fieldB')).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ fieldA: 'a', fieldB: 'b' }, 'fieldA', 'fieldB', 'fieldC')).to.be.equal('fieldC');
        expect(UberUtils.ensureNonEmptyFields({ fieldA: 'a', fieldB: 'b' }, 'fieldA', 'fieldB', 'fieldC', 'fieldD')).to.be.equal('fieldC, fieldD');
        expect(UberUtils.ensureNonEmptyFields({ object: {}}, 'object')).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ array: []}, 'array')).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ string: ''}, 'string')).to.be.equal('string');
        expect(UberUtils.ensureNonEmptyFields({ number: 0}, 'number')).to.be.empty;
        expect(UberUtils.ensureNonEmptyFields({ boolean: false}, 'boolean')).to.be.empty;
    });

    it('pick', () => {
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA')).to.be.eql({ fieldA: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, ['fieldA'])).to.be.eql({ fieldA: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA', 'fieldB')).to.be.eql({ fieldA: 'a', fieldB: 'b' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, ['fieldA', 'fieldB'])).to.be.eql({ fieldA: 'a', fieldB: 'b' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: { nested: 'a' }}, 'fieldB.nested')).to.be.eql({ fieldB: { nested: 'a' }});
        expect(UberUtils.pick({ fieldA: 'a', fieldB: { nested: 'a' }}, 'fieldA', 'fieldB.nested')).to.be.eql({ fieldA: 'a', fieldB: { nested: 'a' }});
        expect(UberUtils.pick({ fieldA: 'a', fieldB: { nested: 'a', junk: 'junk' }}, ['fieldA', 'fieldB.nested'])).to.be.eql({ fieldA: 'a', fieldB: { nested: 'a' }});
        expect(() => { UberUtils.pick() }).to.throw(BadRequestError);
        expect(() => { UberUtils.pick({ }, 'fieldA')}).to.throw(BadRequestError);

        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, ' fieldA')).to.be.eql({ fieldA: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA ')).to.be.eql({ fieldA: 'a' });
    });

    it('pick with rename', () => {
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA=>customField')).to.be.eql({ customField: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA=>customField', 'fieldB')).to.be.eql({ customField: 'a', fieldB: 'b' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA=>customField', 'fieldB=>secondField')).to.be.eql({ customField: 'a', secondField: 'b' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA =>customField')).to.be.eql({ customField: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA => customField')).to.be.eql({ customField: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA  =>  customField')).to.be.eql({ customField: 'a' });
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA=> customField')).to.be.eql({ customField: 'a' });
    });

    it('pick with optional fields', () => {
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA', '#fieldB', '#fieldC')).to.be.eql({ fieldA: 'a', fieldB: 'b'});
        expect(UberUtils.pick({ fieldA: 'a', fieldB: 'b'}, 'fieldA', '#fieldB=>custom', '#fieldC')).to.be.eql({ fieldA: 'a', custom: 'b'});
    });

    it('pick with lodash function', () => {
        expect(UberUtils.pick({ fieldA: '0'}, '_.toInteger(fieldA)')).to.be.eql({ fieldA: 0 });
        expect(UberUtils.pick({ fieldA: '0'}, '_.toInteger(fieldA)=>renamed')).to.be.eql({ renamed: 0 });
        expect(UberUtils.pick({ fieldA: 0}, '_.toString(fieldA)')).to.be.eql({ fieldA: '0' });
        expect(UberUtils.pick({ fieldA: 0}, '_.toString(fieldA)=>renamed')).to.be.eql({ renamed: '0' });
        expect(UberUtils.pick({ }, '#_.toInteger(fieldA)')).to.be.eql({ });
        expect(UberUtils.pick({ fieldA: '0' }, '#_.toInteger(fieldA)', '#_.toInteger(fieldB)')).to.be.eql({ fieldA: 0 });
    });

    it('convertCamelCaseWordLetters', () => {
        expect(UberUtils.convertCamelCaseWordLetters('someCamelCase', (letter) => { return '-' + _.toLower(letter)})).to.be.equal('some-camel-case');
        expect(UberUtils.convertCamelCaseWordLetters('SomeCamelCase', (letter, index) => {
            if (index === 0)
                return _.toLower(letter);
            return '-' + _.toLower(letter)
        })).to.be.equal('some-camel-case');
    });
});

