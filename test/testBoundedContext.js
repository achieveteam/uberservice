import _ from 'lodash';
import Promise from 'bluebird';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { Aggregate } from '../ddd/aggregate';
import { EventProjection } from '../ddd/eventProjection';
import { EventProjectionComponent } from '../ddd/eventProjectionComponent';
import { Saga } from '../ddd/saga';
import { BoundedContext } from '../ddd/boundedContext';
import { MockEventStore } from '../mocks/mockEventStore';
import { MemoryStateStore } from '../ddd/memoryStateStore';
import { MockServiceBus } from '../mocks/mockServiceBus';
import { GenericError, BadRequestError, RequestTimeoutError, ConflictError } from '@badgeteam/liberr';
import { Domain } from '../domain';

chai.use(chaiAsPromised);
let expect = chai.expect;
chai.expect();

class FakeAggregate extends Aggregate {
    static Scope = 'fake';

    create() {
        this.emitEvent('fakeCreated');
    }

    setName(name) {
        this.emitEvent('fakeNameSet', { name: name });
    }

    commandSomeCommand() {
        this.someCommandPerformed = true;
        return Promise.resolve('ok');
    }

    commandSetName(payload) {
        this.emitEvent('fakeNameSet', { name: payload.name });
    }

    commandSync() {
        return 'ok';
    }

    commandNotInteresting() {
        this.emitEvent('fakeNotInteresting');
    }

    commandEmitBadRequestEvent() {
        this.emitEvent('failedWithBadRequest');
    }

    handleFakeNameSet(data) {
        this.name = data.name;
    }
}
class FakeAggregateSubclass extends FakeAggregate { }
class CustomAggregateConstructor extends FakeAggregate {
    constructor(context, id, param) {
        super(context, id);
        this.param = param;
    }
}
class FailedAggregate extends Aggregate {
    static Scope = 'com.test-users.fake';

    constructor(repository, id) {
        super(repository, id);
        throw new GenericError('iam bad boy');
    }
}
class BadAggregate { }
class FakeProjection extends EventProjection {
    initialize() {
        this._store = this.createStore('default');
    }

    respondScopeQueryQuery(payload) {
        return Promise.resolve(payload);
    }

    handleScopeEventEvent(payload) {
        return this._store.loadState(payload.id)
            .then((state) => {
                state.someEvent = true;
                return this._store.saveState(payload.id, state);
            });
    }

    handleScopeEventSyncEvent(payload) {
        console.log('good');
    }

    resetStates() {
        return super.resetStates()
            .then(() => {
                this.resetStatesPerformed = true;
            });
    }
}
class FakeViewProjection extends EventProjection {
    initialize() {
        this._store = this.createStore('default');
    }

    handleFakeEventFakeCreated(event) {
        return this._store.loadState(event.aggregateId)
            .then((state) => {
                state.isNew = false;
                return this._store.saveState(event.aggregateId, state);
            });
    }

    handleFakeEventFakeNameSet(event) {
        return this._store.loadState(event.aggregateId)
            .then((state) => {
                state.name = event.eventData.name;
                state.nameChanges = state.nameChanges ? state.nameChanges + 1 : 1;
                return this._store.saveState(event.aggregateId, state);
            });
    }

    handleFakeEventDeleted(event) {
        return this._store.removeState(event.aggregateId);
    }

    getFakeView(id) {
        return this._store.loadState(id);
    }

    resetStates() {
        return super.resetStates()
            .then(() => {
                this.resetStatesPerformed = true;
            });
    }
}
class FakeCounterProjection extends EventProjection {
    initialize() {
        this._store = this.createStore('default');
    }

    handleFakeEventFakeNameSet(event) {
        return this._store.loadState(event.aggregateId)
            .then((state) => {
                state.nameChanges = _.get(state, 'nameChanges', 0) + 1;
                return this._store.saveState(event.aggregateId, state);
            });
    }

    getNameChanges(id) {
        return this._store.loadState(id)
            .then((state) => {
                return state.nameChanges;
            });
    }
}
class CustomProjectionConstructor extends EventProjection {
    initialize(param) {
        this.param = param;
    }

    handleSecondScopeEventSecondEvent(payload) { }
}

class ProjectionComponent extends EventProjectionComponent {
    initialize() {
        this._store = this.createStore('default');
    }

    handleUserEventCreated(payload) {
        return this._store.updateState(payload.aggregateId, payload.eventData);
    }
}
class ComplexProjection extends EventProjection {
    static getComponents() {
        return ProjectionComponent;
    }

    get componentChanges() {
        return this._componentChanges;
    }

    initialize() {
        this._componentChanges = [];
    }

    componentStateChanged(key, state, store, projectionClass) {
        this._componentChanges.push({
            store: store,
            key: key,
            state: state,
            projectionClass: projectionClass
        });
    }
}
class ComplexProjectionA extends ComplexProjection { }
class ComplexProjectionB extends ComplexProjection { }

class OrderProjection extends EventProjection {
    setEventCallback(callback) {
        this._eventCallback = callback;
    }

    handleFakeEventFakeNameSet() {
        return this._eventCallback();
    }
}
class OrderSaga extends Saga {
    setEventCallback(callback) {
        this._eventCallback = callback;
    }

    handleFakeEventFakeNameSet(event) {
        return this._eventCallback(event);
    }

    handleFakeEventFailedWithBadRequest() {
        return BadRequestError.promise('bad request');
    }

    resetStates() {
        return super.resetStates()
            .then(() => {
                this.resetStatesPerformed = true;
            });
    }
}

describe('BoundedContext', () => {
    /** @type {MockServiceBus} **/
    let bus = null;
    /** @type {MemoryStateStore} **/
    let stateStore = null;
    /** @type {MockEventStore} **/
    let eventStore = null;
    /** @type {BoundedContext} **/
    let context = null;

    beforeEach(() => {
        eventStore = new MockEventStore();
        stateStore = new MemoryStateStore();
        bus = new MockServiceBus();
        bus.emulateBusConstructed();
        bus.emulateBusConnected();

        context = new BoundedContext(eventStore, stateStore, (name) => {
            return new Domain(bus, name);
        });
    });

    describe('getAggregate', () => {
        it('accept sub-classing', () => {
            return context.getAggregate(FakeAggregate, 'id')
                .then((aggregate) => {
                    expect(aggregate).to.exist;
                    expect(aggregate).is.instanceof(FakeAggregate);
                });
        });

        it('accept multiple sub-classing', () => {
            return context.getAggregate(FakeAggregateSubclass, 'id')
                .then((aggregate) => {
                    expect(aggregate).to.exist;
                    expect(aggregate).is.instanceof(FakeAggregateSubclass);
                });
        });

        it('reject bad classes', () => {
            return expect(context.getAggregate(BadAggregate, 'id')).to.be.rejectedWith(GenericError);
        });

        it('reject non-classes', () => {
            return expect(context.getAggregate({}, 'id')).to.be.rejectedWith(GenericError);
        });

        it('reject failed construction', () => {
            return expect(context.getAggregate(FailedAggregate, 'id')).to.be.rejectedWith(GenericError);
        });
    });

    describe('pushEvents', () => {
        it('normal', () => {
            return context.getAggregate(FakeAggregate, 'id')
                .then((aggregate) => {
                    aggregate.create();
                    return aggregate.save();
                })
                .then(() => {
                    expect(eventStore.containEvent(`${FakeAggregate.Scope}.event.fakeCreated`, 'id', { }));
                });
        });
    });

    describe('load aggregate with events', () => {
        it('check version', () => {
            return context.getAggregate(FakeAggregate, 'id')
                .then((aggregate) => {
                    aggregate.create();
                    return aggregate.save();
                })
                .then(() => {
                    return context.getAggregate(FakeAggregate, 'id');
                })
                .then((loadedAggregate) => {
                    expect(loadedAggregate.version).to.be.equal(1);
                    expect(loadedAggregate.isNew).to.be.false;
                });
        });

        it('check event handlers', () => {
            return context.getAggregate(FakeAggregate, 'id')
                .then((aggregate) => {
                    aggregate.create();
                    aggregate.setName('Aragorn');
                    return aggregate.save();
                })
                .then(() => {
                    return context.getAggregate(FakeAggregate, 'id');
                })
                .then((loadedAggregate) => {
                    expect(loadedAggregate.name).to.be.equal('Aragorn');
                });
        });
    });

    describe('binding aggregates', () => {
        it('valid processor', () => {
            return context.bindAggregate(FakeAggregate)
                .then(() => {
                    const processors = context.getProcessors();
                    expect(_.size(processors)).to.be.equal(1);

                    const entry = processors[0];
                    expect(entry.scope).to.be.equal(context.getGlobalScope(FakeAggregate.Scope));

                    const processor = entry.processor;
                    expect(processor).to.exist;


                    return processor.processCommandSomeCommand({aggregateId: 'id'})
                })
                .then((result) => {
                    expect(result).to.be.equal('ok');
                });
        });

        it('sync command handlers', () => {
            return context.bindAggregate(FakeAggregate)
                .then(() => {
                    const processor = context.getProcessors()[0].processor;
                    return processor.processCommandSync({aggregateId: 'id'});
                })
                .then((result) => {
                    expect(result).to.be.equal('ok');
                });
        });

        it('custom aggregate factory', () => {
            return context.bindAggregate(CustomAggregateConstructor, (context, id) => { return new CustomAggregateConstructor(context, id, 'customData')})
                .then(() => {
                    return context.getAggregate(CustomAggregateConstructor, 'id');
                })
                .then((aggregate) => {
                    expect(aggregate.param).to.be.equal('customData');
                });
        });

        it('events push conflict retry', () => {
            let conflictCounter = 7;
            eventStore.pushAggregateEventsOverride = (aggregateId, aggregateVersion, events) => {
                if (--conflictCounter < 0) {
                    eventStore.pushAggregateEventsOverride = null;
                    return eventStore.pushAggregateEvents(aggregateId, aggregateVersion, events);
                }

                return ConflictError.promise('event push conflict', {
                    eventPushConflict: true,
                    aggregateVersion
                });
            };

            return context.bindAggregate(FakeAggregate)
                .then(() => {
                    const processor = context.getProcessors()[0].processor;
                    return processor['processCommandSetName']({ aggregateId: 'id', name: 'name' });
                })
                .then(() => expect(conflictCounter).to.be.eql(-1));
        });
    });

    describe('binding projections', () => {
        it('valid event processor', () => {
            return context.bindProjection(FakeProjection)
                .then(() => {
                    const processors = context.getProcessors();
                    expect(_.size(processors)).to.be.equal(1);

                    const entry = processors[0];
                    expect(entry.scope).to.be.equal('com.onbadge.scope');

                    const processor = entry.processor;
                    expect(processor).to.exist;

                    const projection = context.getProjection(FakeProjection);
                    expect(projection).to.exist;

                    return processor.processEventEvent({ streamVersion: 1, id: 'id' })
                        .then(() => {
                            expect(context.getProjection(FakeProjection).eventCounter).to.be.equal(1);
                            return stateStore.getState(FakeProjection.name + 'Default', 'id');
                        })
                        .then((state) => {
                            expect(state.someEvent).to.be.true;
                        });
                });
        });
        it('valid query processor', () => {
            return context.bindProjection(FakeProjection)
                .then(() => {
                    const processors = context.getProcessors();
                    expect(_.size(processors)).to.be.equal(1);

                    const entry = processors[0];
                    expect(entry.scope).to.be.equal('com.onbadge.scope');

                    const processor = entry.processor;
                    expect(processor).to.exist;

                    const projection = context.getProjection(FakeProjection);
                    expect(projection).to.exist;

                    return processor.processQueryQuery({ id: 'id' })
                        .then((result) => {
                            expect(result).to.be.eql({ id: 'id' });
                        });
                });
        });
        it('custom projection initialize arguments', () => {
            return context.bindProjection(CustomProjectionConstructor, 'customData')
                .then(() => {
                    return context.getProjection(CustomProjectionConstructor);
                })
                .then((projection) => {
                    expect(projection.param).to.be.equal('customData');
                });
        });
        it('multiple projections overlap bug', () => {
            return Promise.all([
                context.bindProjection(FakeProjection),
                context.bindProjection(CustomProjectionConstructor)
            ])
            .then(() => {
                const entries = context.getProcessors();

                const firstScope = _.find(entries, (entry) => { return entry.scope === 'com.onbadge.scope'});
                expect(_.isFunction(firstScope.processor['processEventEvent']), 'should contain handler for Event within com.onbadge.scope').to.be.true;
                expect(_.isFunction(firstScope.processor['processEventSecondEvent']), 'should not contain handler for SecondEvent within com.onbadge.anotherScope').to.be.false;

                const secondScope = _.find(entries, (entry) => { return entry.scope === 'com.onbadge.secondScope'});
                expect(_.isFunction(firstScope.processor['processEventEvent']), 'should not contain handler for Event within com.onbadge.scope').to.be.true;
                expect(_.isFunction(firstScope.processor['processEventSecondEvent']), 'should contain handler for SecondEvent within com.onbadge.anotherScope').to.be.false;
            });
        });
        it('sync projection handlers', () => {
            return context.bindProjection(FakeProjection)
                .then(() => {
                    const processor = context.getProcessors()[0].processor;
                    return processor['processEventSyncEvent']({ streamVersion: 1 });
                });
        });
    });

    describe('restoring projection', () => {
        beforeEach(() => {
            return Promise.all([
                    context.bindProjection(FakeViewProjection),
                    context.bindAggregate(FakeAggregate)
                ])
                .then(() => {
                    return context.getAggregate(FakeAggregate, 'id')
                })
                .then((aggregate) => {
                    aggregate.create();
                    aggregate.setName('name');
                    aggregate.commandNotInteresting();
                    return aggregate.save();
                });
        });

        it('restore fake aggregate projection', () => {
            return context.reviveProjections()
                .then(() => {
                    const projection = context.getProjection(FakeViewProjection);
                    expect(projection.eventCounter).to.be.equal(3);
                    return projection.getFakeView('id');
                })
                .then((fakeView) => {
                    expect(fakeView.name).to.be.equal('name');
                    expect(fakeView.isNew).to.be.false;
                });
        });

        it('restore multiples projections', () => {
            return context.bindProjection(FakeCounterProjection)
                .then(() => {
                    return context.reviveProjections();
                })
                .then(() => {
                    return context.getProjection(FakeViewProjection).getFakeView('id');
                })
                .then((fakeView) => {
                    expect(fakeView.name).to.be.equal('name');
                    expect(fakeView.isNew).to.be.false;
                    return expect(context.getProjection(FakeCounterProjection).getNameChanges('id')).to.be.eventually.equal(1);
                });
        });

        it('partial restore', () => {
            return context.reviveProjections()
                .then(() => {
                    return context.getAggregate(FakeAggregate, 'id');
                })
                .then((aggregate) => {
                    aggregate.setName('newName');
                    return aggregate.save();
                })
                .then(() => {
                    return context.reviveProjections();
                })
                .then(() => {
                    return context.getProjection(FakeViewProjection).getFakeView('id');
                })
                .then((view) => {
                    expect(view.nameChanges).to.be.equal(2);
                });
        });
    });

    describe('revive sagas and projections', () => {
        let projection = null;
        let saga = null;

        beforeEach(() => {
            return Promise.all([
                    context.bindSaga(OrderSaga),
                    context.bindProjection(OrderProjection),
                    context.bindAggregate(FakeAggregate)
                ])
                .then(() => {
                    projection = context.getProjection(OrderProjection);
                    saga = context.getProjection(OrderSaga);
                    return context.getAggregate(FakeAggregate, 'id')
                })
                .then((aggregate) => {
                    aggregate.create();
                    aggregate.setName('name');
                    aggregate.setName('newName');
                    aggregate.setName('pikachu');
                    aggregate.setName('slowpoke');
                    return aggregate.save();
                });
        });

        it('saga should be restored first', () => {
            let projectionCounter = 0;
            let sagaCounter = 0;

            projection.setEventCallback(() => {
                expect(sagaCounter).to.be.equal(0);
                projectionCounter++;
                return new Promise((resolve) => {
                    setTimeout(resolve, 10);
                })
            });

            saga.setEventCallback(() => {
                expect(projectionCounter).to.be.equal(4);
                sagaCounter++;
                return new Promise((resolve) => {
                    setTimeout(resolve, 10);
                })
            });

            return context.reviveProjections()
                .then(() => {
                    expect(sagaCounter).to.be.equal(4);
                });
        });
    });
    
    describe('revive saga with errors', () => {
        /** @type {OrderSaga} */
        let saga = null;

        beforeEach(() => {
            return Promise.all([
                    context.bindSaga(OrderSaga),
                    context.bindAggregate(FakeAggregate)
                ])
                .then(() => {
                    saga = context.getProjection(OrderSaga);
                    return context.getAggregate(FakeAggregate, 'id')
                })
                .then((aggregate) => {
                    aggregate.create();
                    aggregate.setName('name');
                    return aggregate.save();
                });
        });

        it('tolerance to request timeout errors', () => {
            let firstAttempt = true;
            let success = false;
            saga.setEventCallback(() => {
                if (firstAttempt) {
                    firstAttempt = false;
                    return RequestTimeoutError.promise();
                }
                success = true;
                return Promise.resolve();
            });

            return context.reviveProjections()
                .then(() => {
                    expect(success).to.be.true;
                });
        });

        it('tolerance to conflict errors', () => {
            let firstAttempt = true;
            let success = false;
            saga.setEventCallback(() => {
                if (firstAttempt) {
                    firstAttempt = false;
                    return ConflictError.promise();
                }
                success = true;
                return Promise.resolve();
            });

            return context.reviveProjections()
                .then(() => {
                    expect(success).to.be.true;
                });
        });
    });
    
    describe('resetting projections', () => {
        beforeEach(() => {
            return Promise.all([
                context.bindProjection(FakeViewProjection),
                context.bindProjection(FakeProjection),
                context.bindSaga(OrderSaga)
            ]);
        });

        it('reset all projections', () => {
            return context.resetProjections().then(() => {
                expect(context.getProjection(FakeViewProjection).resetStatesPerformed).to.be.true;
                expect(context.getProjection(FakeProjection).resetStatesPerformed).to.be.true;
                expect(context.getProjection(OrderSaga).resetStatesPerformed).to.not.exist;
            });
        });

        it('reset all projections & sagas', () => {
            return context.resetProjections(true).then(() => {
                expect(context.getProjection(FakeViewProjection).resetStatesPerformed).to.be.true;
                expect(context.getProjection(FakeProjection).resetStatesPerformed).to.be.true;
                expect(context.getProjection(OrderSaga).resetStatesPerformed).to.be.true;
            });
        });
    });

    describe('local/remote queries and commands', () => {
        beforeEach(() => {
            bus.listen('com.onbadge.scope', MockServiceBus.Query, 'remoteQuery', (payload) => {
                expect(payload.valid).to.be.true;
                return Promise.resolve({
                    remote: true
                });
            });

            bus.listen('com.onbadge.fake', MockServiceBus.Command, 'remoteCommand', (payload) => {
                expect(payload.valid).to.be.true;
                return Promise.resolve({
                    remote: true
                });
            });

            bus.listen('com.onbadge.sub.scope', MockServiceBus.Command, 'remoteCommand', () => {
                return Promise.resolve({
                    remote: true
                });
            });

            return Promise.all([
                context.bindProjection(FakeProjection),
                context.bindAggregate(FakeAggregate)
            ]);
        });

        it('local query', () => {
            return context.query('scope.query', { local: true })
                .then((result) => {
                    expect(result.local).to.be.true;
                });
        });

        it('remote query', () => {
            return context.query('scope.remoteQuery', { valid: true })
                .then((result) => {
                    expect(result.remote).to.be.true;
                });
        });

        it('local command', () => {
            return context.command('fake.someCommand', { aggregateId: 'id' })
                .then((result) => {
                    expect(result).to.be.equal('ok');
                });
        });

        it('remote command', () => {
            return context.command('fake.remoteCommand', { valid: true })
                .then((result) => {
                    expect(result.remote).to.be.true;
                });
        });

        it('bug - reversed local scope', () => {
            return context.command('sub.scope.remoteCommand', {})
                .then((result) => {
                    expect(result.remote).to.be.true;
                });
        });
    });

    describe('projection components', () => {
        /** @type {ComplexProjection} */
        let projectionA = null;
        /** @type {ComplexProjection} */
        let projectionB = null;

        beforeEach(() => {
            return Promise.all([
                    context.bindProjection(ComplexProjectionA),
                    context.bindProjection(ComplexProjectionB)
                ])
                .then(() => {
                    projectionA = context.getProjection(ComplexProjectionA);
                    projectionB = context.getProjection(ComplexProjectionB);
                });
        });

        it.skip('component changes', () => {
            const processors = context.getProcessors();
            expect(_.size(processors)).to.be.equal(1);

            const entry = processors[0];
            expect(entry.scope).to.be.equal('com.onbadge.user');

            const processor = entry.processor;

            return processor['processEventCreated']({ streamVersion: 1, aggregateId: 'id', eventData: { title: 'title' } })
                .then(() => {
                    const expectChanges = (projection) => {
                        expect(_.size(projection.componentChanges)).to.be.equal(1);
                        const changes = projection.componentChanges[0];
                        expect(changes.key).to.be.equal('id');
                        expect(changes.state).to.be.eql({ id: 'id', title: 'title' });
                    };

                    expectChanges(projectionA);
                    expectChanges(projectionB);
                });
        });
    });
});
