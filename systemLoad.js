import _ from 'lodash';
import os from 'os';

export class SystemLoad {
    /**
     * @private
     * @type {SystemLoad|null}
     */
    static singleTone = null;

    /**
     * @public
     * @returns {SystemLoad}
     */
    static get() {
        if (_.isNull(this.singleTone))
            this.singleTone = new SystemLoad();
        return this.singleTone;
    }

    /**
     * @public
     */
    static dispose() {
        if (this.singleTone) {
            this.singleTone.dispose();
            this.singleTone = null;
        }
    }

    /**
     * @public
     * @returns {Number}
     */
    get cpuCount() {
        return _.size(this._cpus);
    }

    /**
     * @private
     */
    constructor() {
        this.measureLoop();
    }

    /**
     * @private
     */
    updateAdditionalCpuInfo() {
        _.forEach(this._cpus, (cpu) => {
            let totalTime = 0;
            _.forEach(cpu.times, (value, key) => {
                totalTime += cpu.times[key];
            });

            _.forEach(cpu.times, (value, key) => {
                _.set(cpu, `timesPercentage.${key}`, cpu.times[key] / totalTime);
            });
        });
    }

    /**
     * @private
     */
    measureLoop() {
        const cpus = os.cpus();
        if (_.isNil(this._cpus)) {
            this._cpus = cpus;
        } else {
            _.forEach(cpus, (cpu, index) => {
                const currentCpu = this._cpus[index];
                const lastCpu = this._lastCpus[index];

                _.forEach(lastCpu.times, (value, key) => {
                    currentCpu.times[key] = cpu.times[key] - lastCpu.times[key];
                });
            });
        }

        this._lastCpus = cpus;
        this.updateAdditionalCpuInfo();

        this._measureTimer = setTimeout(() => {
            this._measureTimer = null;
            this.measureLoop();
        }, 1000);
    }

    /**
     * @private
     */
    dispose() {
        if (!_.isNil(this._measureTimer)) {
            clearTimeout(this._measureTimer);
            this._measureTimer = null;
        }
    }

    /**
     * @private
     */
    formatLoad(load) {
        let result = `${Math.round(load * 100)}%`;
        if (_.size(result) === 2)
            result = '0' + result;
        return result;
    }

    /**
     * @public
     * @param {Number} core
     * @returns {String}
     */
    getCpuLoad(core) {
        if (core < this.cpuCount) {
            const cpuTimes = this._cpus[core].timesPercentage;
            return `U-${this.formatLoad(cpuTimes.user)} S-${this.formatLoad(cpuTimes.sys)} I-${this.formatLoad(cpuTimes.idle)} (${this._cpus[core].model})`;
        }
        return 'invalid cpu index';
    }
}
