import _ from 'lodash';
import Promise from 'bluebird';
import { BadRequestError, GenericError } from '@badgeteam/liberr';

export class UberUtils {
    /**
     * @public
     * @param {Object} instance - class instance
     * @param {string} prefix - method prefix (filter)
     * @param {Function} callback - function(method, entry)
     * @param {Object} [objectClass] - used for recursion
     */
    static enumerateClassMethods(instance, prefix, callback, objectClass = null) {
        if (_.isNull(objectClass))
            objectClass = Object.getPrototypeOf(instance);

        _.forEach(Object.getOwnPropertyNames(objectClass), (key) => {
            if (_.startsWith(key, prefix) && _.isFunction(instance[key]))
                callback(key, _.lowerFirst(key.replace(prefix, '')));
        });

        let parent = Object.getPrototypeOf(objectClass);
        if (parent !== Object.prototype)
            this.enumerateClassMethods(instance, prefix, callback, parent);
    }

    /**
     * @public
     * @param {Object} instance - class instance
     * @param {string} prefix - method prefix (filter)
     * @param {Function} callback - function(method, entry)
     */
    static enumerateInstanceMethods(instance, prefix, callback) {
        _.forEach(instance, (value, key) => {
            if (_.startsWith(key, prefix) && _.isFunction(instance[key]))
                callback(key, _.lowerFirst(key.replace(prefix, '')));
        });
    }

    /**
     * @public
     * @param {Object} instance - class instance
     * @param {string} prefix - method prefix (filter)
     * @param {Function} callback - function(method, entry)
     */
    static enumerateAllMethods(instance, prefix, callback) {
        this.enumerateClassMethods(instance, prefix, callback);
        this.enumerateInstanceMethods(instance, prefix, callback);
    }

    /**
     * @public
     * @returns {String} of empty fields
     */
    static ensureNonEmptyFields(object, ...fields) {
        const result = [];
        _.forEach((fields), (field) => {
            const value = _.get(object, field);
            if (_.isNil(value) || (_.isString(value) && _.isEmpty(value)))
                result.push(field);
        });
        return result.join(', ');
    }

    /**
     * @public
     * @returns {Object|Error}
     */
    static pick(payload, ...fields) {
        if (!payload)
            throw new BadRequestError(`payload couldn\'t be empty`);

        let fieldArray = [];

        _.forEach(fields, (field) => {
            fieldArray = fieldArray.concat(field);
        });

        const defaultFetcher = function(source, target, descriptor) {
            const value = _.get(source, descriptor.name, undefined);
            if (!_.isUndefined(value))
                _.set(target, descriptor.map, value);
        };

        const createFetcher = function(fieldName, descriptor) {
            const lodashRe = new RegExp('_\\.([A-z].+)\\(([A-z].+)\\)');
            const matches = fieldName.match(lodashRe);
            if (_.size(matches) === 3) {
                descriptor.name = matches[2];
                const lodashFuncName = matches[1];
                if (!_.isFunction(_[lodashFuncName]))
                    throw new GenericError(`lodash does not supports ${lodashFuncName} function!`);

                descriptor.fetcher = function(source, target, descriptor) {
                    const value = _.get(source, descriptor.name, undefined);
                    if (!_.isUndefined(value))
                        _.set(target, descriptor.map, _[lodashFuncName](value));
                };
            } else {
                descriptor.fetcher = defaultFetcher;
                descriptor.name = fieldName;
            }
        };

        const descriptors = [];

        _.forEach(fieldArray, (field) => {
            field = _.trim(field);
            const descriptor = { required: true };

            if (_.startsWith(field, '#')) {
                field = field.substring(1);
                descriptor.required = false;
            }

            const map = _.split(field, '=>');
            if (_.size(map) == 2) {
                createFetcher(_.trim(map[0]), descriptor);
                descriptor.map = _.trim(map[1]);
            } else {
                createFetcher(field, descriptor);
                descriptor.map = descriptor.name;
            }

            descriptors.push(descriptor);
        });

        const requiredFields = _.map(_.filter(descriptors, _.matches({ required: true })), (descriptor) => {
            return descriptor.name;
        });

        const missedFields = UberUtils.ensureNonEmptyFields(payload, ...requiredFields);
        if (!missedFields) {
            const result = {};
            _.forEach(descriptors, (descriptor) => {
                descriptor.fetcher(payload, result, descriptor);
            });
            return result;
        }
        throw new BadRequestError(`payload field(s) [${missedFields}] couldn\'t be empty`);
    }

    /**
     * @public
     * @returns {Promise}
     */
    static promisePick(payload, ...fields) {
        return new Promise((resolve) => {
            return resolve(this.pick(payload, ...fields));
        });
    }

    /**
     * @public
     */
    static rename(payload, key, newKey) {
        const value = _.get(payload, key, undefined);
        if (!_.isUndefined(value)) {
            _.set(payload, newKey, value);
        } else {
            _.unset(payload, newKey);
        }
        _.unset(payload, key);

        return payload;
    }

    /**
     * @public
     * @param {String} source - source string in came case notation (someBadgeProcessor) for example
     * @param {Function} callback - callback function for words someCallback(letter, index)
     * @returns {String} converted string
     */
    static convertCamelCaseWordLetters(source, callback) {
        let index = 0;
        return _.replace(source, /([A-Z])/g, (a, b) => {
            return callback(b, index++);
        });
    }

    /**
     * @public
     * @param {Function} constructor - class constructor
     * @returns {String}
     */
    static classNameToLogName(constructor) {
        return this.convertCamelCaseWordLetters(constructor.name, (letter, index) => {
            if (index === 0)
                return _.toLower(letter);
            return '-' + _.toLower(letter);
        });
    }
}
