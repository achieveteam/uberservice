import _ from 'lodash';
import Debug from 'debug-logger';
import Le from 'le_node';
import DxLogger from 'dx-logger-client';
import util from 'util';
import now from 'performance-now';
import { Environment } from './environment';

export class Logger {
    /**
     * @private
     */
    static _buffer = null;
    /**
     * @private
     */
    static _bufferSize = 0;

    /**
     * @private
     */
    static ensureBuffer() {
        if (!this._buffer) {
            this._bufferSize = Environment.isDevelopment ? 128 : 0;
            this._buffer = Environment.isDevelopment ? [] : null;
        }
        return this._buffer;
    }

    /**
     * @public
     */
    static getBuffer() {
        if (this._buffer) {
            const overflow = _.size(this._buffer) - this._bufferSize;

            if (overflow > 0)
                this._buffer = _.drop(this._buffer, overflow);

            return this._buffer;
        }
        return [];
    }

    /**
     * @public
     */
    static clearBuffer() {
        this._buffer = null;
        this._bufferSize = 0;
    }

    /**
     * @public
     * @returns {String}
     */
    get name() {
        return this._name;
    }

    static isRemote = false;
    static remoteLogger = null;

    static loggerFactory(name) { return Debug(name) };

    static useConfig(config) {
        if (config.remoteLogger && config.remoteLogger.token) {
            Logger.isRemote = true;
            Logger.loggerFactory = () => {
                if (_.isNil(Logger.remoteLogger)) {
                    if (Environment.isProduction)
                        Logger.remoteLogger = new Le({
                            token: config.remoteLogger.token,
                            withStack: true,
                            console: true,
                            levels: {warn: 4, error: 5}
                        });
                    Logger.remoteLogger = new DxLogger({token: config.remoteLogger.token});
                }
                return Logger.remoteLogger;
            };
        }
    }

    /**
     * @constructor
     * @param {String} name
     */
    constructor(name) {
        this._name = name;
        this._logger = Logger.loggerFactory(name);
        this._measureMap = new Map();
    }

    /**
     * @private
     */
    dispatchMessage(level, ...args) {
        const message = util.format(...args);

        this._logger[level](this.addStamp(message));

        const buffer = this.constructor.ensureBuffer();
        if (buffer)
            buffer.push(`${this.name}:${level} ${this.addTime(message)}`);
    }

    /** Info level log messages (stdout). Enabled in production. **/
    info(...args) {
        this.dispatchMessage('info', ...args);
    }

    /** Debug level log messages (stdout) **/
    debug(...args) {
        this.dispatchMessage('debug', ...args);
    }

    /** Error messages (stderr). Can inspect errors. **/
    error(...args) {
        this.dispatchMessage('error', ...args);
    }

    /** Warning messages (stderr) **/
    warn(...args) {
        this.dispatchMessage('warn', ...args);
    }

    /** Inspect objects at warning level **/
    inspect(object) {
        if (!Logger.isRemote)
            this._logger.dir(object, 'debug');
        else
            this.debug(util.inspect(object, {showHidden: true, dept: null}));
    }

    /** Show current memory usage with bit shift **/
    memory(msg, unit, shift) {
        let memory = process.memoryUsage();
        this.dispatchMessage('debug', msg + ' (rss: '
            + (memory.rss >> shift) + ' ' + unit
            + ', heapTotal: ' + (memory.heapTotal >> shift) + ' ' + unit
            + ', heapUsed: ' + (memory.heapUsed >> shift) + ' ' + unit + ')');
    }

    /** Show current memory usage in megabytes **/
    memoryMb(msg) {
        this.memory(msg, 'Mb', 20);
    }

    /** Show current memory usage in kilobytes **/
    memoryKb(msg) {
        this.memory(msg, 'Kb', 10);
    }

    /** Show error output on assertion failure **/
    assert(condition) {
        if (!Logger.isRemote)
            this._logger.assert(condition);
        else if (!condition) {
            let err = new Error('Assertion error!');
            this.error(['Assertion error!', err]);
            throw err;
        }
    }

    addTime(log) {
        return `${new Date().toLocaleTimeString('en-US', {hour12: false})} ${log}`;
    }

    addStamp(log) {
        if (Logger.isRemote)
            return new Date().toUTCString() + ' ' + this.name + ': '+log;
        else
            return log;
    }

    /**
     * Flush buffers
     * @public
     */
    flush() {
        if (Logger.isRemote && !_.isNil(Logger.remoteLogger))
            return Logger.remoteLogger.end();
    }

    measureStart(name) {
        this._measureMap.set(name, now());
    }

    /**
     * @param {String} name - measure name
     * @param {String} format - format string (use {ms}, {s}, {min} as time placeholders)
     * @param {String} [level]
     * @returns {Number}
     */
    measureEnd(name, format = '', level = 'debug') {
        const start = this._measureMap.get(name);
        let ms, s, min;
        if (_.isUndefined(start)) {
            ms = s = min = '(not started)';
        } else {
            ms = (now() - start).toFixed(0);
            s = Math.round(ms / 1000);
            min = Math.round(s / 60);
        }

        this._measureMap.delete(name);

        if (!_.isEmpty(format)) {
            format = format
                .replace('{ms}', ms.toString())
                .replace('{s}', s.toString())
                .replace('{min}', min.toString());

            this.dispatchMessage(level, format);
        }

        return { ms, s, min };
    }
}