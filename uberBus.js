import _ from 'lodash';
import Promise from 'bluebird';
import { EventEmitter2 } from 'eventemitter2';
import amqp from 'amqplib';
import util from 'util';
import uuid from 'node-uuid';
import { Logger } from './logger';
import { UberStateMachine } from './uberStateMachine';
import {
    ErrorFactory,
    BaseError,
    GenericError,
    ServiceUnavailableError,
    NotFoundError,
    RequestTimeoutError,
    ConflictError
} from '@badgeteam/liberr';

export class UberBus {
    static Command = 'command';
    static Query = 'query';
    static Event = 'event';
    static Notice = 'notice';

    static Ok = 'ok';
    static Error = 'error';

    static StateChangeCallback = 'stateChangeCallback'; /* (oldState, newState) */
    static ReadyCallback = 'readyCallback';
    static LostCallback = 'lostCallback';

    static DefaultTimeout = 5000;

    /**
     * @private
     * @param args
     * @returns {string}
     */
    static route(...args) {
        return [].concat(args).join('.');
    }

    /**
     * Constructor
     * @param config {object} bus config
     * @param service {string} name of service is hosting bus
     */
    constructor(config, service) {
        this._id = config.id || uuid.v4().toString();
        this._service = service;
        this._listeners = {};
        this._pendings = {};
        this._eventEmitter = new EventEmitter2();

        this._logger = new Logger(_.get(config, 'debugName', 'unknown-bus'));

        this._stateMachine = this.createStateMachine();
        this._stateMachine.onStateChanged((oldState, newState) => {
            this._logger.debug(util.format('state changed to [%s]', newState));
            this._eventEmitter.emit(UberBus.StateChangeCallback, oldState, newState);
        });

        this.assertConfig(config);
        //if (this.stateMachine.states.current != this.stateMachine.states.failure)
          //  this.startAcquire();
    }

    get isReady() { return this._stateMachine.states.current == this._stateMachine.states.ready.name; }

    get eventsQueue() { return UberBus.route(this._service, UberBus.Event, this._id); }

    get stateMachine() { return this._stateMachine; }

    /**
     * @param name {string} event name
     * @param callback {function} callback
     */
    on(name, callback) { this._eventEmitter.on(name, callback); }

    createStateMachine() {
        return new UberStateMachine()
            .state('uninitialized', { initial: true })
            .state('acquire')
            .state('failure')
            .state('ready', {
                enter: () => { this._eventEmitter.emit(UberBus.ReadyCallback); },
                leave: () => { this._eventEmitter.emit(UberBus.LostCallback); }
            })
            .event('busConstructed', 'uninitialized', 'acquire')
            .event('failed', 'uninitialized', 'failure')
            .event('failed', 'ready', 'acquire')
            .event('connectionError', 'uninitialized', 'failure')
            .event('connectionError', 'ready', 'acquire')
            .event('invalidConfig', 'uninitialized', 'failure')
            .event('busConnected', 'acquire', 'ready')
            .event('unrecoverableError', ['acquire', 'ready'], 'failure');
    }

    /**
     * @private
     * @param {Object} config Bus configuration
     */
    assertConfig(config) {
        this._config = config;
        this._url = _.get(config, 'url', null);
        this._socketOptions = _.get(config, 'socketOptions', null);
        this._channelOptions = _.defaults({}, _.get(config, 'channelOptions', null), this._channelOptions, { prefetch: 1 });
        this._reconnectInterval = _.get(config, 'reconnectInterval', 500);

        if (!this._url || !this._socketOptions) {
            this._logger.error('invalid configuration object:');
            this._logger.error(config);
            this.stateMachine.states.invalidConfig();
        }
    }

    /**
     * @public
     * Begin acquire sequence
     */
    startAcquire() {
        process.nextTick(() => {
            this.stateMachine.states.busConstructed();
            this.acquire();
        });
    }

    /**
     * @private
     */
    acquire(attempt = 0) {
        this._logger.info('acquire - attempt %d', attempt);
        amqp.connect(this._url, this._socketOptions)
            .then((connection) => {
                this._logger.debug('aqcuire - amqp connection established');
                this._connection = connection;
                this._connection.on('error', (err) => {
                    this.amqpError(err);
                });
                this._logger.debug('aqcuire - opening CQN channel');
                return this._connection.createChannel()
            })
            .then((channel) => {
                this._logger.debug('aqcuire - CQN channel opened, set options');
                this._channel = channel;
                this._channel.on('error', (err) => {
                    this.amqpError(err);
                });
                return this._channel.prefetch(this._channelOptions.prefetch);
            })
            .then(() => {
                this._logger.debug('aqcuire - opening Events channel');
                return this._connection.createChannel();
            })
            .then((channel) => {
                this._logger.debug('aqcuire - Events channel opened, set options');
                this._eventChannel = channel;
                this._eventChannel.on('error', (err) => {
                    this.amqpError(err);
                });
                return this._eventChannel.prefetch(this._channelOptions.prefetch);
            })
            .then(() => {
                this._logger.debug('aqcuire - bus setup complete');
                this.stateMachine.states.busConnected();
            })
            .catch((error) => {
                this._logger.error('acquire - amqp connection error %s', error.message);
                this._logger.info('acquire - enqueue next attempt after %d ms', this._reconnectInterval);
                setTimeout(() => { this.acquire(attempt + 1); }, this._reconnectInterval);
            });
    }

    /**
     * @private
     */
    dispose() {
        if (this._channel) {
            this._channel.removeAllListeners();
            this._channel = null;
        }
        if (this._eventChannel) {
            this._eventChannel.removeAllListeners();
            this._eventChannel = null;
        }
        if (this._connection) {
            this._connection.removeAllListeners();
            this._connection = null;
        }
        this._listeners = {};
        this._pendings = {};
    }

    /**
     * @private
     */
    amqpError(error) {
        this._logger.error('bus connection interrupted with error ' + error.stack);
        this.stateMachine.states.connectionError();
        this.dispose();
        this.acquire();
    }

    startPending(pendingId, stop, timeout) {
        const timeoutId = setTimeout(() => {
            stop();
        }, timeout);
        this._pendings[pendingId] = {
            timeoutId: timeoutId,
            stop: stop
        };
    }

    stopPending(pendingId) {
        const pending = this._pendings[pendingId];
        if (pending) {
            clearTimeout(pending.timeoutId);
            delete this._pendings[pendingId];
        }
    }

    static wrap(content) {
        return new Buffer(JSON.stringify(content));
    }

    static unwrap(message) {
        if (message)
            return JSON.parse(message.content);
        return null;
    }

    handleEvent(message) {
        const content = UberBus.unwrap(message);
        if (content) {
            const routingKey = message.fields.routingKey;
            const listener = this._listeners[routingKey];

            if (listener && _.isFunction(listener.handler)) {
                return listener.handler(content.payload)
                    .catch(ServiceUnavailableError, RequestTimeoutError, ConflictError, NotFoundError, (error) => {
                        const timeout = 500;

                        const currentEventPending = _.get(this, '__currentEventPending__', {
                            name: routingKey,
                            attempt: 0,
                            maxAttempts: 20
                        });

                        if ((currentEventPending.attempt++) >= currentEventPending.maxAttempts) {
                            this._logger.warn(`event handler for ${routingKey} returns error ${error.message} and exceeds maximum retry attempts ${currentEventPending.maxAttempts}, rethrow error`);
                            throw error;
                        }

                        _.set(this, '__currentEventPending__', currentEventPending);

                        this._logger.warn(`event handler for ${routingKey} returns error ${error.message}, retry #${currentEventPending.attempt} this event after ${timeout} ms`);
                        this.startPending('eventRetryPending', () => {
                            this.stopPending('eventRetryPending');
                            this._eventChannel.nack(message);
                        }, timeout);

                        return 'noAck';
                    })
                    .catch((error) => {
                        this._logger.warn(`event handler for ${routingKey} returns unrecoverable error ${error.message}, continue event processing`);
                    })
                    .then((param) => {
                        if (!_.isEqual(param, 'noAck')) {
                            this._logger.debug(`acknowledge event ${content.payload.name} with stream version ${content.payload.streamVersion}`);
                            this._eventChannel.ack(message);
                            _.unset(this, '__currentEventPending__');
                        }
                    })

            }
        }
    }

    handleRequest(message, callback) {
        const content = UberBus.unwrap(message);
        if (content) {
            new Promise((resolve) => {
                callback(content.payload)
                    .then((response) => {
                        return resolve({
                            result: UberBus.Ok,
                            payload: response
                        });
                    })
                    .catch((err) => {
                        const response = {
                            result: UberBus.Error,
                            payload: ErrorFactory.serialize(err)
                        };

                        return resolve(response);
                    });
            })
            .then((response) => {
                if (this._channel)
                    return this._channel.sendToQueue(message.properties.replyTo,
                        UberBus.wrap(response),
                        { noAck: true, correlationId: message.properties.correlationId });
            })
            .catch((err) => {
                this._logger.warn('some strange error has occurred in retrieving response: %s', err);
            });
        }
    }

    static handleResponse(message, correlationId, callback) {
        if (message && message.properties.correlationId == correlationId) {
            const content = JSON.parse(message.content);
            if (content) {
                const responsePromise = new Promise((resolve, reject) => {
                    if (!_.isString(content.result))
                        return reject(new GenericError(`response does not contain required field [result]: ${response}`));
                    switch (content.result) {
                        case UberBus.Ok:
                            return resolve(content.payload);
                        case UberBus.Error:
                            return reject(ErrorFactory.deserialize(content.payload));
                        default:
                            return reject(new GenericError(`response has some troubles in [result] field: ${response}`));
                    }
                });

                callback(responsePromise);
            }
        }
    }

    sendCommand(domain, name, payload, timeout = UberBus.DefaultTimeout) {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');

        const queue = UberBus.route(domain, UberBus.Command, name);

        const correlationId = uuid.v4().toString();
        const replyQueue = UberBus.route(queue, correlationId);

        return new Promise((resolve, reject) => {
            this._channel.assertQueue(replyQueue, { durable: false, exclusive: true, autoDelete: true })
                .then(() => {
                    const stop = (responsePromise = null) => {
                        this.stopPending(correlationId);

                        if (!responsePromise)
                            return reject(new RequestTimeoutError(`no command ${name} scope ${domain} response received (timeout ${timeout} ms)`));

                        responsePromise
                            .then(resolve)
                            .catch(reject)
                            .finally(() => {
                                if (this._channel)
                                    return this._channel.cancel(replyQueue)
                                        .catch((error) => {
                                            this._logger.warn(`channel cancel error ${error.message}`);
                                        });
                            });
                    };

                    this.startPending(correlationId, stop, timeout);

                    return this._channel.consume(replyQueue, (message) => {
                        UberBus.handleResponse(message, correlationId, stop);
                    }, { noAck: true, consumerTag: replyQueue});
                })
                .then(() => {
                    return this._channel.assertQueue(queue, { durable: false, autoDelete: true });
                })
                .then(() => {
                    return this._channel.sendToQueue(queue, UberBus.wrap({ payload: payload }),
                        { noAck: true, expiration: timeout, correlationId: correlationId, replyTo: replyQueue });
                })
                .catch(reject);
        });
    }

    sendQuery(domain, name, payload, filter = null, timeout = UberBus.DefaultTimeout) {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');

        const routingKey = UberBus.route(domain, UberBus.Query, name);

        const correlationId = uuid.v4().toString();
        const replyQueue = UberBus.route(routingKey, correlationId);

        return new Promise((resolve, reject) => {
            let filteredReply = [];
            let stopped = false;

            const stop = () => {
                stopped = true;
                finish();

                if (!filteredReply.length)
                    return reject(new RequestTimeoutError(`no query ${name} scope ${domain} response was received (timeout ${timeout} ms)`));

                return resolve(filteredReply);

            };

            const finish = () => {
                this.stopPending(correlationId);
                if (this._channel)
                    this._channel.cancel(replyQueue)
                        .catch((error) => {
                            this._logger.warn(`channel cancel error ${error.message}`);
                        });
            };


            this.startPending(correlationId, stop, timeout);

            const push = (msg) => {
                if (msg && !stopped)
                    filteredReply.push(msg);
            };

            const handler = (responsePromise) => {
                if (!_.isFunction(filter)) {
                    responsePromise
                        .then(resolve)
                        .catch(reject)
                        .finally(finish);
                }
                else {
                    responsePromise
                        .then((payload) => {
                            filter(payload, push, stop, null);
                        })
                        .catch((err) => {
                            filter(null, push, stop, err);
                        });
                }
            };

            this._channel.assertQueue(replyQueue, { durable: false, exclusive: true, autoDelete: true })
                .then(() => {
                    return this._channel.consume(replyQueue, (message) => {
                        UberBus.handleResponse(message, correlationId, handler)
                    }, { noAck: true, consumerTag: replyQueue });
                })
                .then(() => {
                    return this._channel.assertExchange(domain, 'topic', { durable: false });
                })
                .then(() => {
                    return this._channel.publish(domain, routingKey,
                        UberBus.wrap({ payload: payload }),
                        { noAck: true, expiration: timeout,
                            correlationId: correlationId, replyTo: replyQueue });
                })
                .catch(reject);
        });
    }

    sendNotice(domain, name, payload) {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');
        const routingKey = UberBus.route(domain, UberBus.Notice, name);

        return new Promise((resolve, reject) => {
            this._channel.assertExchange(domain, 'topic', { durable: false })
                .then(() => {
                    return this._channel.publish(domain, routingKey,
                        UberBus.wrap({ payload: payload }), { noAck: true });
                })
                .then(resolve)
                .catch(reject);
        });
    }

    sendEvent(domain, name, payload) {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');
        const routingKey = UberBus.route(domain, UberBus.Event, name);
        return new Promise((resolve, reject) => {
            this._eventChannel.assertExchange(domain, 'topic', { durable: false })
                .then(() => {
                    return this._eventChannel.publish(domain, routingKey,
                        UberBus.wrap({ payload: payload }), { noAck: false });
                })
                .then(resolve)
                .catch(reject);
        });
    }

    registerCommand(queue, callback) {
        const consumerTag = UberBus.route(queue, this._id);

        return {
            start: () => {
                return new Promise((resolve, reject) => {
                    this._channel.assertQueue(queue, { durable: false, autoDelete: true })
                        .then(() => {
                            return this._channel.consume(queue, (message) => {
                                this.handleRequest(message, callback);
                            }, { noAck: true, consumerTag: consumerTag });
                        })
                        .then(() => {
                            return resolve({ queue: queue, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            },
            stop: () => {
                return new Promise((resolve, reject) => {
                    this._channel.cancel(consumerTag)
                        .then(() => {
                            return resolve({ queue: queue, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            }
        };
    }

    registerQuery(domain, routingKey, callback) {
        const queue = UberBus.route(routingKey, this._service);
        const consumerTag = UberBus.route(queue, this._id);

        return {
            start: () => {
                return new Promise((resolve, reject) => {
                    this._channel.assertExchange(domain, 'topic', { durable: false })
                        .then(() => {
                            return this._channel.assertQueue(queue, { durable: false, autoDelete: true })
                        })
                        .then(() => {
                            return this._channel.bindQueue(queue, domain, routingKey);
                        })
                        .then(() => {
                            return this._channel.consume(queue, (message) => {
                                this.handleRequest(message, callback);
                            }, { noAck: true, consumerTag: consumerTag });
                        })
                        .then(() => {
                            return resolve({ queue: queue, domain: domain,
                                routingKey: routingKey, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            },
            stop: () => {
                return new Promise((resolve, reject) => {
                    this._channel.cancel(consumerTag)
                        .then(() => {
                            return resolve({ queue: queue, domain: domain,
                                routingKey: routingKey, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            }
        };
    }

    registerNotice(domain, routingKey, callback) {
        const queue = UberBus.route(routingKey, this._id);
        const consumerTag = UberBus.route(routingKey, this._id);

        return {
            start: () => {
                return new Promise((resolve, reject) => {
                    this._channel.assertExchange(domain, 'topic', { durable: false })
                        .then(() => {
                            return this._channel.assertQueue(queue, { durable: false, exclusive: true, autoDelete: true });
                        })
                        .then(() => {
                            return this._channel.bindQueue(queue, domain, routingKey);
                        })
                        .then(() => {
                            return this._channel.consume(queue, (message) => {
                                const content = UberBus.unwrap(message);
                                if (content)
                                    callback(content.payload);
                            }, { noAck: true, consumerTag: consumerTag });
                        })
                        .then(() => {
                            return resolve({ queue: queue, domain: domain,
                                routingKey: routingKey, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            },
            stop: () => {
                return new Promise((resolve, reject) => {
                    this._channel.unbindQueue(queue, domain, routingKey)
                        .then(() => {
                            return this._channel.cancel(consumerTag);
                        })
                        .then(() => {
                            return resolve({ queue: queue, domain: domain,
                                 routingKey: routingKey, consumerTag: consumerTag });
                        })
                        .catch(reject);
                });
            }
        };
    }

    registerEvent(domain, routingKey, callback) {
        const queue = this.eventsQueue;

        return {
            start: () => {
                return new Promise((resolve, reject) => {
                    this._eventChannel.assertExchange(domain, 'topic', { durable: false })
                        .then(() => {
                            return this._eventChannel.assertQueue(queue,
                                { durable: false, exclusive: true, autoDelete: true });
                        })
                        .then(() => {
                            return this._eventChannel.bindQueue(queue, domain, routingKey);
                        })
                        .then(() => {
                            return resolve({ queue: queue, domain: domain, routingKey: routingKey });
                        })
                        .catch(reject);
                });
            },
            stop: () => {
                return new Promise((resolve, reject) => {
                    this._eventChannel.unbindQueue(queue, domain, routingKey)
                        .then(() => {
                            return resolve({ queue: queue, domain: domain, routingKey: routingKey });
                        })
                        .catch(reject);
                });
            },
            handler: callback
        };
    }

    register(domain, type, routingKey, callback) {
        switch (type) {
            case UberBus.Command:
                return this.registerCommand(routingKey, callback);
            case UberBus.Query:
                return this.registerQuery(domain, routingKey, callback);
            case UberBus.Notice:
                return this.registerNotice(domain, routingKey, callback);
            case UberBus.Event:
                return this.registerEvent(domain, routingKey, callback);
            default:
                throw new GenericError.promise(util.format('unknown message type %s', type));
        }
    }

    /**
     *
     * @public
     * @param domain {string}
     * @param type {string}
     * @param name {string}
     * @param callback {function}
     * @returns {Promise}
     */
    listen(domain, type, name, callback) {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');

        return new Promise((resolve, reject) => {
            const routingKey = UberBus.route(domain, type, name);
            if (routingKey in this._listeners)
                return reject(new GenericError(util.format('the bus already have listener with routingKey=%s', routingKey)));
            this._logger.debug('starting to listen bus %s (%s) with routing key {%s}', type, name, routingKey);
            const listener = this.register(domain, type, routingKey, callback);
            this._listeners[routingKey] = listener;
            listener.start()
                .then(() => { resolve({
                    type: type, name: name, routingKey: routingKey }); })
                .catch(reject);
        });
    }

    /**
     *
     * @public
     * @param listener {object}
     */
    removeListener(listener) {
        return new Promise((resolve, reject) => {
            if (!(listener.routingKey in this._listeners))
                return reject(new GenericError(util.format('the bus doesn\'t have listener with routingKey=%s', listener.routingKey)));
            this._logger.debug('stopping to listen bus %s (%s) with routing key {%s}',
                listener.type, listener.name, listener.routingKey);
            if (!this.isReady) {
                delete this._listeners[listener.routingKey];
                return ServiceUnavailableError.promise('bus not ready');
            }
            this._listeners[listener.routingKey].stop()
                .then(() => {
                    delete this._listeners[listener.routingKey];
                    resolve(listener);
                })
                .catch(reject);
            });
    }

    /**
     * Starts consuming ordered events queue
     * @public
     * @returns {Promise}
     */
    consume() {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');

        return new Promise((resolve, reject) => {
            this._eventChannel.assertQueue(this.eventsQueue, {durable: false, exclusive: true, autoDelete: true})
                .then(() => {
                    return this._eventChannel.consume(this.eventsQueue, (message) => {
                        this.handleEvent(message);
                    }, {noAck: false, consumerTag: this.eventsQueue});
                })
                .then(resolve)
                .catch(reject);
        });
    }

    /**
     * Stops consuming ordered events queue
     * @public
     * @returns {Promise}
     */
    cancel() {
        if (!this.isReady)
            return ServiceUnavailableError.promise('bus not ready');
        return this._eventChannel.cancel(this.eventsQueue);
    }

    /**
     * Closes the bus
     * @public
     * @returns {Promise}
     */
    close() {
        this._logger.debug('closing bus');

        const chain = this.cancel();

        chain.then(() => {
            return new Promise((resolve) => {
                this._logger.debug('stopping pendings');
                _.forEach(this._pendings, (pending) => {
                    pending.stop();
                });
                resolve();
            });
        });

        if (this._channel)
            chain.then(() => {
                return new Promise((resolve, reject) => {
                    this._logger.debug('closing CQN channel');
                    return this._channel.close()
                        .then(resolve)
                        .catch(reject);
                });
            });

        if (this._eventChannel)
            chain.then(() => {
                return new Promise((resolve, reject) => {
                    this._logger.debug('closing Events channel');
                    return this._eventChannel.close()
                        .then(resolve)
                        .catch(reject);
                });
            });

        if (this._connection)
            chain.then(() => {
                return new Promise((resolve, reject) => {
                    this._logger.debug('closing connection');
                    this._connection.close()
                        .then(resolve)
                        .catch(reject);
                });
            });

        chain.then(() => {
            this.dispose();
        });

        return chain;
    }

    /**
     * Make bus clone with (for parallel services)
     * @public
     * @param {string} serviceName New service name (duplicates are not allowed)
     */
    clone(serviceName) {
        return new UberBus(this._config, serviceName);
    }
}